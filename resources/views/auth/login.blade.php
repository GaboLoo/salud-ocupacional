@extends('layouts.login')

@section('content')

    

    <div class="container-fluid">

        <div class="row aling-item-center">
            <div class="col-md-7 col-lg-9 bg-login order-1 order-sm-0 titulo-login" style="background-image: url('{{asset('images/img-login.jpg')}}');">
                <h1>Medisegur</h1>
            </div>
            <div class="col-md-5 col-lg-3 content-login">
                <div>
                    <div class="logo text-center">
                        <img src="{{asset('images/medilaboris_logo.png')}}" alt="" class="img-fluid img-logo">
                        <h2 class="text-login-mobil">Medisegur</h2>
                    </div>
                    <div class="form mt-5">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row justify-content-center">

                                <div class="col-md-9">
                                    <div class="input-group mb-3">
                                        <input id="documento" type="text" class="form-control @error('documento') is-invalid @enderror" name="documento" value="{{ old('documento') }}" required autocomplete="email" autofocus placeholder="{{ __('Usuario') }}">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <i class="fas fa-user"></i>
                                            </div>
                                        </div>
                                        @error('documento')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                </div>
                            </div>

                            <div class="form-group row justify-content-center">

                                <div class="col-md-9">
                                    <div class="input-group">
                                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="{{ __('Contraseña') }}">
                                        <div class="input-group-append">
                                            <div class="input-group-text">
                                                <i class="fas fa-user-lock"></i>
                                            </div>
                                        </div>
                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row mb-0 justify-content-center">
                                <div class="col-md-8 text-center">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ __('Ingresar') }}
                                    </button>

                                    {{-- @if (Route::has('password.request'))
                                        <a class="btn btn-link mt-3" href="{{ route('password.request') }}">
                                            {{ __('¿ Olvidaste tu contraseña ?') }}
                                        </a>
                                    @endif --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
