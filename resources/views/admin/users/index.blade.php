@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Usuarios</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Usuarios</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row justify-content-center mb-4">
                    @foreach($tipo_usuarios as $tipo_usuario)

                        @if(auth()->user()->id_type_user != 2)
                            <div class="col-md-3 col-sm-6 col-12" onclick="addUser({{$tipo_usuario->id}})" style="cursor:pointer;">
                                <div class="info-box">
                                    <span class="info-box-icon bg-info"><i class="fas fa-user"></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">{{$tipo_usuario->title}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </div>
                        @else
                            @if($tipo_usuario->id == 3 || $tipo_usuario->id == 4)
                                <div class="col-md-3 col-sm-6 col-12" onclick="addUser({{$tipo_usuario->id}})" style="cursor:pointer;">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-info"><i class="fas fa-user"></i></span>

                                        <div class="info-box-content">
                                            <span class="info-box-text">{{$tipo_usuario->title}}</span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            @endif
                        @endif


                    @endforeach
                </div>

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="row d-none" id="form-user-dash">
                    <section class="col-lg-12 connectedSortable ui-sortable">
                        <div class="card">
                            <div class="card-header ui-sortable-handle">
                                <h3 class="card-title">Formulario de <span id="type"></span></h3>
                            </div>
                            <div class="card-body">
                                <form action="{{route('user.store')}}" method="post"  enctype="multipart/form-data">
                                    @csrf
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <input type="hidden" id="id_type_user" name="id_type_user">
                                                <input type="hidden" id="tipo_form" name="tipo_form" value="1">
                                                <div class="col-md-6 form-group">
                                                    <label for="">Nombre</label>
                                                    <input type="text" name="nombre" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Apellido</label>
                                                    <input type="text" name="apellido" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Email</label>
                                                    <input type="text" name="email" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Contraseña</label>
                                                    <input type="password" name="password" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo Documento</label>
                                                    <select name="tipo_documento" id="" class="selec2 form-control">
                                                        <option value="" selected disabled>Eligue un tipo de documento</option>
                                                        <option value="1" >DNI</option>
                                                        <option value="2" >Carné de Extranjería</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Documento</label>
                                                    <input type="text" name="documento" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Celular</label>
                                                    <input type="text" name="celular" class="form-control">
                                                </div>
                                                {{-- <div class="col-md-6 form-group">
                                                    <label for="">Direccion</label>
                                                    <input type="text" name="direccion" class="form-control">
                                                </div> --}}
                                                <div class="col-md-6 form-group">
                                                    <label for="">Departamento</label>
                                                    <select name="departamento" class="select2 form-control">
                                                        <option value="" disabled selected>Escoger un departamento</option>
                                                            @foreach ($departamentos as $departamento)
                                                                <option value="{{$departamento->id}}">{{$departamento->name}}</option>   
                                                            @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Provincia</label>
                                                    <select name="provincia" class="select2 form-control">
                                                        <option value="" disabled selected>Escoger una provincia</option>
                                                        @foreach ($provincias as $provincia)
                                                            <option value="{{$provincia->id}}">{{$provincia->name}}</option>   
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Distrito</label>
                                                    <select name="distrito" class="select2 form-control">
                                                        <option value="" disabled selected>Escoger un distrito</option>
                                                        @foreach ($distritos as $distrito)
                                                            <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="file_super" type="file" class="d-none" name="file1"  accept="image/x-png,image/gif,image/jpeg">
                                            <label for="file_super" class="input-file m-auto" id="file-foto1">
                                                <i class="far fa-image"></i>
                                            </label>
                                        </div>
                                        <div class="col-md-11">
                                            <hr>
                                        </div>
                                        <div class="col-md-11">
                                            <button class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>


                <div class="row d-none mb-4" id="form-user-colaboradores">
                    <section class="col-lg-12 connectedSortable ui-sortable">
                        <div class="card">
                            <div class="card-header ui-sortable-handle">
                                <h3 class="card-title">Formulario de <span id="type-admin"> </span></h3>
                            </div>
                            <div class="card-body">
                                <form action="{{route('user.store')}}" method="post"  enctype="multipart/form-data">
                                    @csrf
                                    <div class="row justify-content-center">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <input type="hidden" id="id_type_user_colaboradores" name="id_type_user_colaboradores">
                                                <input type="hidden" id="tipo_form" name="tipo_form" value="2">
                                                <div class="col-md-6 form-group">
                                                    <label for="">Nombre</label>
                                                    <input type="text" name="nombre" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Apellido</label>
                                                    <input type="text" name="apellido" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Email</label>
                                                    <input type="text" name="email" class="form-control">
                                                </div>

                                                <div class="col-md-6 form-group">
                                                    <label for="">Contraseña</label>
                                                    <input type="text" name="password" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo Documento</label>
                                                    <select name="tipo_documento" id="" class="select2 form-control">
                                                        <option value="" selected disabled>Eligue un tipo de documento</option>
                                                        <option value="1" >DNI</option>
                                                        <option value="2" >Carnet de extranjeria</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Documento</label>
                                                    <input type="text" name="documento" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Celular</label>
                                                    <input type="text" name="celular" class="form-control">
                                                </div>
                                                {{-- <div class="col-md-6 form-group">
                                                    <label for="">Direccion</label>
                                                    <input type="text" name="direccion" class="form-control">
                                                </div> --}}
                                                <div class="col-md-6 form-group">
                                                    <label for="">Departamento</label>
                                                    <select name="departamento" class="select2 form-control">
                                                        <option value="" disabled selected>Escoger un departamento</option>
                                                            @foreach ($departamentos as $departamento)
                                                                <option value="{{$departamento->id}}">{{$departamento->name}}</option>   
                                                            @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Provincia</label>
                                                    <select name="provincia" class="select2 form-control">
                                                        <option value="" disabled selected>Escoger una provincia</option>
                                                        @foreach ($provincias as $provincia)
                                                            <option value="{{$provincia->id}}">{{$provincia->name}}</option>   
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Distrito</label>
                                                    <select name="distrito" class="select2 form-control">
                                                        <option value="" disabled selected>Escoger un distrito</option>
                                                        @foreach ($distritos as $distrito)
                                                            <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                        @endforeach
                                                    </select>
                                                </div>
                                                {{--datos de la tabla colaborador--}}

                                                <div class="col-md-6 form-group">
                                                    <label for="">Telefono de casa</label>
                                                    <input type="text" name="telefono_casa" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Puesto</label>
                                                    <select name="puesto" id="" class="form-control select2">
                                                        <option value="" selected disabled>Elegir un puesto</option>
                                                        @foreach($puestos as $puesto)
                                                            <option value="{{$puesto->id}}">{{$puesto->titulo}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Area</label>
                                                    <select name="area" id="" class="form-control select2">
                                                        <option value="" selected disabled>Elegir un area</option>
                                                        @foreach($areas as $area)
                                                            <option value="{{$area->id}}">{{$area->titulo}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Fecha Nacimiento</label>
                                                    <input type="date" name="fecha_nacimiento" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Peso</label>
                                                    <input type="text" name="peso" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Altura</label>
                                                    <input type="text" name="altura" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Empresa</label>
                                                    <select name="empresa" id="" class="form-control select2">
                                                        <option value="" disabled selected>Seleccione una empresa</option>
                                                        @foreach($empresas as $empresa)
                                                            <option value="{{$empresa->id}}">{{$empresa->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo de Sangre</label>
                                                    <select name="sangre" id="" class="form-control select2">
                                                        <option value="" disabled selected>Seleccione un tipo de sangre</option>
                                                        <option value="O negativo">O negativo</option>
                                                        <option value="O positivo">O positivo</option>
                                                        <option value="A negativo">A negativo</option>
                                                        <option value="A positivo">A positivo</option>
                                                        <option value="B negativo">B negativo</option>
                                                        <option value="B positivo">B positivo</option>
                                                        <option value="AB negativo">AB negativo</option>
                                                        <option value="AB positivo">AB positivo</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Enfermedades Preexistentes</label>
                                                    <textarea class="form-control" name="emfermedades"></textarea>
                                                </div>
                                                <div class="col-md-12 form-group">
                                                    <label for="">Alérgico a algún medicamento</label>
                                                    <textarea class="form-control" name="alergico"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="file" type="file" class="d-none" name="file"  accept="image/x-png,image/gif,image/jpeg">
                                            <label for="file" class="input-file m-auto" id="file-foto">
                                                <i class="far fa-image"></i>
                                            </label>
                                        </div>
                                        <div class="col-md-11">
                                            <hr>
                                        </div>
                                        <div class="col-md-11">
                                            <button class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Tabla de usuarios</h3>
                                </div>
                                <div class="card-body">
                                    <table class="table" id="example2">
                                        <thead>
                                        <th>Nº</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Documento</th>
                                        <th>Tipo Usuario</th>
                                        <th>Opinion</th>
                                        </thead>
                                        <tbody>
                                        @php $c = 1; @endphp
                                        @foreach($usuarios as $usuario)
                                            <tr>
                                                <td>{{$c++}}</td>
                                                <td>{{$usuario->name}}</td>
                                                <td>{{$usuario->last_name}}</td>
                                                <td>{{$usuario->documento}}</td>
                                                <td>{{$usuario->title}}</td>
                                                <td>
                                                    <a href="{{route('user.edit',$usuario->id)}}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                                    @if(auth()->user()->id_type_user == 1)
                                                        <button onclick="deleteUsuario({{$usuario->id}},'{{$usuario->name}} {{$usuario->last_name}}')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>


            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var filePreview = document.createElement('img');
                    filePreview.id = 'file-preview';
                    filePreview.className = 'img-fluid';
                    //e.target.result contents the base64 data from the image uploaded
                    filePreview.src = e.target.result;
                    console.log(e.target.result);

                    var previewZone = document.getElementById('file-foto');
                    $('#file-foto').html('')
                    previewZone.appendChild(filePreview);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        function readFile1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var filePreview = document.createElement('img');
                    filePreview.id = 'file-preview1';
                    filePreview.className = 'img-fluid';
                    //e.target.result contents the base64 data from the image uploaded
                    filePreview.src = e.target.result;
                    console.log(e.target.result);

                    var previewZone = document.getElementById('file-foto1');
                    $('#file-foto1').html('')
                    previewZone.appendChild(filePreview);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file');

        fileUpload.onchange = function (e) {
            readFile(e.srcElement);
        }

        var fileUpload1 = document.getElementById('file_super');

        fileUpload1.onchange = function (e) {
            readFile1(e.srcElement);
        }

        function addUser(id) {
            if(id == 1){
                $('#form-user-dash').removeClass('d-none')
                $('#form-user-colaboradores').addClass('d-none')
                $('#id_type_user').val(id)
                $('#type').html('Admin')
            }else if(id == 2){
                $('#form-user-dash').removeClass('d-none')
                $('#form-user-colaboradores').addClass('d-none')
                $('#id_type_user').val(id)
                $('#type').html('Medico')
            }else if(id == 3) {
                $('#form-user-colaboradores').removeClass('d-none')
                $('#form-user-dash').addClass('d-none')
                $('#id_type_user_colaboradores').val(id)
                $('#type-admin').html('Colaborador')
            }else if(id == 4) {
                $('#form-user-colaboradores').removeClass('d-none')
                $('#form-user-dash').addClass('d-none')
                $('#id_type_user_colaboradores').val(id)
                $('#type-admin').html('Admin - Empresa')
            }
        }

        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });

        function deleteUsuario(id,name) {
            swal({
                title: `¿ Estas seguro que quieres eliminar a ${name} ?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '../../user/'+id,
                            type: 'delete',
                            dataType: "JSON",
                            data: {
                                "id": id
                            },
                            success: function (response)
                            {
                                if(response.success){
                                    swal(response.message, {
                                        icon: "success",
                                    });
                                    setTimeout(function () {
                                        location.reload();
                                    },1000)
                                }
                            },
                        })


                    }
                });
        }
    </script>
@endsection
