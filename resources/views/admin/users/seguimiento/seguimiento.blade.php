@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Seguimiento</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Seguimiento</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                
                <div class="alert alert-info">
                    @if($user->estado_salud == 1)
                        Estado del trabajor es Sano
                    @elseif($user->estado_salud == 2)
                        Estado del trabajador es Sospechoso
                    @elseif($user->estado_salud == 3)
                        Estado del trabajador es Activo
                    @endif
                </div>

                @if(auth()->user()->id_type_user == 2)
                    <div class="row mb-4">
                        <div class="col-md-4">
                            <div class="card">
                                <a href="{{route('diagnosticar',$id_user)}}" class="btn btn-primary">Diagnosticar</a>
                            </div>
                        </div>
                        @if($user->estado_salud == 2 || $user->estado_salud == 3)
                            <div class="col-md-4">
                                <div class="card">
                                    <form action="{{route('calificar-sano',$id_user)}}" method="get">
                                        <button type="submit" class="btn btn-success btn-block">Calificar como sano</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                        @if($user->estado_salud == 3)
                            <div class="col-md-4">
                                <div class="card">
                                    <form action="{{route('calificar-sospechoso',$id_user)}}" method="get">
                                        <button type="submit" class="btn btn-success btn-block">Calificar como caso sospechoso</button>
                                    </form>
                                </div>
                            </div>
                        @endif
                        @if($user->estado_salud == 2)
                            <div class="col-md-4">
                                <div class="card">
                                    <form action="{{route('calificar-activo',$id_user)}}" method="get">
                                        <button type="submit" class="btn btn-danger btn-block">Calificar como caso activo</button>
                                    </form>
                                </div>
                            </div>
                        @endif

                    </div>

                    @if($user->estado_salud != 1)
                        <div class="row mb-4">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <form action="{{route('save-reporte')}}" method="post">
                                            @csrf
                                            <input type="hidden" value="{{$id_user}}" name="id_user">
                                            <div class="form-group">
                                                <p>Seguimiento de personal bajo sospecha y positivos COVID</p>
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" id="Contactados" value="1" name="seguimiento" class="mr-2" @if($user->seguimiento == 1) checked @endif>
                                                <label for="Contactados">Contactados</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" id="nollamadaa" value="2" name="seguimiento" class="mr-2" @if($user->seguimiento == 2) checked @endif>
                                                <label for="nollamadaa">No respondieron llamadas</label>
                                            </div>
                                            <div class="form-group ">
                                                <button class="btn btn-primary">Guardar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif

                <div class="row">
                    <div class="col-md-12">

                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">Lista de Seguimientos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Historial de Diagnosticos</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-enfermedaades" role="tab" aria-controls="custom-tabs-four-enfermedaades" aria-selected="false">Enfermedades Preexistentes</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                                        @if($data != null)
                                            <table class="table">
                                                <thead>
                                                <th>Nº</th>
                                                <th>Fecha</th>
                                                <th>Fase</th>
                                                <th>respuesta</th>
                                                </thead>
                                                <tbody>
                                                @php $c = 1 @endphp
                                                @foreach($data as $item)
                                                    @if($item['tipo'] == 'fase1')
                                                        <tr>
                                                            <td>{{$c++}}</td>
                                                            <td>@php $date = new DateTime($item['data']->created_at); $fecha = $date->format('d-m-Y'); @endphp {{$fecha}}</td>
                                                            <td>{{$item['tipo']}}</td>
                                                            <td>{{$item['data']->opciones_seleccionadas}}</td>
                                                        </tr>
                                                    @elseif($item['tipo'] == 'fase2')
                                                        <tr>
                                                            <td width="10%">{{$c}}</td>
                                                            <td width="10%">@php $date = new DateTime($item['data']->created_at); $fecha = $date->format('d-m-Y'); @endphp {{$fecha}}</td>
                                                            <td width="10%">{{$item['tipo']}}</td>
                                                            <td width="70%">

                                                                <div class="accordion" id="accordion{{$c}}">
                                                                    <div class="card">
                                                                        <div class="card-header" id="headingOne">
                                                                            <h2 class="mb-0">
                                                                                <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#res{{$c}}" aria-expanded="false" aria-controls="collapseOne">
                                                                                    Lista de Respuesta
                                                                                </button>
                                                                            </h2>
                                                                        </div>

                                                                        <div id="res{{$c}}" class="collapse " aria-labelledby="headingOne" data-parent="#accordion{{$c}}">
                                                                            <div class="card-body">
                                                                                <div class="accordion" id="subaccordion-{{$c}}">

                                                                                    <h6>1. ¿Estuviste en contacto con alguien a quién se le confirmó que tenía COVID-19 dentro de los 14 días de haber iniciado con los síntomas?</h6>
                                                                                    <p>RPTA: {{$item['data']->pregunta1}}</p>
                                                                                    <hr>

                                                                                    <h6>2. ¿Cuándo iniciaron los síntomas?</h6>
                                                                                    <p>RPTA: {{$item['data']->pregunta2}}</p>
                                                                                    <hr>

                                                                                    <h6>3. ¿Realizó aislamiento social?</h6>
                                                                                    <p>RPTA: {{$item['data']->pregunta3}}</p>
                                                                                    <hr>

                                                                                    {{--                                                                           @dump(count($item['personas']))--}}

                                                                                    @if(count($item['personas']) != 0)


                                                                                        <div class="card">
                                                                                            <div class="card-header" >
                                                                                                <h2 class="mb-0">
                                                                                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#dad" aria-expanded="false" aria-controls="dad">
                                                                                                        Lista de personas con la que se tuvo contacto
                                                                                                    </button>
                                                                                                </h2>
                                                                                            </div>

                                                                                            <div id="dad" class="collapse " aria-labelledby="headingOne" data-parent="#subaccordion-{{$c}}">
                                                                                                <div class="card-body">
                                                                                                    @foreach($item['personas'] as $persona)

                                                                                                        <h6><strong>Nombre:</strong> {{$persona->nombre}}</h6>
                                                                                                        <p><strong>Dni:</strong> {{$persona->dni}}</p>
                                                                                                        <p><strong>Edad:</strong> {{$persona->edad}}</p>
                                                                                                        <p><strong>Sexo:</strong> {{$persona->sexo}}</p>
                                                                                                        <p><strong>Teléfono:</strong> {{$persona->telefono}}</p>
                                                                                                        <p><strong>Dirección:</strong> {{$persona->direccion}}</p>
                                                                                                        <hr>
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>


                                                                                    @endif

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </td>
                                                        </tr>

                                                    @elseif($item['tipo'] == 'fase3')
                                                        {{--                                                @dump($item)--}}
                                                        <tr>
                                                            <td width="10%">{{$c}}</td>
                                                            <td width="10%">@php $date = new DateTime($item['registro']->created_at); $fecha = $date->format('d-m-Y'); @endphp {{$fecha}}</td>
                                                            <td>{{$item['tipo']}}</td>
                                                            <td>
                                                                @if(count($item['data']) != 0)
                                                                    <div class="accordion" id="fase3-{{$c}}">
                                                                        <div class="card">
                                                                            <div class="card-header" id="headingOne">
                                                                                <h2 class="mb-0">
                                                                                    <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#fase3{{$c}}" aria-expanded="false" aria-controls="collapseOne">
                                                                                        Lista de respuestas </button>
                                                                                </h2>
                                                                            </div>

                                                                            <div id="fase3{{$c}}" class="collapse" aria-labelledby="headingOne" data-parent="#fase3-{{$c}}">
                                                                                <div class="card-body">
                                                                                    @foreach($item['data'] as $respuesta)
                                                                                        <h6><strong>Pregunta:</strong> {{$respuesta->pregunta}}</h6>
                                                                                        <p><strong>Respuesta:</strong> Opcion {{$respuesta->respuesta}}</p>
                                                                                        <hr>
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    @elseif($item['tipo'] == 'fase4')
                                                        <td width="10%">{{$c}}</td>
                                                        <td width="10%">@php $date = new DateTime($item['data']->created_at); $fecha = $date->format('d-m-Y'); @endphp {{$fecha}}</td>
                                                        <td>{{$item['tipo']}}</td>
                                                        <td>

                                                            <div class="accordion" id="fase4-{{$c}}">
                                                                <div class="card">
                                                                    <div class="card-header" id="headingOne">
                                                                        <h2 class="mb-0">
                                                                            <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#fase4{{$c}}" aria-expanded="false" aria-controls="fase4{{$c}}">
                                                                                Lista de respuestas
                                                                            </button>
                                                                        </h2>
                                                                    </div>

                                                                    <div id="fase4{{$c}}" class="collapse" aria-labelledby="headingOne" data-parent="#fase4-{{$c}}">
                                                                        <div class="card-body">
                                                                            <h6><strong>Pregunta: </strong> ¿Se le realizó algún tipo de prueba diagnóstica para el COVID-19?</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->diagnostico}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> De ser Si, colocar que tipo:</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->tipo_diagnostico}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> Fecha de la infección</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->fecha_toma}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> ¿Cómo cree que se infecto?</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->como_cree_que_infecto}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> ¿Necesito ser hospitalizado?</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->hospitalizacion}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> De ser si, Fecha de hospitalización</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->fecha_hospitalizacion}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> De ser si, Fecha de alta</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->fecha_alta}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> ¿Tuvo alguna complicación en la hospitalización?</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->complicacion_hospitalaria}} </p>
                                                                            <hr>

                                                                            <h6><strong>Pregunta: </strong> De ser si, Coloque cual</h6>
                                                                            <p><strong>Respuesta:</strong> {{$item['data']->complicacion_hospitalaria_descripcion}} </p>
                                                                            <hr>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </td>
                                                    @endif
                                                    @php $c++ @endphp
                                                @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <div class="alert alert-info" role="alert">
                                                El usuario no ha registrado ninguna actividad en el sistema
                                            </div>
                                        @endif
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                                        <table class="table">
                                            <thead>
                                            <th>Nº</th>
                                            <th>Titulo</th>
                                            <th>Fecha y hora</th>
                                            <th>Diagnostico</th>
                                            </thead>
                                            <tbody>
                                            @php $c = 1 @endphp
                                            @foreach($diagnosticos as $diagnostico)
                                                <tr>
                                                    <td>{{$c++}}</td>
                                                    <td>{{$diagnostico->titulo}}</td>
                                                    <td> @php  @endphp {{$diagnostico->fecha_hora}}</td>
                                                    <td>{{$diagnostico->descripcion}}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-four-enfermedaades" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                                        <table class="table">
                                            <thead>
                                            <th>Nº</th>
                                            <th>Enfermedad</th>
                                            </thead>
                                            <tbody>
                                            @php $c = 1 @endphp
                                            @foreach($enfermedades as $enfermedad)
                                                <tr>
                                                    <td>{{$c++}}</td>
                                                    <td>{{$enfermedad->title}}</td>
                                                </tr>
                                            @endforeach
                                            @if($user->enfermedades != '')
                                                <tr>
                                                    <td>{{$c++}}</td>
                                                    <td> <strong>Otras efermedades:</strong> {{$user->enfermedades}}</td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')

@endsection
