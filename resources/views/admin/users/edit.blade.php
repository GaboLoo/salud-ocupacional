@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Usuarios</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Usuarios</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if($usuario_data->id_type_user == 1 || $usuario_data->id_type_user == 2)
                <div class="row" id="form-user-dash">
                    <section class="col-lg-12 connectedSortable ui-sortable">
                        <div class="card">
                            <div class="card-header ui-sortable-handle">
                                <h3 class="card-title">Formulario de <span id="type">{{$usuario_data->title}}</span></h3>
                            </div>
                            <div class="card-body">
                                <form action="{{route('user.update',$id)}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <input type="hidden" id="id_type_user" name="id_type_user">
                                                <input type="hidden" id="tipo_form" name="tipo_form" value="1">
                                                <div class="col-md-6 form-group">
                                                    <label for="">Nombre</label>
                                                    <input type="text" name="nombre" class="form-control" value="{{$usuario_data->name}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Apellido</label>
                                                    <input type="text" name="apellido" class="form-control" value="{{$usuario_data->last_name}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Email</label>
                                                    <input type="text" name="email" class="form-control" value="{{$usuario_data->email}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Contraseña</label>
                                                    <input type="password" name="password" class="form-control">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo Documento</label>
                                                    @if(!empty($usuario_data->type_documento))
                                                        <select name="tipo_documento" id="" class="form-control">
                                                            <option value="1" @if($usuario_data->type_documento == 1) selected @endif>DNI</option>
                                                            <option value="2" @if($usuario_data->type_documento == 2) selected @endif>Carné de Extranjería</option>
                                                        </select>
                                                    @else

                                                    @endif
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Documento</label>
                                                    <input type="text" name="documento" class="form-control" value="{{$usuario_data->documento}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Celular</label>
                                                    <input type="text" name="celular" class="form-control" value="{{$usuario_data->celular}}">
                                                </div>
                                                {{-- <div class="col-md-6 form-group">
                                                    <label for="">Direccion</label>
                                                    <input type="text" name="direccion" class="form-control" value="{{$usuario_data->direccion}}">
                                                </div> --}}
                                                <div class="col-md-6 form-group">
                                                    <label for="">Departamento</label>
                                                    <select name="departamento" class="select2 form-control">
                                                        @if ($usuario_data->id_departamento != '')
                                                            @foreach ($departamentos as $departamento)
                                                                @if ($usuario_data->id_departamento == $departamento->id)
                                                                    <option value="{{$departamento->id}}" selected>{{$departamento->name}}</option>  
                                                                @else
                                                                    <option value="{{$departamento->id}}">{{$departamento->name}}</option>  
                                                                @endif
                                                            @endforeach
                                                            
                                                        @else
                                                            <option value="" disabled selected>Escoger un departamento</option>
                                                            @foreach ($departamentos as $departamento)
                                                                <option value="{{$departamento->id}}">{{$departamento->name}}</option>   
                                                            @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Provincia</label>
                                                    <select name="provincia" class="select2 form-control">
                                                        @if ($usuario_data->id_provincia != '')
                                                            @foreach ($provincias as $provincia)
                                                                @if ($usuario_data->id_provincia == $provincia->id)
                                                                    <option value="{{$provincia->id}}" selected>{{$provincia->name}}</option> 
                                                                @else
                                                                    <option value="{{$provincia->id}}">{{$provincia->name}}</option> 
                                                                @endif  
                                                            @endforeach
                                                        @else
                                                        <option value="" disabled selected>Escoger una provincia</option>
                                                        @foreach ($provincias as $provincia)
                                                            <option value="{{$provincia->id}}">{{$provincia->name}}</option>   
                                                        @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Distrito</label>
                                                    <select name="distrito" class="select2 form-control">
                                                        @if ($usuario_data->id_distrito != '')
                                                            @foreach ($distritos as $distrito)
                                                                @if ($usuario_data->id_distrito == $distrito->id)
                                                                <option value="{{$distrito->id}}" selected>{{$distrito->name}}</option>   
                                                                @else
                                                                <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                                @endif
                                                            @endforeach
                                                        @else
                                                        <option value="" disabled selected>Escoger un distrito</option>
                                                        @foreach ($distritos as $distrito)
                                                            <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                        @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="file" type="file" class="d-none" name="file"  accept="image/x-png,image/gif,image/jpeg">
                                            <label for="file" class="input-file m-auto" id="file-foto">
                                                @if($usuario_data->foto)
                                                    <img src="{{$usuario_data->foto}}" alt="" class="img-fluid">
                                                @else
                                                    <i class="far fa-image"></i>
                                                @endif
                                            </label>
                                        </div>
                                        <div class="col-md-11">
                                            <hr>
                                        </div>
                                        <div class="col-md-11">
                                            <button class="btn btn-primary">Actualizar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
                @endif
                @if($usuario_data->id_type_user == 3 || $usuario_data->id_type_user == 4 )
                <div class="row mb-4" id="form-user-colaboradores">
                    <section class="col-lg-12 connectedSortable ui-sortable">
                        <div class="card">
                            <div class="card-header ui-sortable-handle">
                                <h3 class="card-title">Formulario de Colaborador</h3>
                            </div>
                            <div class="card-body">
                                <form action="{{route('user.update',$id)}}" method="post"  enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <input type="hidden" id="id_type_user_colaboradores" name="id_type_user_colaboradores" value="{{$usuario_data->id_type_user}}">
                                                <input type="hidden" id="tipo_form" name="tipo_form" value="2">
                                                <div class="col-md-6 form-group">
                                                    <label for="">Nombre</label>
                                                    <input type="text" name="nombre" class="form-control" value="{{$usuario_data->name}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Apellido</label>
                                                    <input type="text" name="apellido" class="form-control" value="{{$usuario_data->last_name}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Email</label>
                                                    <input type="text" name="email" class="form-control" value="{{$usuario_data->email}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Contraseña</label>
                                                    <input type="text" name="password" class="form-control" value="">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo Documento</label>
                                                    @if(!empty($usuario_data->type_documento))
                                                        <select name="tipo_documento" id="" class="form-control">
                                                            <option value="1" @if($usuario_data->type_documento == 1) selected @endif>DNI</option>
                                                            <option value="2" @if($usuario_data->type_documento == 2) selected @endif>Carnet de extranjeria</option>
                                                        </select>
                                                    @else

                                                    @endif
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Documento</label>
                                                    <input type="text" name="documento" class="form-control" value="{{$usuario_data->documento}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Celular</label>
                                                    <input type="text" name="celular" class="form-control" value="{{$usuario_data->celular}}">
                                                </div>
                                                {{-- <div class="col-md-6 form-group">
                                                    <label for="">Direccion</label>
                                                    <input type="text" name="direccion" class="form-control" value="{{$usuario_data->direccion}}">
                                                </div> --}}
                                                <div class="col-md-6 form-group">
                                                    <label for="">Departamento</label>
                                                    <select name="departamento" class="select2 form-control">
                                                        @if ($usuario_data->id_departamento != '')
                                                            @foreach ($departamentos as $departamento)
                                                                @if ($usuario_data->id_departamento == $departamento->id)
                                                                    <option value="{{$departamento->id}}" selected>{{$departamento->name}}</option>  
                                                                @else
                                                                    <option value="{{$departamento->id}}">{{$departamento->name}}</option>  
                                                                @endif
                                                            @endforeach
                                                            
                                                        @else
                                                            <option value="" disabled selected>Escoger un departamento</option>
                                                            @foreach ($departamentos as $departamento)
                                                                <option value="{{$departamento->id}}">{{$departamento->name}}</option>   
                                                            @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Provincia</label>
                                                    <select name="provincia" class="select2 form-control">
                                                        @if ($usuario_data->id_provincia != '')
                                                            @foreach ($provincias as $provincia)
                                                                @if ($usuario_data->id_provincia == $provincia->id)
                                                                    <option value="{{$provincia->id}}" selected>{{$provincia->name}}</option> 
                                                                @else
                                                                    <option value="{{$provincia->id}}">{{$provincia->name}}</option> 
                                                                @endif  
                                                            @endforeach
                                                        @else
                                                        <option value="" disabled selected>Escoger una provincia</option>
                                                        @foreach ($provincias as $provincia)
                                                            <option value="{{$provincia->id}}">{{$provincia->name}}</option>   
                                                        @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Distrito</label>
                                                    <select name="distrito" class="select2 form-control">
                                                        @if ($usuario_data->id_distrito != '')
                                                            @foreach ($distritos as $distrito)
                                                                @if ($usuario_data->id_distrito == $distrito->id)
                                                                <option value="{{$distrito->id}}" selected>{{$distrito->name}}</option>   
                                                                @else
                                                                <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                                @endif
                                                            @endforeach
                                                        @else
                                                        <option value="" disabled selected>Escoger un distrito</option>
                                                        @foreach ($distritos as $distrito)
                                                            <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                        @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                
                                                <div class="col-md-6 form-group">
                                                    <label for="">Telefono de casa</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="text" name="telefono_casa" class="form-control" value="{{$data_colaborador->telefono_casa}}">
                                                    @else
                                                        <input type="text" name="telefono_casa" class="form-control">
                                                    @endif

                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Puesto</label>
                                                    <input name="puesto" class="form-control" value="{{$data_colaborador->puesto}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Area</label>
                                                    <select name="area" id="" class="form-control select2">
                                                        @if(!empty($data_colaborador->area != ''))
                                                            @foreach($areas as $area)
                                                                @if($data_colaborador->area == $area->id)
                                                                    <option value="{{$area->id}}" selected>{{$area->titulo}}</option>
                                                                @else
                                                                    <option value="{{$area->id}}">{{$area->titulo}}</option>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <option value="" disabled selected>Seleccione una empresa</option>
                                                            @foreach($areas as $area)
                                                                <option value="{{$area->id}}">{{$area->titulo}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Fecha Nacimiento</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="date" name="fecha_nacimiento" class="form-control" value="{{$data_colaborador->fecha_nacimiento}}">
                                                    @else
                                                        <input type="text" name="fecha_nacimiento" class="form-control">
                                                    @endif

                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Peso</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="text" name="peso" class="form-control" value="{{$data_colaborador->peso}}">
                                                    @else
                                                        <input type="text" name="peso" class="form-control">
                                                    @endif

                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Altura</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="text" name="altura" class="form-control" value="{{$data_colaborador->altura}}">
                                                    @else
                                                        <input type="text" name="altura" class="form-control">
                                                    @endif

                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Empresa</label>
                                                    <select name="empresa" id="" class="select2 form-control">
                                                        @if(!empty($data_colaborador))
                                                            @foreach($empresas as $empresa)
                                                                @if($data_colaborador->id_empresa == $empresa->id)
                                                                    <option value="{{$empresa->id}}" selected>{{$empresa->title}}</option>
                                                                @else
                                                                    <option value="{{$empresa->id}}">{{$empresa->title}}</option>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <option value="" disabled selected>Seleccione una empresa</option>
                                                            @foreach($empresas as $empresa)
                                                                <option value="{{$empresa->id}}">{{$empresa->title}}</option>
                                                            @endforeach
                                                        @endif

                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo de Sangre</label>
                                                    <select name="sangre" id="" class="select2 form-control">

                                                        @if($data_colaborador->tipo_sangre == '')
                                                            <option value="" disabled selected>Seleccione un tipo de sangre</option>
                                                            <option value="O negativo">O negativo</option>
                                                            <option value="O positivo">O positivo</option>
                                                            <option value="A negativo">A negativo</option>
                                                            <option value="A positivo">A positivo</option>
                                                            <option value="B negativo">B negativo</option>
                                                            <option value="B positivo">B positivo</option>
                                                            <option value="AB negativo">AB negativo</option>
                                                            <option value="AB positivo">AB positivo</option>
                                                        @else
                                                            <option value="O negativo" @if($empresa->tipo_sangre == 'O negativo') selected @endif>O negativo</option>
                                                            <option value="O positivo" @if($empresa->tipo_sangre == 'O positivo') selected @endif>O positivo</option>
                                                            <option value="A negativo" @if($empresa->tipo_sangre == 'A negativo') selected @endif>A negativo</option>
                                                            <option value="A positivo" @if($empresa->tipo_sangre == 'A positivo') selected @endif>A positivo</option>
                                                            <option value="B negativo" @if($empresa->tipo_sangre == 'B negativo') selected @endif>B negativo</option>
                                                            <option value="B positivo" @if($empresa->tipo_sangre == 'B positivo') selected @endif>B positivo</option>
                                                            <option value="AB negativo" @if($empresa->tipo_sangre == 'AB negativo') selected @endif>AB negativo</option>
                                                            <option value="AB positivo" @if($empresa->tipo_sangre == 'AB positivo') selected @endif>AB positivo</option>
                                                        @endif


                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Enfermedades</label>

                                                    @if(!empty($data_colaborador))
                                                        <textarea class="form-control" name="enfermedades">{{$data_colaborador->enfermedades}}</textarea>
                                                    @else
                                                        <textarea class="form-control" name="enfermedades"></textarea>
                                                    @endif

                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Cantidad de días de elvaluación</label>
                                                    <input class="form-control" name="cdias" placeholder="cantidad de días de elvaluación" value="{{$usuario_data->dias}}">
                                                </div>

                                                <div class="col-md-12 form-group">
                                                    <label for="">Alérgico a algún medicamento</label>
                                                    <textarea class="form-control" name="alergico">{{$data_colaborador->alergico}}</textarea>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="file" type="file" class="d-none"  accept="image/x-png,image/gif,image/jpeg">
                                            <label for="file" class="input-file m-auto" id="file-foto">
                                                @if($usuario_data->foto)
                                                    <img src="{{$usuario_data->foto}}" alt="" class="img-fluid">
                                                @else
                                                    <i class="far fa-image"></i>
                                                @endif
                                            </label>
                                        </div>
                                        <div class="col-md-11">
                                            <hr>
                                        </div>
                                        <div class="col-md-11">
                                            <button class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>
                @endif
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tabla de usuarios</h3>
                            </div>
                            <div class="card-body">
                                <table class="table" id="example2">
                                    <thead>
                                    <th>Nº</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Tipo Usuario</th>
                                    <th>Opinion</th>
                                    </thead>
                                    <tbody>
                                    @php $c = 1; @endphp
                                    @foreach($usuarios as $usuario)
                                        <tr>
                                            <td>{{$c++}}</td>
                                            <td>{{$usuario->name}}</td>
                                            <td>{{$usuario->last_name}}</td>
                                            <td>{{$usuario->title}}</td>
                                            <td>
                                                <a href="{{route('user.edit',$usuario->id)}}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                                <button onclick="deleteUsuario({{$usuario->id}},'{{$usuario->name}} {{$usuario->last_name}}')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var filePreview = document.createElement('img');
                    filePreview.id = 'file-preview';
                    filePreview.className = 'img-fluid';
                    //e.target.result contents the base64 data from the image uploaded
                    filePreview.src = e.target.result;
                    console.log(e.target.result);

                    var previewZone = document.getElementById('file-foto');
                    $('#file-foto').html('')
                    previewZone.appendChild(filePreview);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file');

        fileUpload.onchange = function (e) {
            readFile(e.srcElement);
        }

        function addUser(id) {
            if(id == 1){
                $('#form-user-dash').removeClass('d-none')
                $('#form-user-colaboradores').addClass('d-none')
                $('#id_type_user').val(id)
                $('#type').html('Admin')
                $('.form-control').val('');
            }else if(id == 2){
                $('#form-user-dash').removeClass('d-none')
                $('#form-user-colaboradores').addClass('d-none')
                $('#id_type_user').val(id)
                $('#type').html('Medico')
                $('.form-control').val('');
            }else if(id == 3) {
                $('#form-user-colaboradores').removeClass('d-none')
                $('#form-user-dash').addClass('d-none')
                $('#id_type_user_colaboradores').val(id)
                $('#type').html('Colaborador')
                $('.form-control').val('');
            }
        }

        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });

        function deleteUsuario(id,name) {
            swal({
                title: `¿ Estas seguro que quieres eliminar a ${name} ?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '../../user/'+id,
                            type: 'delete',
                            dataType: "JSON",
                            data: {
                                "id": id
                            },
                            success: function (response)
                            {
                                if(response.success){
                                    swal(response.message, {
                                        icon: "success",
                                    });
                                    setTimeout(function () {
                                        location.reload();
                                    },1000)
                                }
                            },
                        })


                    }
                });
        }
    </script>
@endsection
