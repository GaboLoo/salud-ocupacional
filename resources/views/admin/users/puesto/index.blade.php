@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Puesto</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('user.index')}}">Usuarios</a></li>
                            <li class="breadcrumb-item active">Puesto</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                @if(auth()->user()->id_type_user == 1)
                    <div class="row" id="form-user-dash">
                        <section class="col-lg-12 connectedSortable ui-sortable">
                            <div class="card">
                                <div class="card-header ui-sortable-handle">
                                    <h3 class="card-title">Formulario de puestos</h3>
                                </div>
                                <div class="card-body">
                                    <form action="{{route('puesto.store')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="type_id" value="{{auth()->user()->id_type_user}}">
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="">Empresa</label>
                                                <select name="empresa" id="" class="form-control">
                                                    <option value="" selected disabled>Elegir una empresa</option>
                                                    @foreach($empresas as $empresa)
                                                        <option value="{{$empresa->id}}">{{$empresa->title}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="">Titulo</label>
                                                <input type="text" name="titulo" class="form-control">
                                            </div>
                                            <div class="form-group col-md-12 text-right mb-0">
                                                <button class="btn btn-primary" type="submit">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Lista de cargos</h3>
                    </div>
                    <div class="card-body">
                        <table class="table" id="example2">
                            <thead>
                            <th>Nº</th>
                            <th>Titulo</th>
                            <th>Empresa</th>
                            <th>Opciones</th>
                            </thead>
                            <tbody>
                            @php $c = 1 @endphp
                            @foreach($puestos as $puesto)
                                <tr>
                                    <td>{{$c++}}</td>
                                    <td>{{$puesto->titulo}}</td>
                                    <td>{{$puesto->empresa}}</td>
                                    <td>
                                        <a href="{{route('puesto.edit',$puesto->id)}}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                        <button onclick="deleteCargo({{$puesto->id}},'{{$puesto->titulo}}')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
        });

        function deleteCargo(id,name) {
            swal({
                title: `¿ Estas seguro que quieres eliminar a ${name} ?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '../../cargo/'+id,
                            type: 'delete',
                            dataType: "JSON",
                            data: {
                                "id": id
                            },
                            success: function (response)
                            {
                                if(response.success){
                                    swal(response.message, {
                                        icon: "success",
                                    });
                                    setTimeout(function () {
                                        location.reload();
                                    },1000)
                                }
                            },
                        })


                    }
                });
        }
    </script>
@endsection
