@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Lista de colaboradores</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('inicio')}}">Home</a></li>
                            <li class="breadcrumb-item active">Lista de colaboradores</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Lista de trabajadores</h3>
                            </div>
                            <div class="card-body">
                                <table class="table" id="example2">
                                    <thead>
                                        <th>Nº</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Celular</th>
                                        <th>Tipo Usuario</th>
                                        <th>Opinion</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                    @php $c = 1; @endphp
                                    @foreach($colaboradores as $colaborador)
                                        @if($colaborador->estado_salud == 1)
                                            @php $text = 'Sano'; $color = 'badge-success' @endphp
                                        @elseif($colaborador->estado_salud == 2)
                                            @php $text = 'Sospechoso'; $color = 'badge-warning' @endphp
                                        @elseif($colaborador->estado_salud == 3)
                                            @php $text = 'Activo coronavirus'; $color = 'badge-danger' @endphp
                                        @endif
                                        <tr>
                                            <td>{{$c++}}</td>
                                            <td>{{$colaborador->name}}</td>
                                            <td>{{$colaborador->last_name}}</td>
                                            <td>{{$colaborador->celular}}</td>
                                            <td>{{$colaborador->title}}</td>
                                            <td><span class="badge <?= $color ?>">{{$text}}</span></td>
                                            <td>
                                                <a href="{{route('seguimiento',$colaborador->id)}}" class="btn btn-default">Seguimiento</a>
                                                <a href="{{route('diagnosticar',$colaborador->id)}}" class="btn btn-primary">Diagnosticar</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@section('script')
    <script>
        $('#example2').DataTable();
    </script>
@endsection
