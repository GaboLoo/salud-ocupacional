@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Perfil de la Empresa</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('inicio')}}">Home</a></li>
                            <li class="breadcrumb-item active">Perfil de la Empresa</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Datos de la empresa</h3>
                            </div>
                            <div class="card-body">
                                <form action="{{route('empresa.update',$dato_user->id_empresa)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" value="empresa.perfil" name="route">
                                    <div class="row">
                                        <div class="col-md-4 form-group">
                                            <label for="">Nombre</label>
                                            <input type="text" name="nombre" class="form-control" value="{{$dato->title}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">R.U.C</label>
                                            <input type="text" name="ruc" class="form-control" value="{{$dato->ruc}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Dirección</label>
                                            <input type="text" name="direccion" class="form-control" value="{{$dato->direccion}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Telefono</label>
                                            <input type="text" name="telefono" class="form-control" value="{{$dato->telefono}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Correo</label>
                                            <input type="email" name="correo" class="form-control" value="{{$dato->correo}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Rugro</label>
                                            <select name="rugro" id="" class="form-control">

                                                @foreach($rugros as $rugro)
                                                    @if($rugro->id  == $dato->id_rugro)
                                                        <option value="{{$rugro->id}}" selected>{{$rugro->titulo}}</option>
                                                    @else
                                                        <option value="{{$rugro->id}}">{{$rugro->titulo}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

@endsection

@section('script')
    <script>
        $('#example2').DataTable();
    </script>
@endsection
