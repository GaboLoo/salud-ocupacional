@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Empresas</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="">Home</a></li>
                            <li class="breadcrumb-item active">Empresas</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Formulario de registro de empresas</h3>
                            </div>
                            <div class="card-body">
                                <form action="{{route('empresa.update',$id)}}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" value="empresa.index" name="route">
                                    <div class="row">
                                        <div class="col-md-4 form-group">
                                            <label for="">Nombre</label>
                                            <input type="text" name="nombre" class="form-control" value="{{$dato->title}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">R.U.C</label>
                                            <input type="text" name="ruc" class="form-control" value="{{$dato->ruc}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Dirección</label>
                                            <input type="text" name="direccion" class="form-control" value="{{$dato->direccion}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Telefono</label>
                                            <input type="text" name="telefono" class="form-control" value="{{$dato->telefono}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Correo</label>
                                            <input type="email" name="correo" class="form-control" value="{{$dato->correo}}">
                                        </div>
                                        <div class="col-md-4 form-group">
                                            <label for="">Rugro</label>
                                            <select name="rugro" id="" class="form-control">

                                                @foreach($rugros as $rugro)
                                                    @if($rugro->id  == $dato->id_rugro)
                                                        <option value="{{$rugro->id}}" selected>{{$rugro->titulo}}</option>
                                                    @else
                                                        <option value="{{$rugro->id}}">{{$rugro->titulo}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <hr>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tabla de Empresas</h3>
                            </div>
                            <div class="card-body">
                                <table class="table" id="example2">
                                    <thead>
                                    <th>Nº</th>
                                    <th>Nombre</th>
                                    <th>R.U.C</th>
                                    <th>Dirección</th>
                                    <th>Teléfono</th>
                                    <th>Opciones</th>
                                    </thead>
                                    <tbody>
                                    @php $c = 1 @endphp
                                    @foreach($empresas as $empresa)
                                        <tr>
                                            <td>{{$c++}}</td>
                                            <td>{{$empresa->title}}</td>
                                            <td>{{$empresa->ruc}}</td>
                                            <td>{{$empresa->direccion}}</td>
                                            <td>{{$empresa->telefono}}</td>
                                            <td>
                                                <a href="{{route('empresa.edit',$empresa->id)}}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                                <button onclick="deleteEmpresa({{$empresa->id}},'{{$empresa->title}}')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                                <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" onclick="importacion({{$empresa->id}})"><i class="fas fa-file-excel"></i></button>
                                                <a href="{{route('empresa.show',$empresa->id)}}" class="btn btn-default"><i class="fas fa-external-link-alt"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="{{route('import.user')}}" enctype="multipart/form-data" method="post">
                @csrf
                <input type="hidden" id="id_empresa" value="" name="id_empresa">
                <input type="hidden" id="route" value="empresa.index" name="route">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Importar Personal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <a href="{{asset('orden-importacion.xlsx')}}" type="button" download>Descargar orden de importacion</a>
                        </div>
                        <div class="form-group">
                            <p><strong>Observaciones</strong> <br> El la columna tipo_usuario colocar: <br> 1 si es el administrador de la empresa
                                <br> 2 si son los colaboradores</p>
                        </div>
                        <div class="form-group">
                            <input type="file" name="personimport" accept=".xlsx,.xls">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
            </form>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script>
        function deleteEmpresa(id,title) {
            swal({
                title: `¿ Estas seguro que quieres eliminar a ${title} ?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: 'empresa/'+id,
                            type: 'delete',
                            dataType: "JSON",
                            data: {
                                "id": id
                            },
                            success: function (response)
                            {
                                if(response.success){
                                    swal(response.message, {
                                        icon: "success",
                                    });
                                    setTimeout(function () {
                                        location.reload();
                                    },1000)
                                }
                            },
                        })


                    }
                });
        }
        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
        });

        function importacion(id_empresa) {
            $('#id_empresa').val(id_empresa);
        }
    </script>
@endsection
