@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Lista de usuario</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Empresa</a></li>
                            <li class="breadcrumb-item active">Lista de usuario</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->


                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Importar usuarios</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Tabla de usuarios</h3>
                            </div>
                            <div class="card-body">
                                <table class="table" id="example2">
                                    <thead>
                                    <th>Nº</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Documento</th>
                                    <th>Tipo Usuario</th>
                                    <th>Opinion</th>
                                    </thead>
                                    <tbody>
                                    @php $c = 1; @endphp
                                    @foreach($usuarios as $usuario)
                                        <tr>
                                            <td>{{$c++}}</td>
                                            <td>{{$usuario->name}}</td>
                                            <td>{{$usuario->last_name}}</td>
                                            <td>{{$usuario->documento}}</td>
                                            <td>{{$usuario->title}}</td>
                                            <td>
                                                <a href="{{route('user.edit',$usuario->id)}}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                                <button onclick="deleteUsuario({{$usuario->id}},'{{$usuario->name}} {{$usuario->last_name}}')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <form action="{{route('import.user')}}" enctype="multipart/form-data" method="post">
                @csrf
                <input type="hidden" id="id_empresa" value="{{$id}}" name="id_empresa">
                <input type="hidden" id="route" value="empresa.show" name="route">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Importar Personal</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <a href="{{asset('orden-importacion.xlsx')}}" type="button" download>Descargar orden de importacion</a>
                        </div>
                        <div class="form-group">
                            <p><strong>Observaciones</strong> <br> El la columna tipo_usuario colocar: <br> 1 si es el administrador de la empresa
                                <br> 2 si son los colaboradores</p>
                        </div>
                        <div class="form-group">
                            <input type="file" name="personimport" accept=".xlsx,.xls">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
            </form>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script>

        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });

        function deleteUsuario(id,name) {
            swal({
                title: `¿ Estas seguro que quieres eliminar a ${name} ?`,
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '../../user/'+id,
                            type: 'delete',
                            dataType: "JSON",
                            data: {
                                "id": id
                            },
                            success: function (response)
                            {
                                if(response.success){
                                    swal(response.message, {
                                        icon: "success",
                                    });
                                    setTimeout(function () {
                                        location.reload();
                                    },1000)
                                }
                            },
                        })


                    }
                });
        }
    </script>
@endsection
