@extends('layouts.app')

@section('title','Reporte estado de salud')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Reporte estado de salud </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Reportes</li>
                            <li class="breadcrumb-item active">Reporte estado de salud</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
               

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <div class="content">
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-header ui-sortable-handle ">
                            <div class="row align-items-center">
                                <div class="col-md-10">
                                    <h3 class="card-title">Estado de salud</h3>
                                </div>
                                <div class="col-md-2 text-right">
                                <a href="{{route('export',[1,$fecha1,$fecha2])}}" target="_blank" class="btn btn-info btn-sm" id="exportar1">Exportar</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-center justify-content-center" id="reportegrafico1">

                                <div class="col-md-12">
                                    <div class="row justify-content-center">
                                    <div class="col-md-3"><input type="date" name="fecha1" id="fecha1" class="form-control" value="{{ $fecha1 }}"></div>
                                        <div class="col-md-3"><input type="date" name="fecha2" id="fecha2" class="form-control" value="{{ $fecha2 }}"></div>
                                        <div class="col-md-1"><button class="btn btn-primary" onclick="fecha()">Consultar</button></div>
                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-5 col-lg-6 mt-5 order-1 order-md-0">
                                    <table class="table table-bordered">
                                        <thead>
                                            <th>Estado de salud</th>
                                            <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Trabajadores sanos</td>
                                                <td>{{$estado_salud_1}}</td>
                                            </tr>
                                            <tr>
                                                <td>Casos bajo sospecha</td>
                                                <td>{{$estado_salud_2}}</td>
                                            </tr>
                                            <tr>
                                                <td>Casos activos detectados</td>
                                                <td>{{$estado_salud_3}}</td>
                                            </tr>
                                            <tr>
                                                <td>Reincorporados</td>
                                                <td>{{$estado_salud_4}}</td>
                                            </tr>
                                            <tr>
                                                <td>Total de trabajadores</td>
                                                <td>{{ $total_trabajadores }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-8 col-md-7 col-lg-6">
                                    <canvas id="canvas"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
{{--     
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-header ui-sortable-handle">
                            <h3 class="card-title">Seguimiento de personal bajo sospecha y positivos COVID</h3>
                        </div>
                        <div class="card-body">
                            <div class="row" id="">
                                <div class="col-md-6">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Seguimiento</th>
                                        <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Contactados</td>
                                            <td>{{$seguimiento1}}</td>
                                        </tr>
                                        <tr>
                                            <td>No respondieron llamadas</td>
                                            <td>{{$seguimiento2}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="canvas2"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-header ui-sortable-handle">
                            <h3 class="card-title">Cumplimiento de llenado de Formulario de seguimiento	</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Seguimiento</th>
                                        <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Cumplen con el seguimiento diario</td>
                                            <td>{{$cumplimiento1}}</td>
                                        </tr>
                                        <tr>
                                            <td>No cumplen</td>
                                            <td>{{$cumplimiento2}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="canvas3"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> --}}
        </div>
    </div>
  
@endsection

@section('script')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

    <script>
        var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var color = Chart.helpers.color;
        var horizontalBarChartData = {
            labels: ['Trabajadores sanos', 'Casos bajo sospecha', 'Casos activos detectados', 'Reincorporados'],
            datasets: [{
                label: 'Estado',
                backgroundColor: 'rgba(60,141,188,0.9)',
                borderColor: 'rgba(60,141,188,0.9)',
                borderWidth: 1,
                data: [{{$estado_salud_1}}, {{$estado_salud_2}}, {{$estado_salud_3}}, {{$estado_salud_4}}]
            }]

        };

        var ctx = document.getElementById('canvas').getContext('2d');
        window.myHorizontalBar = new Chart(ctx, {
            type: 'horizontalBar',
            data: horizontalBarChartData,
            options: {
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    rectangle: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                legend: {
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Estado de salud de los Trabajadores'
                }
            }
        });
    </script>

  
    <script>
      

        function fecha(){
            
            let fecha1 = $('#fecha1').val();
            let fecha2 = $('#fecha2').val();

            if(fecha1 != '' && fecha2 != ''){
                let url = location.origin;
                location.href = url+'/reportes/1/'+fecha1+'/'+fecha2;
            }else{
                swal("Error", "Debe de seleccionar las dos fechas", "error");
            }

        }
    </script>
@endsection
