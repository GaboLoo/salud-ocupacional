@extends('layouts.app')

@section('title','Reporte de cumplimiento')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Reporte de cumplimiento </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Reportes</li>
                            <li class="breadcrumb-item active">Cumplimiento </li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
               

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <div class="content">
    
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-header ui-sortable-handle ">
                            <div class="row align-items-center">
                                <div class="col-md-10">
                                    <h3 class="card-title">Cumplimiento de llenado de Formulario de seguimiento	</h3>
                                </div>
                                <div class="col-md-2 text-right">
                                <a href="{{route('export',[3,$fecha1,$fecha2])}}" target="_blank" class="btn btn-info btn-sm" id="exportar1">Exportar</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-md-12 mb-5">
                                    <div class="row justify-content-center">
                                    <div class="col-md-3"><input type="date" name="fecha1" id="fecha1" class="form-control" value="{{ $fecha1 }}"></div>
                                        <div class="col-md-3"><input type="date" name="fecha2" id="fecha2" class="form-control" value="{{ $fecha2 }}"></div>
                                        <div class="col-md-1"><button class="btn btn-primary" onclick="fecha()">Consultar</button></div>
                                    </div>
                                </div>

                                <div class="col-md-6 mt-5">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Seguimiento</th>
                                        <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Cumplen con el seguimiento diario</td>
                                            <td>{{$cumplimiento1}}</td>
                                        </tr>
                                        <tr>
                                            <td>No cumplen</td>
                                            <td>{{$total_trabajadores - $cumplimiento1}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="canvas3"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
  
@endsection

@section('script')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

    <script>

        var config2 = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        {{$cumplimiento1}},{{($total_trabajadores - $cumplimiento1)}}
                    ],
                    backgroundColor: ['#00c0ef', '#f39c12'],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Cumplen con el seguimiento diario',
                    'No cumplen',
                ]
            },
            options: {
                responsive: true
            }
        };

            var ctx2 = document.getElementById('canvas3').getContext('2d');
            var mychaart4 = new Chart(ctx2, config2);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script>
        
        function fecha(){
            
            let fecha1 = $('#fecha1').val();
            let fecha2 = $('#fecha2').val();

            if(fecha1 != '' && fecha2 != ''){
                let url = location.origin;
                location.href = url+'/reportes/3/'+fecha1+'/'+fecha2;
            }else{
                swal("Error", "Debe de seleccionar las dos fechas", "error");
            }

        }
    </script>
@endsection
