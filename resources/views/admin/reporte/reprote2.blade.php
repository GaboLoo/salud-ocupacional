@extends('layouts.app')

@section('title','Reporte seguimiento de personal')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Reporte seguimiento de personal </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Reportes</li>
                            <li class="breadcrumb-item active">Reporte seguimiento de personal </li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
               

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <div class="content">
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-header ui-sortable-handle ">
                            <div class="row align-items-center">
                                <div class="col-md-10">
                                    <h3 class="card-title">Seguimiento de personal bajo sospecha y positivos COVID	</h3>
                                </div>
                                <div class="col-md-2 text-right">
                                <a href="{{route('export',[2,$fecha1,$fecha2])}}" target="_blank" class="btn btn-info btn-sm" id="exportar1">Exportar</a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row" id="">


                                <div class="col-md-12 mb-5">
                                    <div class="row justify-content-center">
                                    <div class="col-md-3"><input type="date" name="fecha1" id="fecha1" class="form-control" value="{{ $fecha1 }}"></div>
                                        <div class="col-md-3"><input type="date" name="fecha2" id="fecha2" class="form-control" value="{{ $fecha2 }}"></div>
                                        <div class="col-md-1"><button class="btn btn-primary" onclick="fecha()">Consultar</button></div>
                                    </div>
                                </div>


                                <div class="col-md-6 mt-5 ">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Seguimiento</th>
                                        <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Contactados</td>
                                            <td>{{$seguimiento1}}</td>
                                        </tr>
                                        <tr>
                                            <td>No respondieron llamadas</td>
                                            <td>{{$total_trabajadores - $seguimiento1}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="canvas2"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    
            {{-- <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-header ui-sortable-handle">
                            <h3 class="card-title">Cumplimiento de llenado de Formulario de seguimiento	</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Seguimiento</th>
                                        <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Cumplen con el seguimiento diario</td>
                                            <td>{{$cumplimiento1}}</td>
                                        </tr>
                                        <tr>
                                            <td>No cumplen</td>
                                            <td>{{$cumplimiento2}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="canvas3"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> --}}
        </div>
    </div>
  
@endsection

@section('script')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

    <script>

        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        {{$seguimiento1}},{{($total_trabajadores - $seguimiento1)}}
                    ],
                    backgroundColor: ['#f56954', '#00a65a'],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Contactados',
                    'No respondieron llamadas',
                ]
            },
            options: {
                responsive: true
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('canvas2').getContext('2d');
            var mychaart2 = new Chart(ctx, config);
        };

       
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script>
        $('#exportar1').click(function(event) {
            // get size of report page
            var reportPageHeight = $('#reportegrafico1').innerHeight();
            var reportPageWidth = $('#reportegrafico1').innerWidth();

            // create a new canvas object that we will populate with all other canvas objects
            var pdfCanvas = $('<canvas />').attr({
                id: "canvaspdf",
                width: reportPageWidth,
                height: reportPageHeight
            });

            // keep track canvas position
            var pdfctx = $(pdfCanvas)[0].getContext('2d');
            var pdfctxX = 0;
            var pdfctxY = 0;
            var buffer = 100;

            // for each chart.js chart
            $("canvas").each(function(index) {
                // get the chart height/width
                var canvasHeight = $(this).innerHeight();
                var canvasWidth = $(this).innerWidth();

                // draw the chart into the new canvas
                pdfctx.drawImage($(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
                pdfctxX += canvasWidth + buffer;

                // our report page is in a grid pattern so replicate that in the new canvas
                if (index % 2 === 1) {
                    pdfctxX = 0;
                    pdfctxY += canvasHeight + buffer;
                }
            });
            //
            // create new pdf and add our new canvas as an image
            var pdf = new jsPDF('l', 'pt', [reportPageWidth, reportPageHeight]);
            pdf.addImage($(pdfCanvas)[0], 'PNG', 0, 0);
            //
            // // download the pdf
            // pdf.save('Estado_de_salud_de_los_Trabajadores.pdf');
        });


        function fecha(){
            
            let fecha1 = $('#fecha1').val();
            let fecha2 = $('#fecha2').val();

            if(fecha1 != '' && fecha2 != ''){
                let url = location.origin;
                location.href = url+'/reportes/2/'+fecha1+'/'+fecha2;
            }else{
                swal("Error", "Debe de seleccionar las dos fechas", "error");
            }

        }
    </script>
@endsection
