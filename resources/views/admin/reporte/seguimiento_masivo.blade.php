@extends('layouts.app')

@section('title','Reporte de seguimiento masivo')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Reporte de seguimiento masivo </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Reportes</li>
                            <li class="breadcrumb-item active">Seguimiento Masivo </li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
               

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <div class="content">
    
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-header ui-sortable-handle ">
                            <div class="row align-items-center">
                                <div class="col-md-10">
                                    <h3 class="card-title">Lista de seguimiento masivo</h3>
                                </div>
                                
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-md-12 mb-5">
                                    <div class="row justify-content-center">
                                    <div class="col-md-3"><input type="date" name="fecha1" id="fecha1" class="form-control" value="{{ $fecha1 }}"></div>
                                    <div class="col-md-3"><input type="date" name="fecha2" id="fecha2" class="form-control" value="{{ $fecha2 }}"></div>
                                    <div class="col-md-3">
                                        @if ($id_empresa != 0)
                                            <select name="empresa" id="empresa" class="form-control select2">
                                                <option value="" disabled>Todos</option>
                                                @foreach ($empresas as $empresa)
                                                    @if ($empresa->id == $id_empresa)
                                                        <option value="{{$empresa->id}}" selected>{{$empresa->title}}</option>
                                                    @else
                                                        <option value="{{$empresa->id}}">{{$empresa->title}}</option>
                                                    @endif
                                                    
                                                @endforeach
                                            </select>
                                        @else
                                            <select name="empresa" id="empresa" class="form-control select2">
                                                <option value="" disabled selected>Todos</option>
                                                @foreach ($empresas as $empresa)
                                                    <option value="{{$empresa->id}}">{{$empresa->title}}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                        
                                    </div>
                                    <div class="col-md-1"><button class="btn btn-primary" onclick="fecha()">Consultar</button></div>
                                    </div>
                                </div>

                                <div class="col-md-12 mt-5">
                                    <table class="table table-bordered" id="example2">
                                        <thead>
                                            <th>DNI</th>
                                            <th>Apellidos</th>
                                            <th>Nombre</th>
                                            <th>Fecha</th>
                                            <th>Hora</th>
                                            <th>Fase</th>
                                        </thead>
                                        <tbody>
                                            @foreach ($lista as $item)
                                                <tr>
                                                    <td>{{$item->documento}}</td>
                                                    <td>{{$item->last_name}}</td>
                                                    <td>{{$item->name}}</td>
                                                    <td>{{date('d-m-Y', strtotime($item->fecha_hr))}}</td>
                                                    <td>{{date('H:s:i', strtotime($item->fecha_hr))}}</td>
                                                    <td>{{$item->name_table}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
  
@endsection

@section('script')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

    <script>

        
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script>
        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });
        function fecha(){
            
            let fecha1 = $('#fecha1').val();
            let fecha2 = $('#fecha2').val();
            let id_empresa = $('#empresa').val();

            if(fecha1 != '' && fecha2 != '' && id_empresa != ''){
                let url = location.origin;
                location.href = url+'/reportes/4/'+fecha1+'/'+fecha2+'/'+id_empresa;
            }else{
                swal("Error", "Debe de seleccionar las dos fechas", "error");
            }

        }
    </script>
@endsection
