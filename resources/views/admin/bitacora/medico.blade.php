@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Bideoteca</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Medico</a></li>
                            <li class="breadcrumb-item active">Bideoteca</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                        @if (session('id_hdm'))
                            <a href="{{route('reincorporacion-laboral-pdf',session('id_hdm'))}}" target="_blank" class="btn btn-dark ml-5">Ver documento de reincorporación</a>
                        @endif
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                {{-- <div class="row mb-3">
                    <div class="col-md-3">
                        <button class="btn btn-success" onclick="generarDescansoMedico()">Generar descanso medico</button>
                    </div>
                </div> --}}

                @if(auth()->user()->id_type_user == 2)
                    <div class="row mb-4" id="form-user-colaboradores">
                        <section class="col-lg-12 connectedSortable ui-sortable">
                            <div class="card">
                                <div class="card-header ui-sortable-handle">
                                    <h3 class="card-title">Formulario de Bitacora</h3>
                                </div>
                                <div class="card-body">
                                    <form action="{{ route('bitacora-medico-save') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row justify-content-center">
                                            
                                            <div class="col-md-6 form-group">
                                                <label for="">Fecha y Hora</label>
                                                <input type="text" name="nombre" class="form-control" disabled value="{{ date('d/m/Y H:i:s') }}">
                                                <input type="hidden" name="fecha" class="form-control" value="{{ date('d/m/Y H:i:s') }}">
                                            </div>
                                                
                                            <div class="col-md-6 form-group">
                                                <label for="">Empresa</label>
                                                <select name="empresa" id="empresa" class="select2 form-control">
                                                    <option value="" disabled selected>Escoger una empresa</option>
                                                    @foreach ($empresas as $empresa)
                                                        <option value="{{$empresa->id}}">{{$empresa->title}}</option>   
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                            <div class="col-md-12 form-group">
                                                <label for="">Resumen del Día</label>
                                                <textarea class="form-control" rows="10" cols="50" name="resumen"></textarea>
                                            </div>
                                            
                                            <div class="col-md-12 text-right">
                                                <button class="btn btn-primary">Guardar</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </section>
                    </div>
                @endif
                
                
                <div class="row mb-4" id="form-user-colaboradores">
                    <section class="col-lg-12 connectedSortable ui-sortable">
                        <div class="card">
                            <div class="card-header ui-sortable-handle">
                                <h3 class="card-title">Lista de Bitacora</h3>
                            </div>
                            <div class="card-body">
                                
                                    <div class="row justify-content-center mb-5">
                                        
                                        <div class="col-md-4 form-group">
                                            <label for="">Fecha Inicio</label>
                                            <input type="date" id="fecha1" name="fechainicio" class="form-control">
                                        </div>
                                            
                                        <div class="col-md-4 form-group">
                                            <label for="">Fecha Fin</label>
                                             <input type="date" id="fecha2" name="fechainicio" class="form-control">
                                        </div>
                                        
                                        <div class="col-md-4 form-group">
                                            <label for="">Empresas</label>
                                            <select name="empresa" id="empresa_select" class="select2 form-control">
                                                <option value="" disabled selected>Escoger una empresa</option>
                                                @foreach ($empresas as $empresa)
                                                    <option value="{{$empresa->id}}">{{$empresa->title}}</option>   
                                                @endforeach
                                            </select>
                                        </div>
                                        
                                        
                                        <div class="col-md-12 text-right">
                                            <button class="btn btn-primary" onclick="consultar()">Consultar</button>
                                        </div>
                                    </div>
                                
                                <div id="content_table">
                                    <table class="table mt-5" id="example2">
                                        <thead>
                                            <th>Nº</th>
                                            <th>Fecha</th>
                                            <th>Empresa</th>
                                            <th>Resumen</th>
                                        </thead>
                                        <tbody>
                                            @php $c = 1; @endphp
                                            @foreach($bitacoras as $item)
                                                <tr>
                                                    <td>{{ $c }}</td>
                                                    <td>{{ $item->created_at }}</td>
                                                    <td>{{ $item->empresa }}</td>
                                                    <td>{{ $item->mensaje }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });
        
        function consultar(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                        
            let fecha1 = $('#fecha1').val();
            let fecha2 = $('#fecha2').val();
            let empresa = $('#empresa_select').val();
                        

            $.ajax({
                url: "https://medilaboris.net.pe/bitacora-medico-consult",
                type: 'post',
                dataType: "JSON",
                data: {
                    fecha_inicio : fecha1,
                    fecha_fin: fecha2,
                    empresa: empresa
                },
                success: function (response)
                {
                    $('#content_table').html(response)
                    $("#example2").DataTable({
                        "responsive": true,
                        "autoWidth": false,
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ]
                    });
                },
            })
        }
        
    </script>
@endsection