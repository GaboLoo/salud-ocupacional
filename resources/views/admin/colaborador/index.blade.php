@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Encuesta</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Colaborador</li>
                            <li class="breadcrumb-item active">Encuesta</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <p>Corrige los siguientes errores:</p>
                                <ul>
                                    @foreach ($errors->all() as $message)
                                        <li>{{ $message }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                            @if($bloqueo)
                                <div class="alert alert-info">
                                    No se encuentra ninguna encuesta pendiente
                                </div>
                            @endif


                            @if($form == 'riesgos')
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">¿A qué grupo de riesgo perteneces?</h3>
                                    </div>
                                    <form action="{{route('form-colaborador.store')}}"  method="post">
                                        @csrf
                                        <input type="hidden" name="type_form" value="0">
                                        <div class="card-body">

                                            @foreach($riesgos as $riesgo)
                                                <div class="form-group">
                                                    <input type="checkbox" class="mr-2" name="riesgos[]" value="{{$riesgo->id}}" id="{{$riesgo->id}}"  onclick="select_riego({{$riesgo->id}})">
                                                    <label for="{{$riesgo->id}}">{{$riesgo->title}}</label>
                                                </div>
                                            @endforeach
                                                <div class="form-group">
                                                    <label for="otras">Otras Enfermedades</label>
                                                    <textarea class="form-control" name="riesgos[]" placeholder="Ingresa otras enfermedades"></textarea>
                                                </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button type="submit" class="btn btn-primary">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            @endif

                            @if($form == 'fase1' && $bloqueo == false)
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Fase 1</h3>
                                    </div>
                                    <form action="{{route('form-colaborador.store')}}"  method="post">
                                        @csrf
                                        <input type="hidden" name="type_form" value="1">
                                        <div class="card-body">
                                            <ol class="m-0 pl-3 mb-4" >
                                                <li>Marque usted si presenta algunos de los siguientes síntomas</li>
                                            </ol>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Sensación de alza térmica o fiebre" id="1" class="mr-2" onclick="select_options(1)">
                                                <label for="1">Sensación de alza térmica o fiebre</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Dificultad para respirar" id="2" class="mr-2" onclick="select_options(2)">
                                                <label for="2">Dificultad para respirar</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Secreción o congestión nasal" id="3" class="mr-2" onclick="select_options(3)">
                                                <label for="3">Secreción o congestión nasal</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Tos" id="4" class="mr-2" onclick="select_options(4)">
                                                <label for="4">Tos</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Estornudos" id="5" class="mr-2" onclick="select_options(5)">
                                                <label for="5">Estornudos</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Expectoración o flema amarilla o verdosa" id="6" class="mr-2" onclick="select_options(6)">
                                                <label for="6">Expectoración o flema amarilla o verdosa</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Pérdida del olfato" id="7" class="mr-2" onclick="select_options(7)">
                                                <label for="7">Pérdida del olfato</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Pérdida del gusto" id="8" class="mr-2" onclick="select_options(8)">
                                                <label for="8">Pérdida del gusto</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="Malestar general" id="9" class="mr-2" onclick="select_options(9)">
                                                <label for="9">Malestar general</label>
                                            </div>
                                            <div class="form-group">
                                            <input type="checkbox" name="fase1[]" value="Dolor de cabeza" id="10" class="mr-2" onclick="select_options(10)">
                                            <label for="10">Dolor de cabeza</label>
                                        </div>
                                        <div class="form-group">
                                            <input type="checkbox" name="fase1[]" value="Diarrea" id="11" class="mr-2" onclick="select_options(11)">
                                            <label for="11">Diarrea</label>
                                        </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="fase1[]" value="No presento síntomas" id="12" class="mr-2" onclick="select_options(12)">
                                                <label for="12">No presento síntomas</label>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-primary">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            @endif

                            @if($form == 'fase2' && $bloqueo == false)
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Fase 2</h3>
                                    </div>
                                    <form action="{{route('form-colaborador.store')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="type_form" value="2">
                                        <div class="card-body">
                                            <ol class="pl-3 m-0">
                                                <li>
                                                    ¿Estuviste en contacto con alguien a quién se le confirmó que tenía COVID-19 dentro de los 14 días de haber iniciado con los síntomas?
                                                    <br><br>
                                                    <div class="form-group">
                                                        <input type="radio" name="contacto" id="si" value="si" class="mr-2">
                                                        <label for="si">Si</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="radio" name="contacto" id="no" value="no" class="mr-2">
                                                        <label for="no">No</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="radio" name="contacto" id="nolose" value="no lo se" class="mr-2">
                                                        <label for="nolose">No lo se</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    ¿Cuándo iniciaron los síntomas?
                                                    <br><br>
                                                    <div class="form-group row">
                                                        <label for="" class="col-md-2">Fecha:</label>
                                                        <div class="col-md-5">
                                                            <input type="date" name="fecha" class="form-control">
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    ¿Realizó aislamiento social?
                                                    <br><br>
                                                    <div class="form-group">
                                                        <input type="radio" name="aislamiento" id="si-aislamiento" value="si" class="mr-2">
                                                        <label for="si-aislamiento">Si</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="radio" name="aislamiento" id="no-aislamiento" value="no" class="mr-2">
                                                        <label for="no-aislamiento">No</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    Por favor colocar en el siguiente cuadro los datos de las personas con las que tuvo contacto cuando inició con los síntomas:
                                                    <br><br>
                                                    <div class="form-personas">
                                                        <input type="hidden" value="1" id="cant_person">
                                                        <div class="content-person mb-3">
                                                            <p>Persona 1</p>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[0][nombre]" placeholder="Nombre">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[0][dni]" placeholder="DNI">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[0][edad]" placeholder="Edad">
                                                            </div>
                                                            <div class="form-group">
                                                                <select name="persona[0][sexo]" id="" class="form-control">
                                                                    <option value="" disabled selected>Sexo</option>
                                                                    <option value="Masculino">Masculino</option>
                                                                    <option value="Femenino">Femenino</option>
                                                                    <option value="Otro">Otro</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[0][telefono]" placeholder="Telefono">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[0][direccion]" placeholder="Direccón">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="button" class="btn btn-primary btn-block" onclick="addContacto()">Agregar otro contacto</button>
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-primary">Guardar</button>
                                        </div>
                                    </form>
                                </div>
                            @endif

                            @if($form == 'fase3' && $bloqueo == false)
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Fase 3</h3>
                                    </div>
                                    <div class="card-body">
                                        <p>1.- Califique sus síntomas del 1 al 5, dónde 1 significa la presencia nula del síntoma y 5 la presencia máxima del síntoma</p>
                                        <table  class="table">
                                            <thead>
                                                <th>Nº</th>
                                                <th>Preunta</th>
                                                <th>1</th>
                                                <th>2</th>
                                                <th>3</th>
                                                <th>4</th>
                                                <th>5</th>
                                            </thead>
                                            <tbody>
                                                <form action="{{ route('form-colaborador.store') }}" method="post">
                                                    <input type="hidden" name="type_form" value="3">
                                                    @csrf
                                                    @php $c = 1  @endphp
                                                    @foreach($preguntas as $pregunta)
                                                        <tr>
                                                            <td>{{$c++}}</td>
                                                            <td>{{ $pregunta->pregunta }}</td>
                                                            <th><input type="radio" name="pregunta[{{$pregunta->id}}]" value="1"></th>
                                                            <th><input type="radio" name="pregunta[{{$pregunta->id}}]" value="2"></th>
                                                            <th><input type="radio" name="pregunta[{{$pregunta->id}}]" value="3"></th>
                                                            <th><input type="radio" name="pregunta[{{$pregunta->id}}]" value="4"></th>
                                                            <th><input type="radio" name="pregunta[{{$pregunta->id}}]" value="5"></th>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="7" class="text-right">
                                                            <button class="btn btn-primary">Enviar</button>
                                                        </td>
                                                    </tr>
                                                </form>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            @endif

                            @if($form == 'fase4' && $bloqueo == false)
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Fase 4</h3>
                                    </div>
                                    <form action="{{ route('form-colaborador.store') }}" method="post">
                                        <input type="hidden" name="type_form" value="4">
                                        @csrf
                                        <div class="card-body">
                                            <p>1.- ¿Se le realizó algún tipo de prueba diagnóstica para el COVID-19?</p>
                                            <div class="form-group">
                                                <input type="radio" id="si-p1" value="si" name="p1" class="mr-2">
                                                <label for="si-p1">Si</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" id="no-p1" value="no" name="p1" class="mr-2">
                                                <label for="no-p1">No</label>
                                            </div>
                                            <div class="pl-5">
                                                <p>1.1.- Si tu respuesta es si, colocar que tipo:</p>
                                                <div class="form-group">
                                                    <input type="radio" id="rapida" value="si" name="tipo" class="mr-2">
                                                    <label for="rapida">Rapida</label>
                                                </div>
                                                <div class="form-group">
                                                    <input type="radio" id="molecular" value="no" name="tipo" class="mr-2">
                                                    <label for="molecular">Molecular</label>
                                                </div>
                                                <div class="form-group">
                                                    <label for="molecular">Fecha de toma de muestra</label>
                                                    <input type="date" class="form-control" name="fecha_de_toma">
                                                </div>
                                                <p>1.2.- ¿Cómo cree que se infecto?</p>
                                                <div class="form-group">
                                                    <textarea name="como_se_infecto" id="como_se_infecto" cols="30" rows="5" class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <p>2.- ¿Necesito ser hospitalizado?</p>
                                            <div class="form-group">
                                                <input type="radio" id="si-hospital" value="si" name="hospital" class="mr-2">
                                                <label for="si-hospital">Si</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" id="no-hospital" value="no" name="hospital" class="mr-2">
                                                <label for="no-hospital">No</label>
                                            </div>
                                            <div class="form-group">
                                                <label for="molecular">Fecha de hospitalización</label>
                                                <input type="date" class="form-control" name="fecha_hospitalizacion">
                                            </div>
                                            <div class="form-group" >
                                                <label for="molecular">Fecha de alta</label>
                                                <input type="date" class="form-control" name="fecha_alta">
                                            </div>
                                            <p>3.- ¿Tuvo alguna complicación en la hospitalización?</p>
                                            <div class="form-group">
                                                <input type="radio" id="si-hospital-complicacion" value="si" name="hospital-complicacion" class="mr-2">
                                                <label for="si-hospital-complicacion">Si</label>
                                            </div>
                                            <div class="form-group">
                                                <input type="radio" id="no-hospital-complicacion" value="no" name="hospital-complicacion" class="mr-2">
                                                <label for="no-hospital-complicacion">No</label>
                                            </div>
                                            <div class="form-group">
                                                <label>Coloque Cual</label>
                                                <textarea name="complicaciones" id="complicaciones" cols="30" rows="5" class="form-control"></textarea>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <button class="btn btn-primary">Enviar</button>
                                        </div>
                                    </form>
                                </div>
                            @endif
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        function addContacto() {
            console.log('hola estas agregando mas contaactos')
            let  cant_person = $('#cant_person').val();

            $('.form-personas').append(`<div class="content-person mb-3">
                                                            <p>Persona ${Number(cant_person) + 1}</p>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[${cant_person}][nombre]" placeholder="Nombre">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[${cant_person}][dni]" placeholder="DNI">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[${cant_person}][edad]" placeholder="Edad">
                                                            </div>
                                                            <div class="form-group">
                                                                <select name="persona[${cant_person}][sexo]" id="" class="form-control">
                                                                    <option value="" disabled selected>Sexo</option>
                                                                    <option value="Masculino">Masculino</option>
                                                                    <option value="Femenino">Femenino</option>
                                                                    <option value="Otro">Otro</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[${cant_person}][telefono]" placeholder="Telefono">
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" name="persona[${cant_person}][direccion]" placeholder="Dirección">
                                                            </div>
                                                        </div>`);

            $('#cant_person').val(Number(cant_person) + 1);
        }

        function select_options(id) {

            if(id == 9){
                if($( '#9' ).is( ":checked" )){
                    $('#1').attr('disabled',true)
                    $('#2').attr('disabled',true)
                    $('#3').attr('disabled',true)
                    $('#4').attr('disabled',true)
                    $('#5').attr('disabled',true)
                    $('#6').attr('disabled',true)
                    $('#7').attr('disabled',true)
                    $('#8').attr('disabled',true)
                }else{
                    $('#1').attr('disabled',false)
                    $('#2').attr('disabled',false)
                    $('#3').attr('disabled',false)
                    $('#4').attr('disabled',false)
                    $('#5').attr('disabled',false)
                    $('#6').attr('disabled',false)
                    $('#7').attr('disabled',false)
                    $('#8').attr('disabled',false)
                }
            }else{
                if($( "input:checked" ).val() == undefined){
                    $('#9').attr('disabled',false)
                }else{
                    $('#9').attr('disabled',true)
                }

            }
        }

        function select_riego(id_riesgo) {
            if(id_riesgo == 1){
                if($( '#1' ).is( ":checked" )){
                    $( "input" ).attr('disabled',true)
                    $('#1').attr('disabled',false)
                }else{
                    $( "input" ).attr('disabled',false)
                    $('#1').attr('disabled',true)
                }
            }else{
                if($( "input:checked" ).val() == undefined){
                    $('#1').attr('disabled',false)
                }else{
                    $('#1').attr('disabled',true)
                }

            }
        }
    </script>
@endsection
