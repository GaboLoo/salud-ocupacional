@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Alta Epidemiológica</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Medico</a></li>
                            <li class="breadcrumb-item active">Alta Epidemiológica</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                        @if (session('id_hdm'))
                            <a href="{{route('reincorporacion-laboral-pdf',session('id_hdm'))}}" target="_blank" class="btn btn-dark ml-5">Ver documento de reincorporación</a>
                        @endif
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                {{-- <div class="row mb-3">
                    <div class="col-md-3">
                        <button class="btn btn-success" onclick="generarDescansoMedico()">Generar descanso medico</button>
                    </div>
                </div> --}}
                
                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                      <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Autorización de reincorporación laboral</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Lista de autorización de reincorporación laboral</a>
                                    </li>
                                  </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                        <form action="{{route('reincorporacion-laboral.save')}}" method="post">
                                            @csrf
                                            <div class="d-flex flex-wrap">
                                                <div class="col-6">
                                                    <label for="">Paciente</label>
                                                    <select name="paciente" class="form-control select2" id="paciente" onchange="pacienteSelect()">
                                                        <option value="" disabled selected>Selecciona un paciente</option>
                                                        @foreach ($pacientes as $paciente)
                                                            <option value="{{$paciente->id}}">{{$paciente->name}} {{$paciente->last_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-6 form-group">
                                                    <label for="">SIGNOS Y SINTOMAS PRINCIPALES</label>
                                                    <input type="text" id="sigysinto" name="sigysinto" class="form-control">
                                                </div>
                                                <div class="col-6 form-group">
                                                    <label for="">FACTORES DE RIESGO (EDAD)</label>
                                                    <input type="text" id="facriesgo" name="facriesgo" class="form-control">
                                                </div>
                                                <div class="col-6 form-group">
                                                    <label for="">TIPO DE CASO</label>
                                                    <select name="tipo_caso" id="tipo_caso" class="select2 form-control">
                                                        <option value="CASO SOSPECHOSO">CASO SOSPECHOSO</option>
                                                        <option value="CASO CONFIRMADO">CASO CONFIRMADO</option>
                                                        <option value="CONTACTO DIRECTO">CONTACTO DIRECTO</option>
                                                    </select>
                                                </div>
                                                <div class="col-12 border-top border-bottom py-2 bg-primary mb-4">
                                                    <label for="" class="m-0">LINEA DE TIEMPO</label>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <label for="">INICIO Y FIN DE LOS SINTOMAS</label>
                                                    <div class="row">
                                                        <div class="col-6 row">
                                                            <label for="" class="col-2">DEL:</label>
                                                            <div class="col-10">
                                                                <input type="date" name="inicio_sintomas" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-6 row">
                                                            <label for="" class="col-2">AL:</label>
                                                            <div class="col-10">
                                                                <input type="date" name="fin_sintomas" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <label for="">INICIO Y FIN DEL AISLAMIENTO</label>
                                                    <div class="row">
                                                        <div class="col-6 row">
                                                            <label for="" class="col-2">DEL:</label>
                                                            <div class="col-10">
                                                                <input type="date" name="inicio_aislamiento" id="inicio_aislamiento" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-6 row">
                                                            <label for="" class="col-2">AL:</label>
                                                            <div class="col-10">
                                                                <input type="date" name="fin_aislamiento" id="fin_aislamiento" class="form-control">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 border-top border-bottom py-2 bg-primary mb-4">
                                                    <label for="" class="m-0">EXAMENES AUXILIARES</label>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <label for="">PRUEBA RAPIDA O MOLECULAR</label>
                                                    <div class="row">
                                                        <div class="col-4 d-flex">
                                                            <label for="" class="col-3">1RA:</label>
                                                            <div class="col-9">
                                                                <input type="date" name="date1RA" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-8 d-flex">
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">IGM</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="IGM1" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">IGG</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="IGG1" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">PM</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="PM1" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">LAB</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="LAB1" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row mt-4">
                                                        <div class="col-4 d-flex">
                                                            <label for="" class="col-3">2DA:</label>
                                                            <div class="col-9">
                                                                <input type="date" name="date2DA" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-8 d-flex">
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">IGM</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="IGM2" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">IGG</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="IGG2" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">PM</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="PM2" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0">LAB</label>
                                                                <div class="col-4">
                                                                    <input type="checkbox" name="LAB2" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 border-top border-bottom py-2 bg-primary mb-4">
                                                    <label for="" class="m-0">CUADRO CLINICO</label>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <div class="row justify-content-center">
                                                        <div class="col-10 d-flex">
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-9 m-0 text-right" style="font-size: 12px">ASINTOMATICO </label>
                                                                <div class="col-3">
                                                                    <input type="radio" name="cuadroclinico" value="asintomatico" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-7 m-0 text-right" style="font-size: 12px">LEVE</label>
                                                                <div class="col-3">
                                                                    <input type="radio" name="cuadroclinico" id="" value="level" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-8 m-0 text-right" style="font-size: 12px">MODERADO </label>
                                                                <div class="col-3">
                                                                    <input type="radio" name="cuadroclinico" value="moderado" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-3 d-flex align-items-center">
                                                                <label for="" class="col-6 m-0 text-right" style="font-size: 12px">SEVERO </label>
                                                                <div class="col-3">
                                                                    <input type="radio" name="cuadroclinico" value="severo" id="" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 border-top border-bottom py-2 bg-primary mb-4">
                                                    <label for="" class="m-0">COMPLICACIONES</label>
                                                </div>
                                                <div class="col-12 form-group">
                                                    <label for="">HOSPITALIZACION</label>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <label for="" >DEL:</label>
                                                            <input type="date" name="hospitalizacion[del]" class="form-control">
                                                        </div>
                                                        <div class="col-3">
                                                            <label for="" >AL:</label>
                                                            <input type="date" name="hospitalizacion[al]" class="form-control">
                                                        </div>
                                                        <div class="col-3">
                                                            <label for="" >VENT.MEC:</label>
                                                            <input type="input" name="hospitalizacion[vent_mec]" class="form-control">
                                                        </div>
                                                        <div class="col-3">
                                                            <label for="" >F.ALTA:</label>
                                                            <input type="input" name="hospitalizacion[f_alta]" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 border-top border-bottom py-2 bg-primary mb-4">
                                                    <label for="" class="m-0">CRITERIOS DE REINCORPORACION</label>
                                                </div>
                                                <div class="col-12">
                                                    <table class="table table-border">
                                                        <tr>
                                                            <td><label for="">ASINTOMATICO:</label></td>
                                                            <td style="width: 150px">
                                                                <select name="asintomatico_select" class="form-control select2" id="">
                                                                    <option value="si">SI</option>
                                                                    <option value="no">NO</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <textarea name="asintomatico_especificacion" id="" class="form-control" placeholder="ESPECIFIQUE"></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><label for="">FACTORES DE RIESGO:</label></td>
                                                            <td>
                                                                <select name="cr2_select" class="form-control select2" id="">
                                                                    <option value="si">SI</option>
                                                                    <option value="no">NO</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <textarea name="cr2_especificacion" id="" class="form-control" placeholder="ESPECIFIQUE"></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><label for="">TUVO 2 SEMANAS AISLAMIENTO:</label></td>
                                                            <td>
                                                                <select name="cr3_select" class="form-control select2" id="">
                                                                    <option value="si">SI</option>
                                                                    <option value="no">NO</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <textarea name="cr3_especificacion" id="" class="form-control" placeholder="ESPECIFIQUE"></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><label for="">P.RAPIDA PREVIA RETORNO:</label></td>
                                                            <td>
                                                                <select name="cr4_select" class="form-control select2" id="">
                                                                    <option value="si">SI</option>
                                                                    <option value="no">NO</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <select name="cr4_especificacion" class="form-control select2" id="">
                                                                    <option value="IGG">IGG</option>
                                                                    <option value="IGM">IGM</option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><label for="">COMPLICACIONES :</label></td>
                                                            <td>
                                                                <select name="cr5_select" class="form-control select2" id="">
                                                                    <option value="si">SI</option>
                                                                    <option value="no">NO</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <textarea name="cr5_especificacion" id="" class="form-control" placeholder="ESPECIFIQUE"></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><label for="">PUESTO ESENCIAL :</label></td>
                                                            <td>
                                                                <select name="cr6_select" class="form-control select2" id="">
                                                                    <option value="si">SI</option>
                                                                    <option value="no">NO</option>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <textarea name="cr6_especificacion" id="" class="form-control" placeholder="ESPECIFIQUE"></textarea>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td><label for="">RIESGO EXPOSICION:</label></td>
                                                            <td colspan="2">
                                                                <select name="cr7_select" class="form-control select2" id="">
                                                                    <option value="bajo">BAJO </option>
                                                                    <option value="medio">MEDIO </option>
                                                                    <option value="alto">ALTO  </option>
                                                                    <option value="muy alto">MUY ALTO  </option>
                                                                </select>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-12 border-top border-bottom py-2 bg-primary mb-4">
                                                    <label for="" class="m-0">CONCLUSION</label>
                                                </div>
                                                <div class="col-12 row">
                                                    <div class="col-6 form-group">
                                                        <label for="">Modo de reincorporación</label>
                                                        <select name="modo_reincorporacion" class="form-control select2" id="">
                                                            <option value="REINCORPORACION">REINCORPORACION</option>
                                                            <option value="TRABAJO REMOTO">TRABAJO REMOTO</option>
                                                            <option value="REUBICACION">REUBICACION</option>
                                                            <option value="AISLAMIENTO">AISLAMIENTO</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-6 form-group">
                                                        <label for="">FECHA DE REINCORPORACION</label>
                                                        <input type="date" name="fecha_reincorporacion" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-12 border-top border-bottom py-2 bg-primary mb-4">
                                                    <label for="" class="m-0">RECOMENDACIONES</label>
                                                </div>
                                                <div class="col-12 row form-group">
                                                    <div class="recomendaciones d-flex flex-wrap justify-content-center col-12">
                                                        <div class="form-group row col-10">
                                                            <label for="" class="col-1">1</label>
                                                            <div class="col-10">
                                                                <input type="text" name="recomendaciones[]" class="form-control" placeholder="Ingrese su recomendación">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="d-flex flex-wrap justify-content-center col-12">
                                                        <div class="text-right col-8 p-0">
                                                            <button type="button" class="btn btn-info btn-block" onclick="agregarRecomendaciones()">Agregar recomendaciones</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="border-top pt-3 text-right">
                                                <button class="btn btn-primary">Generar reincorporación laboral</button>
                                            </div>
                                            
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                                        <table class="table table-border" id="example2">
                                            <thead>
                                                <th>Nº</th>
                                                <th>Nombre</th>
                                                <th>Fecha</th>
                                                <th>Ver</th>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $n = 1;
                                                @endphp
                                                @foreach ($lista_reincorpracion as $item)
                                                    <tr>
                                                        <td>{{$n++}}</td>
                                                        <td>{{$item->nombre}}</td>
                                                        <td>{{$item->created_at}}</td>
                                                        <td><a href="{{route('reincorporacion-laboral-pdf',$item->id)}}" target="_blank" class="btn btn-success">Ver documento de reincorporación</a></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                  </div>

                            </div>
                        </div>
                    </div>
                </div>
             

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });
        function pacienteSelect() {
            let paciente = $('#paciente').val();
            $.ajax({
                url: `{{route('reincorporacion-laboral.edad')}}`,
                method: "post",
                data:{ paciente:paciente },
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#sigysinto').val(response.fase1.opciones_seleccionadas)
                    $('#facriesgo').val(response.texto)
                    $('#inicio_aislamiento').val(response.historial_descanso_medico.del)
                    $('#fin_aislamiento').val(response.historial_descanso_medico.al)
                }
            })
        }
        let recomendaciones_count = 1;
        function agregarRecomendaciones() {
            recomendaciones_count = recomendaciones_count + 1
            $('.recomendaciones').append(`<div class="form-group row col-10">
                                                            <label for="" class="col-1">${recomendaciones_count}</label>
                                                            <div class="col-10">
                                                                <input type="text" name="recomendaciones[]" class="form-control" placeholder="Ingrese su recomendación">
                                                            </div>
                                                        </div>`)
        }
    </script>
@endsection