@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Diagnostico</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Diagnostico</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="row mb-4">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Formulario de diagnostico</h3>
                            </div>
                            <div class="card-body">
                                <form action="{{route('diagnostico.update')}}" method="post">
                                    @csrf
                                    <input type="hidden" value="{{$diagnostico_consult->id_usuario}}" name="id_fase" >
                                    <input type="hidden" value="{{$diagnostico_consult->id}}" name="id_diagnostico" >
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="">Titulo</label>
                                            <input type="text" name="titulo" class="form-control" value="{{$diagnostico_consult->titulo}}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="">Fecha y Hora</label>
                                            <input type="datetime-local" name="fecha_hora" class="form-control" value="{{$diagnostico_consult->fecha_hora}}">
                                        </div>
                                        <div class="form-group col-md-12">
                                            <textarea name="diagnostico" id="" cols="30" rows="10" class="form-control" placeholder="Escriba el diagnostico aquí">{{$diagnostico_consult->descripcion}}</textarea>
                                        </div>
                                        <div class="form-group text-right col-md-12">
                                            <button type="submit" class="btn btn-primary">Guardar Diagnostico</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Historial de Diagnosticos</h3>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                    <th>Nº</th>
                                    <th>Titulo</th>
                                    <th>Fecha y hora</th>
                                    <th>Diagnostico</th>
                                    <th>Opciones</th>
                                    </thead>
                                    <tbody>
                                    @php $c = 1 @endphp
                                    @foreach($diagnosticos as $diagnostico)
                                        <tr>
                                            <td>{{$c++}}</td>
                                            <td>{{$diagnostico->titulo}}</td>
                                            <td>{{$diagnostico->fecha_hora}}</td>
                                            <td>{{$diagnostico->descripcion}}</td>
                                            <td>
                                                <a href="{{route('diagnostico.edit',$diagnostico->id)}}" class="btn btn-success"><i class="fas fa-pen"></i></a>
                                                <button onclick="deleteDiagnostico({{$diagnostico->id}},'{{$diagnostico->titulo}}')" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        function deleteDiagnostico(id,nombre) {
            swal({
                title: `¿ Estas seguro que quieres eliminar ${nombre} ?`,
                text: "",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $.ajax({
                            url: '../../diagnosticar-delete/'+id,
                            type: 'get',
                            dataType: "JSON",
                            success: function (response)
                            {
                                if(response.success){
                                    swal(response.message, {
                                        icon: "success",
                                    });
                                    setTimeout(function () {
                                        location.reload();
                                    },1000)
                                }
                            },
                        })

                    } else {

                    }
                })
        }
    </script>
@endsection
