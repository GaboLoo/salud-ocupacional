@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Descanso Medico</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Medico</a></li>
                            <li class="breadcrumb-item active">Descanso Medico</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                        @if (session('id_hdm'))
                            <a href="{{route('descanso-medico-pdf',session('id_hdm'))}}" target="_blank" class="btn btn-dark ml-5">Ver descanso medico</a>
                        @endif
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                {{-- <div class="row mb-3">
                    <div class="col-md-3">
                        <button class="btn btn-success" onclick="generarDescansoMedico()">Generar descanso medico</button>
                    </div>
                </div> --}}

                <div class="row mb-3">
                    <div class="col-md-12">
                        <div class="card card-primary card-outline card-outline-tabs">
                            <div class="card-header p-0 border-bottom-0">
                                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                                    <li class="nav-item">
                                      <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Formulario descanso medico</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Lista de descanso medico emitidos</a>
                                    </li>
                                  </ul>
                            </div>
                            <div class="card-body">
                                <div class="tab-content" id="custom-tabs-three-tabContent">
                                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                                        <form action="{{route('descanso-medico.save')}}" method="post">
                                            @csrf
                                            <div class="d-flex flex-wrap">
                                                <div class="col-6">
                                                    <label for="">Paciente</label>
                                                    <select name="paciente" class="form-control select2" id="paciente" onchange="pacienteSelect()">
                                                        <option value="" disabled selected>Selecciona un paciente</option>
                                                        @foreach ($pacientes as $paciente)
                                                            <option value="{{$paciente->id}}">{{$paciente->name}} {{$paciente->last_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Edad</label>
                                                    <input type="text" id="edad" name="edad" class="form-control" placeholder="colocar la edad">
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">H.C Nº</label>
                                                    <input type="text" name="hcn" class="form-control">
                                                </div>
                                                <div class="col-12 form-group">
                                                    <label for="">Dignóstico</label>
                                                    <textarea name="diagnostico" id="diagnostico" cols="30" rows="5" class="form-control"></textarea>
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Dias</label>
                                                    <input type="number" name="dias" id="dias" class="form-control">
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Horas</label>
                                                    <input type="text" name="horas" class="form-control">
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Del</label>
                                                    <input type="date" name="del" class="form-control">
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Al</label>
                                                    <input type="date" name="al" class="form-control">
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Fecha</label>
                                                    <input type="text" name="fecha" class="form-control disabled" value="<?= date('d-m-Y') ?>" disabled>
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Hora</label>
                                                    <input type="text" name="hora" class="form-control disabled" value="<?= date('H:i:s') ?>" disabled>
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">Doctor</label>
                                                    <input type="text" name="doctor" class="form-control disabled" value="<?= auth()->user()->name.' '.auth()->user()->last_name ?>" disabled>
                                                </div>
                                                <div class="col-3 form-group">
                                                    <label for="">CMP.</label>
                                                    <input type="text" name="cmp" class="form-control">
                                                </div>
                                            </div>
                                            <div class="border-top pt-3 text-right">
                                                <button class="btn btn-primary">Generar descanso medico</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                                       <table id="example2" class="table">
                                            <thead>
                                                <th>Nº</th>
                                                <th>Paciente</th>
                                                <th>Edad</th>
                                                <th>Ver</th>
                                            </thead>
                                            <tbody>
                                                @php $c = 1; @endphp
                                                @foreach ($historiales as $historial)
                                                <tr>
                                                    <td>{{$c++}}</td>
                                                    <td>{{$historial->nombre}}</td>
                                                    <td>{{$historial->edad}}</td>
                                                    <td><a href="{{route('descanso-medico-pdf',$historial->id)}}" target="_blank" class="btn btn-info">Ver</a></td>
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                       </table>
                                    </div>
                                  </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
        $("#example2").DataTable({
            "responsive": true,
            "autoWidth": false,
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ]
        });
        function pacienteSelect() {
            let paciente = $('#paciente').val();
            $.ajax({
                url: `{{route('descanso-medico-edad')}}`,
                method: "post",
                data:{ paciente:paciente },
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#edad').val(response.edad)
                }
            })
        }
    </script>
@endsection