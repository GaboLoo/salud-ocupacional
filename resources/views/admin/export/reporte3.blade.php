@extends('layouts.app')

@section('title','Reporte Cumplimiento')

@section('content')
    <div class="content-wrapper">
       

        <div class="content">
    
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row justify-content-center">

                                <div class="col-md-12">
                                    <h1 class="m-0 text-dark text-center">Cumplimiento de llenado de Formulario de seguimiento</h1>
                                    <br><br><br>
                                </div>

                                <div class="col-md-6">
                                    <canvas id="canvas3"></canvas>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-8 mt-5">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Seguimiento</th>
                                        <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Cumplen con el seguimiento diario</td>
                                            <td>{{$cumplimiento1}}</td>
                                        </tr>
                                        <tr>
                                            <td>No cumplen</td>
                                            <td>{{$total_trabajadores - $cumplimiento1}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
  
@endsection

@section('script')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

    <script>

        var config2 = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        {{$cumplimiento1}},{{($total_trabajadores - $cumplimiento1)}}
                    ],
                    backgroundColor: ['#00c0ef', '#f39c12'],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Cumplen con el seguimiento diario',
                    'No cumplen',
                ]
            },
            options: {
                responsive: true
            }
        };

            var ctx2 = document.getElementById('canvas3').getContext('2d');
            var mychaart4 = new Chart(ctx2, config2);
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script>
        
        setTimeout(function(){
            window.print();
        },1000)

    </script>
@endsection
