@extends('layouts.app')

@section('title','Reporte estado de salud')

@section('content')
    <div class="content-wrapper">

        <div class="content">
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        <div class="card-body">
                            <div class="row justify-content-center" id="reportegrafico1">

                                <div class="col-md-12">
                                    <h1 class="m-0 text-dark text-center">Reporte estado de salud de los Trabajadores</h1>
                                    <br><br><br>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="canvas"></canvas>
                                    <br><br><br>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-6">
                                    <table class="table table-bordered">
                                        <thead>
                                            <th>Estado de salud</th>
                                            <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Trabajadores sanos</td>
                                                <td>{{$estado_salud_1}}</td>
                                            </tr>
                                            <tr>
                                                <td>Casos bajo sospecha</td>
                                                <td>{{$estado_salud_2}}</td>
                                            </tr>
                                            <tr>
                                                <td>Casos activos detectados</td>
                                                <td>{{$estado_salud_3}}</td>
                                            </tr>
                                            <tr>
                                                <td>Reincorporados</td>
                                                <td>{{$estado_salud_4}}</td>
                                            </tr>
                                            <tr>
                                                <td>Total de trabajadores</td>
                                                <td>{{ $total_trabajadores }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
  
@endsection

@section('script')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>

    <script>
        var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        var color = Chart.helpers.color;
        var horizontalBarChartData = {
            labels: ['Trabajadores sanos', 'Casos bajo sospecha', 'Casos activos detectados', 'Reincorporados'],
            datasets: [{
                label: 'Estado',
                backgroundColor: 'rgba(60,141,188,0.9)',
                borderColor: 'rgba(60,141,188,0.9)',
                borderWidth: 1,
                data: [{{$estado_salud_1}}, {{$estado_salud_2}}, {{$estado_salud_3}}, {{$estado_salud_4}}]
            }]
        };

        var ctx = document.getElementById('canvas').getContext('2d');
        window.myHorizontalBar = new Chart(ctx, {
            type: 'horizontalBar',
            data: horizontalBarChartData,
            options: {
                // Elements options apply to all of the options unless overridden in a dataset
                // In this case, we are setting the border of each horizontal bar to be 2px wide
                elements: {
                    rectangle: {
                        borderWidth: 2,
                    }
                },
                responsive: true,
                legend: {
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Estado de salud de los Trabajadores'
                }
            }
        });

        setTimeout(function(){
            window.print();
        },1000)

    </script>

@endsection
