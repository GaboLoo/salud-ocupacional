@extends('layouts.app')

@section('title','Reporte Seguimiento de personal')

@section('content')
    <div class="content-wrapper">
       

        <div class="content">
            <div class="row mb-4" id="form-user-colaboradores">
                <section class="col-lg-12 connectedSortable ui-sortable">
                    <div class="card">
                        
                        <div class="card-body">
                            <div class="row justify-content-center" id="">
                                <div class="col-md-12">
                                    <h1 class="m-0 text-dark text-center">Seguimiento de personal bajo sospecha y positivos COVID</h1>
                                    <br><br><br>
                                </div>
                                <div class="col-md-7">
                                    <canvas id="canvas2"></canvas>
                                    <br><br><br>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-6">
                                    <table class="table table-bordered">
                                        <thead>
                                        <th>Seguimiento</th>
                                        <th>Cantidad</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Contactados</td>
                                            <td>{{$seguimiento1}}</td>
                                        </tr>
                                        <tr>
                                            <td>No respondieron llamadas</td>
                                            <td>{{$total_trabajadores - $seguimiento2}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </section>
            </div>
    
           
        </div>
    </div>
  
@endsection

@section('script')
    <script src="{{asset('plugins/chart.js/Chart.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>

    <script>

        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        {{$seguimiento1}},{{($total_trabajadores - $seguimiento2)}}
                    ],
                    backgroundColor: ['#f56954', '#00a65a'],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Contactados',
                    'No respondieron llamadas',
                ]
            },
            options: {
                responsive: true
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('canvas2').getContext('2d');
            var mychaart2 = new Chart(ctx, config);
        };

       
    </script>
    <script>
    
        function fecha(){
            
            let fecha1 = $('#fecha1').val();
            let fecha2 = $('#fecha2').val();

            if(fecha1 != '' && fecha2 != ''){
                let url = location.origin;
                location.href = url+'/reportes/1/'+fecha1+'/'+fecha2;
            }else{
                swal("Error", "Debe de seleccionar las dos fechas", "error");
            }

        }

        setTimeout(function(){
            window.print();
        },1000)

    </script>
@endsection
