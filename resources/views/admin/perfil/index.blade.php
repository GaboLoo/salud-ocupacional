@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row mb-4">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Perfil</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">Colaborador</li>
                            <li class="breadcrumb-item active">Perfil</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul class="mb-0">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif


                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="row mb-4" id="form-user-colaboradores">
                    <section class="col-lg-12 connectedSortable ui-sortable">
                        <div class="card">
                            <div class="card-header ui-sortable-handle">
                                <h3 class="card-title">Formulario de Colaborador</h3>
                            </div>

                            <div class="card-body">
                                <form action="{{route('form-colaborador.perfil.update',auth()->user()->id)}}" method="post"  enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="row align-items-center justify-content-center">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <input type="hidden" id="id_type_user_colaboradores" name="id_type_user_colaboradores" value="{{$usuario_data->id_type_user}}">
                                                <input type="hidden" id="tipo_form" name="tipo_form" value="2">
                                                <div class="col-md-6 form-group">
                                                    <label for="">Nombre</label>
                                                    <input type="text" name="nombre" class="form-control" value="{{$usuario_data->name}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Apellido</label>
                                                    <input type="text" name="apellido" class="form-control" value="{{$usuario_data->last_name}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Email</label>
                                                    <input type="text" name="email" class="form-control" value="{{$usuario_data->email}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Contraseña</label>
                                                    <input type="text" name="password" class="form-control" value="">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo Documento</label>
                                                    @if(!empty($usuario_data->type_documento))
                                                    <select name="tipo_documento" id="" class="form-control">
                                                        <option value="1" @if($usuario_data->type_documento == 1) selected @endif>DNI</option>
                                                        <option value="2" @if($usuario_data->type_documento == 2) selected @endif>Carné de Extranjería</option>
                                                    </select>
                                                    @else
                                                        <select name="tipo_documento" id="" class="form-control">
                                                            <option value="" disabled selected>Eleguir tipo de documento</option>
                                                            <option value="1">DNI</option>
                                                            <option value="2">Carné de Extranjería</option>
                                                        </select>
                                                    @endif
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Documento</label>
                                                    <input type="text" name="documento" class="form-control" value="{{$usuario_data->documento}}">
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Celular</label>
                                                    <input type="text" name="celular" class="form-control" value="{{$usuario_data->celular}}">
                                                </div>
                                                {{-- <div class="col-md-6 form-group">
                                                    <label for="">Direccion</label>
                                                    <input type="text" name="direccion" class="form-control" value="{{$usuario_data->direccion}}">
                                                </div> --}}
                                                <div class="col-md-6 form-group">
                                                    <label for="">Departamento</label>
                                                    <select name="departamento" id="departamento" class="select2 form-control">
                                                        @if ($usuario_data->id_departamento != '')
                                                            @foreach ($departamentos as $departamento)
                                                                @if ($usuario_data->id_departamento == $departamento->id)
                                                                    <option value="{{$departamento->id}}" selected>{{$departamento->name}}</option>  
                                                                @else
                                                                    <option value="{{$departamento->id}}">{{$departamento->name}}</option>  
                                                                @endif
                                                            @endforeach
                                                            
                                                        @else
                                                            <option value="" disabled selected>Escoger un departamento</option>
                                                            @foreach ($departamentos as $departamento)
                                                                <option value="{{$departamento->id}}">{{$departamento->name}}</option>   
                                                            @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Provincia</label>
                                                    <select name="provincia" id="provincia" class="select2 form-control">
                                                        @if ($usuario_data->id_provincia != '')
                                                            @foreach ($provincias as $provincia)
                                                                @if ($usuario_data->id_provincia == $provincia->id)
                                                                    <option value="{{$provincia->id}}" selected>{{$provincia->name}}</option> 
                                                                @else
                                                                    <option value="{{$provincia->id}}">{{$provincia->name}}</option> 
                                                                @endif  
                                                            @endforeach
                                                        @else
                                                        <option value="" disabled selected>Escoger una provincia</option>
                                                        @foreach ($provincias as $provincia)
                                                            <option value="{{$provincia->id}}">{{$provincia->name}}</option>   
                                                        @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                <div class="col-md-6 form-group">
                                                    <label for="">Distrito</label>
                                                    <select name="distrito" id="distrito" class="select2 form-control">
                                                        @if ($usuario_data->id_distrito != '')
                                                            @foreach ($distritos as $distrito)
                                                                @if ($usuario_data->id_distrito == $distrito->id)
                                                                <option value="{{$distrito->id}}" selected>{{$distrito->name}}</option>   
                                                                @else
                                                                <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                                @endif
                                                            @endforeach
                                                        @else
                                                        <option value="" disabled selected>Escoger un distrito</option>
                                                        @foreach ($distritos as $distrito)
                                                            <option value="{{$distrito->id}}">{{$distrito->name}}</option>   
                                                        @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                </div>
                                                {{--datos de la tabla colaborador--}}
                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-6 form-group">
                                                    <label for="">Telefono de casa</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="text" name="telefono_casa" class="form-control" value="{{$data_colaborador->telefono_casa}}">
                                                    @else
                                                        <input type="text" name="telefono_casa" class="form-control">
                                                    @endif

                                                </div>
                                                @endif
                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)

                                                    <div class="col-md-6 form-group">
                                                        <label for="">Puesto</label>

                                                        @if(!empty($data_colaborador->puesto != ''))
                                                            <input type="text" class="form-control" value="{{$data_colaborador->puesto}}" name="puesto">
                                                        @else
                                                            <input type="text" class="form-control" value="" name="puesto">
                                                        @endif
                                                    </div>

                                                @endif

                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-6 form-group">
                                                    <label for="">Area</label>
                                                    <select class="form-control">
                                                        @if($data_colaborador->area != '')
                                                            @foreach($areas as $area)
                                                                
                                                                @if($data_colaborador->area == $area->id)
                                                                    <option value="{{ $area->id }}" selected>{{ $area->titulo }} {{ $area->descripción }}</option>
                                                                @else
                                                                    <option value="{{ $area->id }}">{{ $area->titulo }} {{ $area->descripción }}</option>
                                                                @endif
                                                                
                                                            @endforeach
                                                        @else
                                                            <option disabled selected>Selecciona una area</option>
                                                            @foreach($areas as $area)
                                                                <option value="{{ $area->id }}">{{ $area->titulo }} {{ $area->descripción }}</option>
                                                            @endforeach
                                                        @endif
                                                        
                                                    </select>
                                                    

                                                </div>

                                                @endif
                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-6 form-group">
                                                    <label for="">Fecha Nacimiento</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="date" name="fecha_nacimiento" class="form-control" value="{{$data_colaborador->fecha_nacimiento}}">
                                                    @else
                                                        <input type="text" name="fecha_nacimiento" class="form-control">
                                                    @endif

                                                </div>
                                                @endif
                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-6 form-group">
                                                    <label for="">Peso</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="text" name="peso" class="form-control" value="{{$data_colaborador->peso}}">
                                                    @else
                                                        <input type="text" name="peso" class="form-control">
                                                    @endif

                                                </div>
                                                @endif
                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-6 form-group">
                                                    <label for="">Altura</label>
                                                    @if(!empty($data_colaborador))
                                                        <input type="text" name="altura" class="form-control" value="{{$data_colaborador->altura}}">
                                                    @else
                                                        <input type="text" name="altura" class="form-control">
                                                    @endif

                                                </div>
                                                @endif

                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-6 form-group">
                                                    <label for="">Tipo de Sangre</label>
                                                    <select name="sangre" id="" class="form-control">

                                                        @if($data_colaborador->tipo_sangre == '')
                                                            <option value="" disabled selected>Elege un tipo de sangre</option>
                                                            <option value="O negativo">O negativo</option>
                                                            <option value="O positivo">O positivo</option>
                                                            <option value="A negativo">A negativo</option>
                                                            <option value="A positivo">A positivo</option>
                                                            <option value="B negativo">B negativo</option>
                                                            <option value="B positivo">B positivo</option>
                                                            <option value="AB negativo">AB negativo</option>
                                                            <option value="AB positivo">AB positivo</option>
                                                        @else
                                                            <option value="O negativo" @if($data_colaborador->tipo_sangre == 'O negativo') selected @endif>O negativo</option>
                                                            <option value="O positivo" @if($data_colaborador->tipo_sangre == 'O positivo') selected @endif>O positivo</option>
                                                            <option value="A negativo" @if($data_colaborador->tipo_sangre == 'A negativo') selected @endif>A negativo</option>
                                                            <option value="A positivo" @if($data_colaborador->tipo_sangre == 'A positivo') selected @endif>A positivo</option>
                                                            <option value="B negativo" @if($data_colaborador->tipo_sangre == 'B negativo') selected @endif>B negativo</option>
                                                            <option value="B positivo" @if($data_colaborador->tipo_sangre == 'B positivo') selected @endif>B positivo</option>
                                                            <option value="AB negativo" @if($data_colaborador->tipo_sangre == 'AB negativo') selected @endif>AB negativo</option>
                                                            <option value="AB positivo" @if($data_colaborador->tipo_sangre == 'AB positivo') selected @endif>AB positivo</option>
                                                        @endif


                                                    </select>
                                                </div>
                                                @endif
                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-6 form-group">
                                                    <label for="">Enfermedades</label>

                                                    @if(!empty($data_colaborador))
                                                        <textarea class="form-control" name="enfermedades">{{$data_colaborador->enfermedades}}</textarea>
                                                    @else
                                                        <textarea class="form-control" name="enfermedades"></textarea>
                                                    @endif

                                                </div>
                                                @endif
                                                @if(auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4)
                                                <div class="col-md-12 form-group">
                                                    <label for="">Alérgico a algún medicamento</label>
                                                    <textarea class="form-control" name="alergico">{{$data_colaborador->alergico}}</textarea>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <input id="file" type="file" class="d-none" name="file"  accept="image/x-png,image/gif,image/jpeg">
                                            <label for="file" class="input-file m-auto" id="file-foto">
                                                @if($usuario_data->foto)
                                                    <img src="{{$usuario_data->foto}}" alt="" class="img-fluid">
                                                @else
                                                    <i class="far fa-image"></i>
                                                @endif
                                            </label>
                                        </div>
                                        <div class="col-md-11">
                                            <hr>
                                        </div>
                                        <div class="col-md-11">
                                            <button class="btn btn-primary">Guardar</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>


            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')
    <script>
         function readFile(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var filePreview = document.createElement('img');
                    filePreview.id = 'file-preview';
                    filePreview.className = 'img-fluid';
                    //e.target.result contents the base64 data from the image uploaded
                    filePreview.src = e.target.result;
                    console.log(e.target.result);

                    var previewZone = document.getElementById('file-foto');
                    $('#file-foto').html('')
                    previewZone.appendChild(filePreview);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        var fileUpload = document.getElementById('file');

        fileUpload.onchange = function (e) {
            readFile(e.srcElement);
        }    
    </script>
@endsection