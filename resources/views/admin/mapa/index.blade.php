@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-7">
                    <div class="mapa-calor">
                        <img src="{{asset('dist/img/MAPA/amazonas.png')}}" alt="" class="img-fluid">
                        <img src="{{asset('dist/img/MAPA/Ancash.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Apurimac.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Arequipa.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Ayacucho.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/cajamarca.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Cuzco.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Huancavelica.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Huanuco.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Ica.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Junin.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/LaLibertad.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Lambayeque.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Lima.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/loreto.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/MadreDeDios.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Moquegua.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Pasco.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/piura.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Puno.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/sanMartin.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Tacna.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/tumbes.png')}}" alt="" class="img-fluid img-absolute-mapa">
                        <img src="{{asset('dist/img/MAPA/Ucayali.png')}}" alt="" class="img-fluid img-absolute-mapa">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="escroll">
                        <table class="table">
                            <thead>
                                <th>Color</th>
                                <th>Departamento</th>
                                <th>Infectados</th>
                            </thead>
                            <tbody>
                                @foreach ($datos_array as $item)
                                <tr>
                                    <td>
                                        <div class="color" style="background:{{$item['color']}}"></div>
                                    </td>
                                    <td>{{$item['nombre']}}</td>
                                    <td>{{$item['cantidad']}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    
@endsection