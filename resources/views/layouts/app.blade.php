<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Salud Ocupacional - Medisegur | @yield('title')</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="{{asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{asset('plugins/summernote/summernote-bs4.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <style>
        .input-file{
            width: 200px;
            height: 200px;
            border: 2px dashed #0e84b5;
            border-radius: 20px;

            display: flex;
            justify-content: center;
            align-items: center;
            font-size: 50px;
            cursor: pointer;

            padding: 10px;
        }
    </style>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body class="hold-transition sidebar-mini layout-fixed">

    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-navy navbar-dark">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
            </ul>

        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-light-navy elevation-4">
            <!-- Brand Logo -->
            <a href="{{route('inicio')}}" class="brand-link navbar-white">
                <img src="{{asset('images/medilaboris_logo.png')}}" alt="AdminLTE Logo" class="img-fluid"
                     style="opacity: .8">
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar user panel (optional) -->
                <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                    <div class="image">
                        <img src="{{ auth()->user()->foto }}" class="img-circle elevation-2" alt="User Image">
                    </div>
                    <div class="info">
                        <a href="#" class="d-block"> {{ auth()->user()->name }} {{ auth()->user()->last_name }}</a>
                    </div>
                </div>

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class
                             with font-awesome or any other icon font library -->
                        @if(auth()->user()->id_type_user == 1)
                            <li class="nav-item has-treeview ">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Gestion de Usuarios
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{route('user.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Usuarios</p>
                                        </a>
                                    </li>
                                    <!--<li class="nav-item">
                                        <a href="{{route('puesto.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Puesto</p>
                                        </a>
                                    </li>-->
                                    <li class="nav-item">
                                        <a href="{{route('area.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Area</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview ">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-building"></i>
                                    <p>
                                        Gestion de Empresas
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview" >
                                    <li class="nav-item">
                                        <a href="{{route('empresa.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Empresa</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('rugro.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Rubro</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview ">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-chart-bar"></i>
                                    <p>
                                        Reportes
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        @php $fecha2 = date('Y-m-d'); $fecha1 = date("Y-m-d",strtotime($fecha2."- 7 days")); @endphp
                                        <a href="{{route('reportes',[1,$fecha1,$fecha2])}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Estado de salud</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('reportes',[2,$fecha1,$fecha2])}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Seguimiento de personal</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('reportes',[3,$fecha1,$fecha2])}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Cumplimiento</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            
                        @endif
                        @if(auth()->user()->id_type_user ==2)
                            <li class="nav-item has-treeview ">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-users"></i>
                                    <p>
                                        Gestion de Usuarios
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>

                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        <a href="{{route('user.index')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Usuarios</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview ">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-building"></i>
                                    <p>
                                        Gestion de Empresas
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview" >
                                    <li class="nav-item">
                                        <a href="{{route('inicio')}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Empresa</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item has-treeview ">
                                <a href="#" class="nav-link">
                                    <i class="fas fa-chart-bar"></i>
                                    <p>
                                        Reportes
                                        <i class="fas fa-angle-left right"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                        @php $fecha2 = date('Y-m-d'); $fecha1 = date("Y-m-d",strtotime($fecha2."- 7 days")); @endphp
                                        <a href="{{route('reportes',[1,$fecha1,$fecha2])}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Estado de salud</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('reportes',[2,$fecha1,$fecha2])}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Seguimiento de personal</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('reportes',[3,$fecha1,$fecha2])}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Cumplimiento</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('reportes',[4,$fecha1,$fecha2])}}" class="nav-link">
                                            <i class="far fa-circle nav-icon"></i>
                                            <p>Seguimiento masivo</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('descanso-medico')}}" type="submit" class="nav-link btn btn-block text-left">
                                    <i class="fas fa-notes-medical"></i>
                                    <p>
                                        Descanso Medico
                                    </p>
                                </a>
                            </li>
                            
                            <li class="nav-item">
                                <a href="{{route('reincorporacion-laboral')}}" type="submit" class="nav-link btn btn-block text-left">
                                    <i class="fas fa-file-signature"></i>
                                    <p>
                                        Alta Epidemiológica
                                    </p>
                                </a>
                            </li>
                        @endif
                        @if(auth()->user()->id_type_user == 3)
                            <li class="nav-item">
                                <a href="{{route('form-colaborador.index')}}" class="nav-link">
                                    <i class="fas fa-clipboard-check"></i>
                                    <p>
                                        Encuesta
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('seguimiento',auth()->user()->id)}}" class="nav-link">
                                    <i class="fas fa-clipboard-list"></i>
                                    <p>
                                        Mi seguimiento
                                    </p>
                                </a>
                            </li>
                        @endif
                        @if(auth()->user()->id_type_user == 4)
                            <li class="nav-item">
                                <a href="{{route('user.colaboradores')}}" class="nav-link">
                                    <i class="fas fa-users"></i>
                                    <p>
                                        Colaboraadores
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('empresa.perfil')}}" class="nav-link">
                                    <i class="fas fa-building"></i>
                                    <p>
                                        Empresa
                                    </p>
                                </a>
                            </li>
                        @endif
                        <li class="nav-item">
                            <a href="{{route('mapa.calor')}}" type="submit" class="nav-link btn btn-block text-left">
                                <i class="fas fa-map-marked-alt"></i>
                                <p>
                                    Mapa de calor
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('form-colaborador.perfil')}}" type="submit" class="nav-link btn btn-block text-left">
                                <i class="fas fa-user-tie"></i>
                                <p>
                                    Perfil
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <form action="{{route('logout')}}" method="post">
                                @csrf
                                <button type="submit" class="nav-link btn btn-block text-left">
                                    <i class="fas fa-sign-out-alt"></i>
                                    <p>
                                        Cerrar Sesion
                                    </p>
                                </button>
                            </form>
                        </li>
                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        @yield('content')
        <!-- /.content-wrapper -->
        <footer class="main-footer">
            <strong>Copyright &copy; {{ date('Y') }} Medisegur</strong>
            todos los derechos restringidos
            <div class="float-right d-none d-sm-inline-block">
                <b>Version</b> 1.0
            </div>
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{asset('plugins/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
        let URL = location.host
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Select2 -->
    <script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>

    <!-- Summernote -->
    <script src="{{asset('plugins/summernote/summernote-bs4.min.js')}}"></script>
    <!-- overlayScrollbars -->
    <script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>$('.select2').select2()    </script>
    @yield('script')
</body>
</html>
