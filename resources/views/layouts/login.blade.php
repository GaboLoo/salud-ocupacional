<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Salud Ocupacional - Medisegur</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        .titulo-login{
            display: flex;
            align-items: center;
            justify-content: center;
            color: #ffffff;
           
        }
        .titulo-login h1{
            position: relative;
            z-index: 9999;
            font-size: 5rem;
            font-weight: bold;
            text-align: center
        }
        .text-login-mobil{
            display: none;
        }

        @media(max-width: 425px){
            .text-login-mobil{
                display: block;
                margin-top: 30px
            }

            .titulo-login h1{
                display: none;
            }
        }
    </style>
</head>
<body>
<div id="app">
    <main>
        @yield('content')
    </main>
</div>
</body>
</html>
