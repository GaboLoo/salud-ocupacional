<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Autorización de Reincorporación Laboral Post COVID | {{$datos->nombre}} </title>
     <!-- Theme style -->
     <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
     <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
     <style>
         .border-separacion{
             border: 2px solid #000
         }
         .red{
             color:tomato
         }
         .bl-2{
             border-left: 2px solid #000;
         }
         .br-2{
             border-right: 2px solid #000;
         }
         .bb-2{
             border-bottom: 2px solid #000;
         }
     </style>
</head>
<body>
    <div class="container border-separacion mt-2">
        <div class="row">
            <div class="col-md-2 p-3">
                <img src="{{asset('images/medilaboris_logo.png')}}" alt="" class="img-fluid">
            </div>
            <div class="col-md-10 p-3 text-center bl-2">
                <h3>Alta Epidemiológica  <span class="red">Post COVID</span></h3>
            </div>
        </div>
    </div>
    <div class="container border-separacion mt-4">
        <div class="row">
            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>APELLIDOS Y NOMBRES:</strong> <strong class="ml-2">{{$datos->nombre}}</strong>
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                SIGNOS Y SINTOMAS PRINCIPALES
            </div>
            <div class="col-8  p-2 bb-2">
                {{$datos->signos_sintomas_principales}}
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                FACTORES DE RIESGO (EDAD)
            </div>
            <div class="col-8  p-2 bb-2">
                {{$datos->factores_riesgo}}
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                TIPO DE CASO
            </div>
            <div class="col-8  p-2 bb-2">
                {{$datos->tipo_caso}}
            </div>
            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>LINEA DE TIEMPO:</strong> 
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                INICIO Y FIN DE LOS SINTOMAS
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                DEL: {{$datos->rango_sintoma_del}}
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                AL: {{$datos->rango_sintoma_al}}
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                INICIO Y FIN DEL AISLAMIENTO
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                DEL: {{$datos->rango_aislamiento_del}}
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                AL: {{$datos->rango_aislamiento_al}}
            </div>

            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>EXAMENES AUXILIARES:</strong> 
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                PRUEBA RAPIDA O MOLECULAR
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                1RA: {{$datos->prm_1_fecha}}
            </div>
            <div class="col-4 p-2 bb-2 ">
                <div class="row">
                    <div class="col-3">
                        IGM: @if ($datos->prm_1_igm == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                    <div class="col-3">
                        IGG: @if ($datos->prm_1_igg == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                    <div class="col-3">
                        PM: @if ($datos->prm_1_pm == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                    <div class="col-3">
                        LAB: @if ($datos->prm_1_lab == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                </div>
                
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                PRUEBA RAPIDA O MOLECULAR
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                2DA: {{$datos->prm_2_fecha}}
            </div>
            <div class="col-4 p-2 bb-2">
                <div class="row">
                    <div class="col-3">
                        IGM: @if ($datos->prm_2_igm == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                    <div class="col-3">
                        IGG: @if ($datos->prm_2_igg == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                    <div class="col-3">
                        PM: @if ($datos->prm_2_pm == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                    <div class="col-3">
                        LAB: @if ($datos->prm_2_lab == 'on') <i class="fas fa-check"></i> @endif
                    </div>
                </div>
            </div>

            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>CUADRO CLINICO:</strong> 
            </div>
            <div class="col-12 p-2 bb-2">
                <div class="row justify-content-center">
                    <div class="col-2">
                        ASINTOMATICO : @if ($datos->cuadro_clinico == 'asintomatico') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                    </div>
                    <div class="col-2">
                        LEVE : @if ($datos->cuadro_clinico == 'leve') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                    </div>
                    <div class="col-2">
                        MODERADO : @if ($datos->cuadro_clinico == 'moderado') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                    </div>
                    <div class="col-2">
                        SEVERO : @if ($datos->cuadro_clinico == 'severo') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                    </div>
                </div>
            </div>

            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>COMPLICACIONES:</strong> 
            </div>
            <div class="col-4 p-2 br-2 bb-2">
                HOSPITALIZACION 
            </div>
            <div class="col-2 p-2 br-2 bb-2">
                DEL: {{$datos->complicaciones_del}}
            </div>
            <div class="col-2 p-2 br-2 bb-2">
                AL: {{$datos->complicaciones_al}}
            </div>
            <div class="col-2 p-2 br-2 bb-2">
                VENT.MEC: {{$datos->vent_mec}}
            </div>
            <div class="col-2 p-2 br-2 bb-2">
                F.ALTA: {{$datos->f_alta}}
            </div>

            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>CRITERIOS DE REINCORPORACION:</strong> 
            </div>

            @foreach ($creterios_reincorporacion as $item)
                <div class="col-4 p-2 br-2 bb-2">
                    {{$item->criterio_name}}
                </div>
                @if ($item->criterio_name == 'RIESGO EXPOSICION' )
                    <div class="col-8 p-2 br-2 bb-2">
                        BAJO @if ($item->criterio_select == 'bajo') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                        &nbsp;&nbsp;&nbsp;
                        MEDIO @if ($item->criterio_select == 'medio') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                        &nbsp;&nbsp;&nbsp;
                        ALTO @if ($item->criterio_select == 'alto') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                        &nbsp;&nbsp;&nbsp;
                        MUY ALTO @if ($item->criterio_select == 'muy alto') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                        &nbsp;&nbsp;&nbsp;
                    </div>
                @else
                    @if ($item->criterio_name == 'P.RAPIDA PREVIA RETORNO')
                        <div class="col-2 p-2 br-2 bb-2">
                            SI @if ($item->criterio_select == 'si') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif &nbsp;&nbsp;&nbsp; NO @if ($item->criterio_select == 'no') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                        </div>
                        <div class="col-6 p-2 br-2 bb-2">
                            IGG: @if ($item->especificacion == 'IGG') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                            &nbsp;&nbsp;&nbsp;
                            IGM: @if ($item->especificacion == 'IGM') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                            &nbsp;&nbsp;&nbsp;
                        </div>
                    @else
                        <div class="col-2 p-2 br-2 bb-2">
                            SI @if ($item->criterio_select == 'si') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif &nbsp;&nbsp;&nbsp; NO @if ($item->criterio_select == 'no') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                        </div>
                        <div class="col-6 p-2 br-2 bb-2">
                            {{$item->especificacion}}
                        </div>
                    @endif
                @endif
            @endforeach

            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>CONCLUSION:</strong> 
            </div>

            <div class="col-12 p-3 bb-2">
                REINCORPORACION  @if ($datos->modo_reincorporacion == 'REINCORPORACION') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                &nbsp;&nbsp;&nbsp;
                TRABAJO REMOTO  @if ($datos->modo_reincorporacion == 'TRABAJO REMOTO') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                &nbsp;&nbsp;&nbsp;
                REUBICACION  @if ($datos->modo_reincorporacion == 'REUBICACION') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                &nbsp;&nbsp;&nbsp;
                AISLAMIENTO  @if ($datos->modo_reincorporacion == 'AISLAMIENTO') ( <i class="fas fa-check"></i> ) @else &lpar; &rpar; @endif
                &nbsp;&nbsp;&nbsp;
            </div>

            <div class="col-12 p-3 bb-2">
                FECHA DE REINCORPORACION: &nbsp;&nbsp;&nbsp; {{$datos->fecha_reincorporacion}}
            </div>

            <div class="col-12 p-3 bg-info bb-2" style="color:#000!important">
                <strong>RECOMENDACIONES:</strong> 
            </div>

            @php
            $c = 1;   
            @endphp
            @foreach ($recomendaciones as $recomendacion)
            <div class="col-12 p-3 bb-2">
               <strong> {{$c++}}. {{$recomendacion->recomendacion}} </strong>
            </div>
            @endforeach

            <div class="col-4 p-3 bb-2 br-2" style="height: 200px"></div>
            <div class="col-8 p-3 bb-2" style="height: 200px"></div>
            <div class="col-4 p-3 br-2 text-center">FIRMA DEL TRABAJADOR</div>
            <div class="col-8 p-3 text-center">FIRMA DEL AUTORIZANTE (*)</div>
        </div>

    </div>
    <div class="container">
        <div class="col-12 p-4">(*)Se autoriza la reincorporación luego de verificar los documentos que acrediten que el trabajador ha cumplido con los 14 días de descanso otorgados por la entidad de salud correspondiente o 14 días después del alta en caso de hospitalización. Documentos como: descanso medico, pruebas de laboratorio, alta, ficha de seguimiento o autoevaluación, etc.</div>
    </div>
</body>
</html>

<script>
    window.print();
</script>