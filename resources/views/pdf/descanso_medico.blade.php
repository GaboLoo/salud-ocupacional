<style>
    *{
        font-family: sans-serif;
        font-size: 13px
    }

    .fondo{
        width: 70px;
        opacity: 0.5;
        display: flex;
        justify-content: center;
        align-items: center
    }

    .fondo img{
        transform: rotate(-90deg);
        height: 70px;
    }

    .fondo-r img{
        transform: rotate(90deg);
        height: 70px;
    }

    .contenido{
        width: 500px;
        position: relative;
        z-index: 2;
        padding-left: 20px;
        padding-right: 20px
    }
</style>

<div style="display: flex; flex-wrap: wrap;">
    <div class="fondo"><img src="{{asset('images/medilaboris_logo.png')}}" alt=""></div>
    <div class="contenido">
        <div style="border: 2px solid #001f3f ; border-radius: 20px; text-align:center; padding:20px 20px">
            <img src="{{asset('images/medilaboris_logo.png')}}" alt="" style="width: 300px">
            <p style="text-align: center; ">Salud Ocupacional y Medicina Laboral</p>
            <h3 style="text-align: center; margin:0px">DESCANSO MÉDICO</h3>
        </div>

        <div style="margin-top: 10px;border: 2px solid #001f3f ; border-radius: 20px; padding:20px 20px">
            <div style="display: flex; justify-content:space-between; margin-bottom: 15px">
                <p style="width: 100px; margin:0px">PACIENTE</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 150px);">{{$hdm->nombre}}</div>
            </div>
            <div style="display: flex; justify-content:space-between; margin-bottom: 15px">
                <p style="width: 100px; margin:0px">EDAD</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 150px);">{{$hdm->edad}}</div>
            </div>
            <div style="display: flex; justify-content:space-between;">
                <p style="width: 100px; margin:0px">H.C. N°</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 150px);">{{$hdm->hcn}}</div>
            </div>
        </div>

        <div style="margin-top: 10px;border: 2px solid #001f3f ; border-radius: 20px; padding:20px 20px">
            <div style="margin-bottom: 15px">
                <p style="margin:0px">Diagnostico</p> 
                <div style="border: 2px solid #001f3f; padding:10px; margin-top:15px">{{$hdm->diagnostico}}</div>
            </div>
        </div>

        <div style="margin-top: 10px;border: 2px solid #001f3f ; border-radius: 20px; padding:20px 20px; display: flex; justify-content:space-between; flex-wrap:wrap">
            <div style="display: flex; justify-content:space-between; margin-bottom: 20px ; width: 220px;">
                <p style="width: 90px; margin:0px">DÍAS</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->dias}}</div>
            </div>
            <div style="display: flex; justify-content:space-between; margin-bottom: 20px ; width: 220px;">
                <p style="width: 90px; margin:0px">HORAS</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->horas}}</div>
            </div>
            <div style="display: flex; justify-content:space-between; margin-bottom: 20px; width: 220px;">
                <p style="width: 90px; margin:0px">DEL</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->del}}</div>
            </div>
            <div style="display: flex; justify-content:space-between;width: 220px; margin-bottom: 20px">
                <p style="width: 90px; margin:0px">AL</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->al}}</div>
            </div>
            <div style="display: flex; justify-content:space-between;width: 220px; margin-bottom: 20px">
                <p style="width: 90px; margin:0px">FECHA</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->fecha}}</div>
            </div>
            <div style="display: flex; justify-content:space-between;width: 220px; margin-bottom: 20px">
                <p style="width: 90px; margin:0px">HORA</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->hora}}</div>
            </div>
            <div style="display: flex; justify-content:space-between;width: 600px ; margin-bottom: 20px">
                <p style="width: 90px; margin:0px">DOCTOR</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->doctor}}</div>
            </div>
            <div style="display: flex; justify-content:space-between;width: 600px;">
                <p style="width: 90px; margin:0px">CMP</p> 
                <div style="border-bottom: 2px solid #001f3f; padding: 0px 10px; width: calc(100% - 90px);">{{$hdm->cmp}}</div>
            </div>
        </div>

        <div style="">
            <div style="display: flex; justify-content:space-between;">
                <p style="border-top: 2px solid; padding-top: 10px; width: 300px; margin-left: 300px; margin-top: 80px; margin-bottom: 20px ; text-align: center">FIRMA Y SELLO</p>
            </div>
            <div style="display: flex; justify-content:space-between;">
                <p style="border-top: 2px solid; padding-top: 5px; text-align: center ; width: 600px; font-size: 14px; margin:0px">
                    www.medilaboris.com     /     ventas@medilaboris.com     /     984261513 <br>  
                    Av. José Gálvez Barrenechea 566 Of. 203 - San Isidro</p>
            </div>
        </div>
    </div>
    <div class="fondo fondo-r"><img src="{{asset('images/medilaboris_logo.png')}}" alt=""></div>
</div>

<script>
    window.print();
</script>