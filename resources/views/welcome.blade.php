@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div class="row">
                    @if($empresas != null)
                        @foreach($empresas as $empresa)
                            <a href="{{route('list-empresa',$empresa->id)}}" class="col-md-3 col-sm-6 col-12">
                                <div class="info-box">
                                    <span class="info-box-icon bg-info"><i class="nav-icon fas fa-building"></i></span>

                                    <div class="info-box-content">
                                        {{--                                    <span class="info-box-text"></span>--}}
                                        <span class="info-box-number">{{$empresa->title}}</span>
                                    </div>
                                    <!-- /.info-box-content -->
                                </div>
                                <!-- /.info-box -->
                            </a>
                        @endforeach
                    @endif
                </div>

            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    </div>
@endsection

@section('script')

@endsection
