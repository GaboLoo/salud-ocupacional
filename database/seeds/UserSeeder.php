<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $size = 100;

        $grav_url = "https://gravatar.com/avatar/" . md5( strtolower( trim( 'admin@gmail.com' ) ) ) ."?d=identicon&r=pg". "&s=" . $size;

        DB::table('users')->insert([
            'id_type_user' => 1,
            'name' => 'Johan Andrew',
            'last_name' => 'Buitron Flores',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin'),
            'documento' => '72255243',
            'celular' => '997730739',
            'direccion' => 'Mi casa xD',
            'foto' => $grav_url,
        ]);
    }
}
