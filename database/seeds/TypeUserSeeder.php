<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('type_user')->insert([
            'title' => 'Admin'
        ]);

        DB::table('type_user')->insert([
            'title' => 'Medico'
        ]);

        DB::table('type_user')->insert([
            'title' => 'User'
        ]);

        DB::table('type_user')->insert([
            'title' => 'Admin - empresa'
        ]);
    }
}
