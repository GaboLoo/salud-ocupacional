<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('departamento')->insert([
            'nombre' => 'Amazonas'
        ]);

        DB::table('departamento')->insert([
            'nombre' => 'Ancash'
        ]);

        DB::table('departamento')->insert([
            'nombre' => 'Apurimac'
        ]);

        DB::table('departamento')->insert([
            'nombre' => 'Arequipa'
        ]);

        DB::table('departamento')->insert([
            'nombre' => 'Ayacucho'
        ]);

        DB::table('departamento')->insert([
            'nombre' => 'Cajamarca'
        ]);

        DB::table('departamento')->insert([
            'nombre' => 'Callao'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Cusco'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Huancavelica'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Huanuco'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Ica'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Junin'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'La Libertad'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Lambayeque'
        ]);

        DB::table('departamento')->insert([
            'nombre' => 'Lima'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Loreto'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Madre De Dios'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Moquegua'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Pasco'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Piura'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Puno'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'San Martin'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Tacna'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Tumbes'
        ]);
        DB::table('departamento')->insert([
            'nombre' => 'Ucayali'
        ]);
    }
}
