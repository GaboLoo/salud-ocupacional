<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupoRiesgoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('grupo_riesgo')->insert([
            'title' => 'Ninguna de las anteriores'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Mayor a 60 años'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Gestante'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Hipertensión arterial'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Diabetes'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Caradiopatías'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Patología pulmonar'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Enfermedad renal crónica'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Enfermedad inmunosupresora'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Patología hepática'
        ]);

        DB::table('grupo_riesgo')->insert([
            'title' => 'Neoplasias activas'
        ]);
    }
}
