<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasContactoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas_contacto', function (Blueprint $table) {
            $table->id();
            $table->integer('id_fase2');
            $table->string('nombre');
            $table->string('dni');
            $table->string('edad');
            $table->string('sexo');
            $table->string('telefono');
            $table->string('direccion');
            $table->integer('logica_delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas_contacto');
    }
}
