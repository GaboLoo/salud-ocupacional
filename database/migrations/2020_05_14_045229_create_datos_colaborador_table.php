<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosColaboradorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos_colaborador', function (Blueprint $table) {
            $table->id();
            $table->integer('id_user');
            $table->string('telefono_casa');
            $table->string('puesto');
            $table->string('area');
            $table->string('fecha_nacimiento');
            $table->string('peso');
            $table->string('altura');
            $table->string('enfermedades');
            $table->string('tipo_sangre');
            $table->string('alergico');
            $table->integer('id_empresa');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos_colaborador');
    }
}
