<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFase3ItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fase3_items', function (Blueprint $table) {
            $table->id();
            $table->string('id_pregunta');
            $table->string('id_usuario');
            $table->string('id_fase3');
            $table->string('respuesta');
            $table->integer('logicaa_delete')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fase3_items');
    }
}
