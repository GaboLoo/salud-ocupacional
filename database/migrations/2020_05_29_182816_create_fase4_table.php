<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFase4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fase4', function (Blueprint $table) {
            $table->id();
            $table->string('diagnostico');
            $table->string('tipo_diagnostico')->nullable();
            $table->string('fecha_toma')->nullable();
            $table->string('como_cree_que_infecto')->nullable();
            $table->string('hospitalizacion');
            $table->string('fecha_hospitalizacion')->nullable();
            $table->string('fecha_alta')->nullable();
            $table->string('complicacion_hospitalaria');
            $table->string('complicacion_hospitalaria_descripcion')->nullable();
            $table->integer('logica_delete')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fase4');
    }
}
