<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth','validate_user'])->group(function () {
    Route::get('/', function () {

        if (auth()->user()->id_type_user == 2){
            $empresas = DB::table('medico_empresa')->join('empresa','empresa.id','=','medico_empresa.id_empresa')
                ->where('id_medico',auth()->user()->id)->where('medico_empresa.logica_delete',1)->select('empresa.*')->get();
            return view('welcome',compact('empresas'));
        }else{
            $empresas = null;
            return view('welcome',compact('empresas'));
        }

    })->name('inicio');
    Route::resources([
        'user' => 'UserController',
        'empresa' => 'EmpresaController',
        'puesto' => 'PuestoController',
        'area' => 'AreaController',
        'rugro' => 'RugroController',
    ]);
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/import-users', 'ImportController@user')->name('import.user');
    Route::get('/empresa/asignar/{id_empresa}', 'EmpresaController@asignar')->name('empresa.asignar');
    Route::post('/empresa/asignar-guardar', 'EmpresaController@asignar_guardar')->name('empresa.asignar_guardar');
    Route::get('/empresa/asignar-delete/{id_medico}/{id_empresa}', 'EmpresaController@asignar_delete')->name('empresa.asignar_delete');
    Route::get('/user-colaboradores','UserController@user_colaboradores')->name('user.colaboradores');
    Route::get('/empresa-perfil','EmpresaController@empresa_perfil')->name('empresa.perfil');

    Route::get('colaboradores-list/{id_empresa}','EmpresaController@list_empresa')->name('list-empresa');
    Route::get('diagnosticar/{id_fase}','EmpresaController@diagnosticar')->name('diagnosticar');

    Route::post('diagnosticar-save','EmpresaController@diagnosticar_save')->name('diagnosticar.save');
    Route::get('diagnosticar-edit/{id_diagnostico}','EmpresaController@diagnosticar_edit')->name('diagnostico.edit');
    Route::post('diagnosticar-update','EmpresaController@diagnosticar_update')->name('diagnostico.update');
    Route::get('diagnosticar-delete/{id_diagnostico}','EmpresaController@diagnosticar_delete')->name('diagnostico.delete');
    Route::post('save-reporte','UserController@saveReporte')->name('save-reporte');

    Route::get('calificar-activo/{id_usuario}','UserController@calificarActivo')->name('calificar-activo');
    Route::get('calificar-sano/{id_usuario}','UserController@calificarSano')->name('calificar-sano');
    Route::get('calificar-sospechoso/{id_usuario}','UserController@calificarSospechoso')->name('calificar-sospechoso');

    Route::get('reportes/{type}/{fecha1}/{fecha2}/{id_empresa?}','ReporeteController@reportes')->name('reportes');
    Route::get('export/{type}/{fecha1}/{fecha2}','ReporeteController@export')->name('export');
    // paginas de medico
    // - descanso medico
    Route::get('descanso-medico','MedicoController@descansoMedico')->name('descanso-medico');
    Route::post('descanso-medico-edad','MedicoController@descansoMedicoEdad')->name('descanso-medico-edad');
    Route::post('descanso-medico-save','MedicoController@descansoMedicoSave')->name('descanso-medico.save');
    Route::get('descanso-medico-pdf/{id_hdm}','MedicoController@descansoMedicoPdf')->name('descanso-medico-pdf');

    // - reincorporación laboral
    Route::get('reincorporacion-laboral','MedicoController@reincorporacionLaboral')->name('reincorporacion-laboral');
    Route::post('reincorporacion-laboral-edad','MedicoController@reincorporacionLaboralEdad')->name('reincorporacion-laboral.edad');
    Route::post('reincorporacion-laboral-save','MedicoController@reincorporacionLaboralSave')->name('reincorporacion-laboral.save');
    Route::get('reincorporacion-laboral-pdf/{id_hdm}','MedicoController@reincorporacionLaboralPdf')->name('reincorporacion-laboral-pdf');

    Route::get('map-calor','MapController@MapController')->name('mapa.calor');
    
    Route::get('bitacora-medico','BitacoraController@bitacoraMedico')->name('bitacora-medico');
    Route::post('bitacora-medico-save','BitacoraController@saveBitacoraMedico')->name('bitacora-medico-save');
    Route::post('bitacora-medico-consult','BitacoraController@bitacoraMedicoConsult')->name('bitacora-medico-consult');

});

Route::middleware(['auth'])->group(function () {
    Route::resource('form-colaborador','ColaboraadorController');
    Route::get('perfil','ColaboraadorController@perfil')->name('form-colaborador.perfil');
    Route::put('perfil-update/{id}','ColaboraadorController@perfilUpdate')->name('form-colaborador.perfil.update');
    Route::get('/seguimiento/{id_user}','UserController@seguimiento')->name('seguimiento');
});

Auth::routes();


