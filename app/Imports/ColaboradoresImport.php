<?php

namespace App\Imports;

use App\Models\DatosColaborador;
use App\Models\EstadoSalud;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ColaboradoresImport implements ToCollection, WithHeadingRow
{

    protected $empresa;

    public function __construct(int $empresa)
    {
        $this->empresa = $empresa;
    }

    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        //

        foreach ($collection as $row)
        {

            if (!empty($row['email'])){
                $size = 100;

                $grav_url = "https://gravatar.com/avatar/" . md5( strtolower( trim( $row['email'] ) ) ) ."?d=identicon&r=pg". "&s=" . $size;

                $foto = $grav_url;
            }else{
                $foto = '-';
            }

            if ($row['tipo_usuario'] == 1){
                
                $validar = DB::table('users')->where('documento',$row['documento'])->where('id_type_user',4)->where('logica_delete',1)->count();
                
                if($validar == 0){
                    $user = new User();
                    $user->id_type_user = 4;
                    $user->name = $row['nombre'];
                    $user->last_name = $row['apellido'];
                    $user->password = Hash::make($row['password']);
                    $user->email = $row['email'];
                    $user->documento = $row['documento'];
                    $user->celular = $row['celular'];
                    $user->direccion = $row['direccion'];
                    $user->foto = $foto;
    
                    if ( $user->save()){
                        $datos = new DatosColaborador();
                        $datos->id_user = $user->id;
                        $datos->id_empresa = $this->empresa;
                        $datos->save();
                    }
    
                    $estado_salud = new EstadoSalud();
                    $estado_salud->id_user = $user->id;
                    $estado_salud->type_estado_salud = 1;
                    $estado_salud->save();
                }

            }elseif ($row['tipo_usuario'] == 2){
                
                $validar = DB::table('users')->where('documento',$row['documento'])->where('id_type_user',3)->count();
                
                if($validar == 0){
                    $user = new User();
                    $user->id_type_user = 3;
                    $user->name = $row['nombre'];
                    $user->last_name = $row['apellido'];
                    $user->password = Hash::make($row['password']);
                    $user->email = $row['email'];
                    $user->documento = $row['documento'];
                    $user->celular = $row['celular'];
                    $user->direccion = $row['direccion'];
                    $user->foto = $foto;
    
                    if ( $user->save()){
                        $datos = new DatosColaborador();
                        $datos->id_user = $user->id;
                        $datos->id_empresa = $this->empresa;
                        $datos->save();
                    }
    
                    $estado_salud = new EstadoSalud();
                    $estado_salud->id_user = $user->id;
                    $estado_salud->type_estado_salud = 1;
                    $estado_salud->save();   
                }else{
                    $validar = DB::table('users')->where('documento',$row['documento'])->where('id_type_user',3)->first();
                    DB::table('users')->where('id',$validar->id)->update([
                       'logica_delete' => 1,
                       'password' => Hash::make($row['password'])
                    ]);
                }

            }


        }

    }
}
