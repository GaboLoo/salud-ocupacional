<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MapController extends Controller
{
    //
    function MapController(){

        $departamentos = DB::table('ubigeo_peru_departments')->get();

        $datos_array = [];
        foreach ($departamentos as $departamento){
            $datos = DB::table('users')->where('id_departamento',$departamento->id)->where('estado_salud','!=',1)->count();
            $datos_array[$departamento->id] = ['cantidad'=>$datos,'nombre'=>$departamento->name,'color'=>$departamento->color];
        }

        // dd($datos_array);

        return view('admin.mapa.index',compact('datos_array'));
    }
}
