<?php

namespace App\Http\Controllers;

use App\Imports\ColaboradoresImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller
{
    //
    public function user(Request $request){
        Excel::import(new ColaboradoresImport($request->input('id_empresa')), $request->file('personimport'));

        if ($request->input('route') == 'empresa.index'){
            return redirect()->route($request->input('route'))->with('success','Se ha importaco de manera correta los usuarios');
        }else{
            return redirect()->route($request->input('route'),$request->input('id_empresa'))->with('success','Se ha importaco de manera correta los usuarios');
        }


    }
}
