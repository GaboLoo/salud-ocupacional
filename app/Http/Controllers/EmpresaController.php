<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Diagnostico;
use App\Models\Empresa;
use App\Models\MedicoEmpresa;
use App\Models\Puesto;
use App\Models\Rugro;
use App\Models\TypeUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rugros_count = Rugro::where('logica_delete',1)->count();

        if ($rugros_count == 0){
            return redirect()->route('rugro.index')->with('error','No se encontro rugros ingresados, comience registrando un rugro para crear una empresa');
        }else{
            $empresas = DB::table('empresa')->join('rugro','rugro.id','=','empresa.id_rugro')->
            where('empresa.logica_delete',1)->select('empresa.*','rugro.titulo as rugro')->get();
            $rugros = Rugro::where('logica_delete',1)->get();
            $medicos = DB::table('users')->where('id_type_user',2)->get();
            return view('admin.empresa.index',compact('empresas','rugros','medicos'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $rules = [
             'nombre' => 'required',
             'ruc' => 'required',
             'direccion' => 'required',
             'telefono' => 'required',
             'correo' => 'required',
             'rugro' => 'required',
         ];

        $messages = [
            'nombre.required' => 'El campo nombre es campo obligatorio',
            'ruc.required' => 'El campo ruc es campo obligatorio',
            'telefono.required' => 'El telefono titulo es campo obligatorio',
            'correo.required' => 'El campo correo es campo obligatorio',
            'rugro.required' => 'El campo correo es campo obligatorio',
            'direccion.required' => 'El campo dirección es campo obligatorio',
        ];

        $this->validate($request, $rules, $messages);

        $empresa = new Empresa();
        $empresa->title = $request->input('nombre');
        $empresa->ruc = $request->input('ruc');
        $empresa->direccion = $request->input('direccion');
        $empresa->telefono = $request->input('telefono');
        $empresa->correo = $request->input('correo');
        $empresa->id_rugro = $request->input('rugro');

        if ($empresa->save()){
            return redirect()->route('empresa.index')->with('success', 'La empresa ha sido guardad con exito');
        }else{
            return redirect()->route('empresa.index')->with('error', 'Ha ocurrido un error, intente mas tarde');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $usuarios = DB::table('users')->
        join('type_user','type_user.id','=','users.id_type_user')->
        join('datos_colaborador','datos_colaborador.id_user','=','users.id')->
        where('users.logica_delete',1)->where('datos_colaborador.id_empresa',$id)->
        select('users.*','type_user.title')->get();
        return view('admin.empresa.show',compact('usuarios','id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $rugros = Rugro::where('logica_delete',1)->get();
        $empresas = Empresa::where('logica_delete',1)->get();
        $dato = Empresa::where('id',$id)->first();
        return view('admin.empresa.edit',compact('empresas','dato','id','rugros'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'nombre' => 'required',
            'ruc' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'correo' => 'required',
            'rugro' => 'required',
        ];

        $messages = [
            'nombre.required' => 'El campo nombre es campo obligatorio',
            'ruc.required' => 'El campo ruc es campo obligatorio',
            'telefono.required' => 'El telefono titulo es campo obligatorio',
            'correo.required' => 'El campo correo es campo obligatorio',
            'rugro.required' => 'El campo correo es campo obligatorio',
            'direccion.required' => 'El campo dirección es campo obligatorio',
        ];

        $this->validate($request, $rules, $messages);

        $empresa = new Empresa();
        $empresa->title = $request->input('nombre');
        $empresa->ruc = $request->input('ruc');
        $empresa->direccion = $request->input('direccion');
        $empresa->telefono = $request->input('telefono');

        $update = DB::table('empresa')->where('id',$id)->update([
            'title' => $request->input('nombre'),
            'ruc' => $request->input('ruc'),
            'direccion' => $request->input('direccion'),
            'telefono' => $request->input('telefono')
        ]);

        if ($update){
            return redirect()->route($request->input('route'))->with('success', 'La empresa ha sido actualizada con exito');
        }else{
            return redirect()->route($request->input('route'))->with('error', 'No se ha modificado ningun dato en el formulario, para hacer una actualización modifique el contenido');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = DB::table('empresa')->where('id',$id)->update([
            'logica_delete' => 0
        ]);

        if ($delete){
            return response()->json(['success'=>true,'message'=>'Se elimino correctamente']);
        }else{
            return response()->json(['success'=>false,'message'=>'Error, intente mas tarde']);
        }
    }

    function asignar($id_empresa){

        $medicos = DB::table('medico_empresa')->join('users','users.id','=','medico_empresa.id_medico')->
        where('id_empresa',$id_empresa)->where('medico_empresa.logica_delete',1)->select('medico_empresa.id','users.name','users.last_name')->get();

        $c = 1;
        $tabla = '';
        foreach ($medicos as $medico){
            $tabla.= '<tr>';
            $tabla.= '<td>'.$c++.'</td>';
            $tabla.= '<td>'.$medico->name.' '.$medico->last_name.'</td>';
            $tabla.= '<td> <buton class="btn btn-danger" onclick="deleteMedico('.$medico->id.','.$id_empresa.')"> <i class="fas fa-trash"></i> </buton> </td>';
            $tabla.= '</tr>';
        }

        return response()->json(['tabla'=>$tabla]);

    }

    function asignar_guardar(Request $request){

        $verific = DB::table('medico_empresa')->where('id_medico',$request->input('medico_selec'))
        ->where('id_empresa',$request->input('id_empresa'))->where('logica_delete',1)->count();

        if ($verific == 0){
            $medico_trabajo = new MedicoEmpresa();
            $medico_trabajo->id_medico = $request->input('medico_selec');
            $medico_trabajo->id_empresa = $request->input('id_empresa');
            $medico_trabajo->save();

            $medicos = DB::table('medico_empresa')->join('users','users.id','=','medico_empresa.id_medico')->
            where('id_empresa',$request->input('id_empresa'))->where('medico_empresa.logica_delete',1)->select('medico_empresa.id','users.name','users.last_name')->get();

            $c = 1;
            $tabla = '';
            foreach ($medicos as $medico){
                $tabla.= '<tr>';
                $tabla.= '<td>'.$c++.'</td>';
                $tabla.= '<td>'.$medico->name.' '.$medico->last_name.'</td>';
                $tabla.= '<td> <buton class="btn btn-danger" onclick="deleteMedico('.$medico->id.','.$request->input('id_empresa').')"> <i class="fas fa-trash"></i> </buton> </td>';
                $tabla.= '</tr>';
            }

            return response()->json(['tabla'=>$tabla,'success'=>true,'message'=>'Excelente, Se guardo de manera exitosa']);
        }else{
            return response()->json(['success'=>false,'message'=>'Error, el medico ya se encuentra asignado']);
        }



    }

    function asignar_delete($id_medico,$id_empresa){
        $delete = DB::table('medico_empresa')->where('id',$id_medico)->update([
            'logica_delete' => 0,
        ]);

        $medicos = DB::table('medico_empresa')->join('users','users.id','=','medico_empresa.id_medico')->
        where('id_empresa',$id_empresa)->where('medico_empresa.logica_delete',1)->select('medico_empresa.id','users.name','users.last_name')->get();

        $c = 1;
        $tabla = '';
        foreach ($medicos as $medico){
            $tabla.= '<tr>';
            $tabla.= '<td>'.$c++.'</td>';
            $tabla.= '<td>'.$medico->name.' '.$medico->last_name.'</td>';
            $tabla.= '<td> <buton class="btn btn-danger" onclick="deleteMedico('.$medico->id.')"> <i class="fas fa-trash"></i> </buton> </td>';
            $tabla.= '</tr>';
        }

        return response()->json(['tabla'=>$tabla,'success'=>true,'message'=>'Excelente, Se elimino de manera exitosa']);
    }

    function empresa_perfil(){

        $dato_user = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();

        $rugros = Rugro::where('logica_delete',1)->get();
        $empresas = Empresa::where('logica_delete',1)->get();
        $dato = Empresa::where('id',$dato_user->id_empresa)->first();
        return view('admin.admin_empresa.empresa_perfil',compact('empresas','dato','dato_user','rugros'));

    }

    function list_empresa($id_empresa){

        $colaboradores = DB::table('datos_colaborador')->join('users','users.id','=','datos_colaborador.id_user')
            ->join('type_user','type_user.id','=','users.id_type_user')
            ->where('datos_colaborador.id_empresa',$id_empresa)->select('users.*','type_user.title')->get();

//        dd($usuarios);

        return view('admin.admin_empresa.index',compact('colaboradores'));

    }

    function diagnosticar($id_fase){

        $diagnosticos = DB::table('diagnostico')->where('logica_delete',1)->where('id_usuario',$id_fase)->get();

        return view('admin.medico.diagnostico',compact('id_fase','diagnosticos'));

    }

    function diagnosticar_save(Request $request){


        // $usuario = DB::table('fase3')->where('id',$request->input('id_usuario'))->first();

        $diagnostico = new Diagnostico();
        $diagnostico->id_usuario = $request->input('id_usuario');
        $diagnostico->titulo = $request->input('titulo');
        $diagnostico->descripcion = $request->input('diagnostico');
        $diagnostico->fecha_hora = $request->input('fecha_hora');
        $diagnostico->logica_delete = 1;
        $diagnostico->save();

        return redirect()->route('diagnosticar',$request->input('id_usuario'))->with('success','El diagnostico se guardo de manera exitosa');

    }

    function diagnosticar_edit($id_diagnostico){

        $diagnostico_consult = DB::table('diagnostico')->where('id',$id_diagnostico)->first();

        $diagnosticos = DB::table('diagnostico')->where('logica_delete',1)->where('id_usuario',$diagnostico_consult->id_usuario)->get();

        return view('admin.medico.diagnostico_edit',compact('diagnosticos','diagnostico_consult'));

    }

    function diagnosticar_update(Request $request){

        $usuario = DB::table('fase3')->where('id',$request->input('id_fase'))->first();

//        dd($request->all());

        $update = DB::table('diagnostico')->where('id',$request->input('id_diagnostico'))->update([
            'titulo' => $request->input('titulo'),
            'descripcion' => $request->input('diagnostico'),
            'fecha_hora' => $request->input('fecha_hora'),
        ]);

        return redirect()->route('diagnosticar',$usuario->id_user)->with('success','El diagnostico se actualizo de manera exitosa');
    }


    function diagnosticar_delete($id_diagnostico){

        $update = DB::table('diagnostico')->where('id',$id_diagnostico)->update([
            'logica_delete' => 0,
        ]);

        return response()->json(['success'=>true,'message'=>'El diagnostico se elimino correctamente']);
    }

}
