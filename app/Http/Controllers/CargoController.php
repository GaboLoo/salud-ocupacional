<?php

namespace App\Http\Controllers;

use App\Models\Cargo;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CargoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empresas = Empresa::where('logica_delete',1)->get();
        $cargos = DB::table('cargo')->join('empresa','empresa.id','=','cargo.id_empresa')->
        where('cargo.logica_delete',1)->select('cargo.*','empresa.title as empresa')->get();
        return view('admin.users.cargo.index',compact('empresas','cargos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->input('type_id') == 1){
            $rules = [
                'empresa' => 'required',
                'titulo' => 'required'
            ];

            $messages = [
                'empresa.required' => 'El campo empresa es campo obligatorio',
                'titulo.required' => 'El campo titulo es campo obligatorio',
            ];

            $this->validate($request, $rules, $messages);

            $cargo = new Cargo();
            $cargo->id_empresa = $request->input('empresa');
            $cargo->titulo = $request->input('titulo');

            if ($cargo->save()){
                return redirect()->route('cargo.index')->with('success','El cargo se guardo correctamente');
            }else{
                return redirect()->route('cargo.index')->with('error','Ups, error al guardar la información, intente mas tarde');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cargo_consult = Cargo::where('id',$id)->first();
        $empresas = Empresa::where('logica_delete',1)->get();
        $cargos = DB::table('cargo')->join('empresa','empresa.id','=','cargo.id_empresa')->
        where('cargo.logica_delete',1)->select('cargo.*','empresa.title as empresa')->get();
        return view('admin.users.cargo.edit',compact('empresas','cargos','cargo_consult','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->input('type_id') == 1){
            $rules = [
                'empresa' => 'required',
                'titulo' => 'required'
            ];

            $messages = [
                'empresa.required' => 'El campo empresa es campo obligatorio',
                'titulo.required' => 'El campo titulo es campo obligatorio',
            ];

            $this->validate($request, $rules, $messages);

            $update = DB::table('cargo')->where('id',$id)->update([
                    'id_empresa' => $request->input('empresa'),
                    'titulo' => $request->input('titulo')
                ]);

            if ($update){
                return redirect()->route('cargo.edit',$id)->with('success','Se ha actualizado correctamente');
            }else{
                return redirect()->route('cargo.edit',$id)->with('error','Ups, error al guardar la información, intente mas tarde');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = DB::table('cargo')->where('id',$id)->update([
            'logica_delete' => 0
        ]);

        if ($delete){
            return response()->json(['success'=>true,'message'=>'se ha eliminado correctamente']);
        }else{
            return response()->json(['success'=>false,'message'=>'Error al momento de eliminar el cargo, intente mas tarde']);
        }
    }
}
