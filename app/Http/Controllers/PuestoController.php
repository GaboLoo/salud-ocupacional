<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Puesto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empresas = Empresa::where('logica_delete',1)->get();
        $puestos = DB::table('puesto')->join('empresa','empresa.id','=','puesto.id_empresa')->
        where('puesto.logica_delete',1)->select('puesto.*','empresa.title as empresa')->get();
        return view('admin.users.puesto.index',compact('empresas','puestos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->input('type_id') == 1){
            $rules = [
                'empresa' => 'required',
                'titulo' => 'required'
            ];

            $messages = [
                'empresa.required' => 'El campo empresa es campo obligatorio',
                'titulo.required' => 'El campo titulo es campo obligatorio',
            ];

            $this->validate($request, $rules, $messages);

            $puesto = new Puesto();
            $puesto->id_empresa = $request->input('empresa');
            $puesto->titulo = $request->input('titulo');

            if ($puesto->save()){
                return redirect()->route('puesto.index')->with('success','El puesto se guardo correctamente');
            }else{
                return redirect()->route('puesto.index')->with('error','Ups, error al guardar la información, intente mas tarde');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $puesto_consult = Puesto::where('id',$id)->first();
        $empresas = Empresa::where('logica_delete',1)->get();
        $puestos = DB::table('puesto')->join('empresa','empresa.id','=','puesto.id_empresa')->
        where('puesto.logica_delete',1)->select('puesto.*','empresa.title as empresa')->get();
        return view('admin.users.puesto.edit',compact('empresas','puestos','puesto_consult','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->input('type_id') == 1){
            $rules = [
                'empresa' => 'required',
                'titulo' => 'required'
            ];

            $messages = [
                'empresa.required' => 'El campo empresa es campo obligatorio',
                'titulo.required' => 'El campo titulo es campo obligatorio',
            ];

            $this->validate($request, $rules, $messages);

            $update = DB::table('puesto')->where('id',$id)->update([
                'id_empresa' => $request->input('empresa'),
                'titulo' => $request->input('titulo')
            ]);

            if ($update){
                return redirect()->route('puesto.edit',$id)->with('success','Se ha actualizado correctamente');
            }else{
                return redirect()->route('puestos.edit',$id)->with('error','Ups, error al guardar la información, intente mas tarde');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = DB::table('puesto')->where('id',$id)->update([
            'logica_delete' => 0
        ]);

        if ($delete){
            return response()->json(['success'=>true,'message'=>'se ha eliminado correctamente']);
        }else{
            return response()->json(['success'=>false,'message'=>'Error al momento de eliminar el cargo, intente mas tarde']);
        }
    }
}
