<?php

namespace App\Http\Controllers;

use App\Models\Rugro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

class RugroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rugros = DB::table('rugro')->where('logica_delete',1)->get();
        return view('admin.empresa.rugro.index',compact('rugros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = [
            'titulo' => 'required'
        ];

        $messages = [
            'titulo.required' => 'El campo titulo es campo obligatorio',
        ];

        $this->validate($request, $rules, $messages);

        $rugro = new Rugro();
        $rugro->titulo = $request->input('titulo');

        if ($rugro->save()){
            return redirect()->route('rugro.index')->with('success','El rugro se guardo correctamente');
        }else{
            return redirect()->route('rugro.index')->with('error','Ups, error al guardar la información, intente mas tarde');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $rugro_consilt = Rugro::where('logica_delete',1)->where('id',$id)->first();
        $rugros = DB::table('rugro')->where('logica_delete',1)->get();
        return view('admin.empresa.rugro.edit',compact('rugros','rugro_consilt','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules = [
            'titulo' => 'required'
        ];

        $messages = [
            'titulo.required' => 'El campo titulo es campo obligatorio',
        ];

        $this->validate($request, $rules, $messages);

        $update = DB::table('rugro')->where('id',$id)->update([
            'titulo' => $request->input('titulo')
        ]);

        if ($update){
            return redirect()->route('rugro.index')->with('success','El rugro se actualizado correctamente');
        }else{
            return redirect()->route('rugro.index')->with('error','Ups, error al guardar la información, intente mas tarde');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = DB::table('rugro')->where('id',$id)->update([
            'logica_delete' => 0
        ]);

        if ($delete){
            return response()->json(['success'=>true,'message'=>'se ha eliminado correctamente']);
        }else{
            return response()->json(['success'=>false,'message'=>'Error al momento de eliminar el cargo, intente mas tarde']);
        }
    }
}
