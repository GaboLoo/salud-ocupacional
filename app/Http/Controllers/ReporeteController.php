<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReporeteController extends Controller
{
    //
    function reportes($type,$fecha1,$fecha2,$id_empresa = 0){

     $total_trabajadores = DB::table('users')->where('id_type_user','!=','1')->where('id_type_user','!=',4)->count();
       if($type == 1){

          //   $estado_salud_1 = DB::table('estado_salud')->where('type_estado_salud',1)->where('logica_delete',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
            if(auth()->user()->id_type_user == 2){
                
                $total_trabajadores = 0;
                $estado_salud_2 = 0;
                $estado_salud_3 = 0;
                $estado_salud_4 = 0;
                $empresas = DB::table('medico_empresa')->join('empresa','empresa.id','=','medico_empresa.id_empresa')
                ->where('medico_empresa.logica_delete',1)->where('id_medico',auth()->user()->id)->where('medico_empresa.logica_delete',1)->get();
                
                foreach($empresas as $empresa){
                    $total_trabajadores = $total_trabajadores + DB::table('users')
                                        ->join('datos_colaborador','datos_colaborador.id_user','users.id')
                                        ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                        ->count();
                                        
                    $estado_salud_2 = $estado_salud_2 + DB::table('estado_salud')
                                    ->join('datos_colaborador','datos_colaborador.id_user','=','estado_salud.id_user')
                                    ->where('type_estado_salud',2)
                                    ->whereDate('estado_salud.created_at','>=',$fecha1)
                                    ->whereDate('estado_salud.created_at','<=',$fecha2)
                                    ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                    ->where('estado_salud.logica_delete', 1)
                                    ->count();
                                    
                    $estado_salud_3 = $estado_salud_3 + DB::table('estado_salud')
                                    ->join('datos_colaborador','datos_colaborador.id_user','=','estado_salud.id_user')
                                    ->where('type_estado_salud',3)
                                    ->whereDate('estado_salud.created_at','>=',$fecha1)
                                    ->whereDate('estado_salud.created_at','<=',$fecha2)
                                    ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                    ->where('estado_salud.logica_delete', 1)
                                    ->count();
                                    
                    $estado_salud_4 = $estado_salud_4 + DB::table('estado_salud')
                                    ->join('datos_colaborador','datos_colaborador.id_user','=','estado_salud.id_user')
                                    ->where('type_estado_salud',4)
                                    ->whereDate('estado_salud.created_at','>=',$fecha1)
                                    ->whereDate('estado_salud.created_at','<=',$fecha2)
                                    ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                    ->where('estado_salud.logica_delete', 1)
                                    ->count();
    
                }
                
               
                $estado_salud_1 = $total_trabajadores - ($estado_salud_2 + $estado_salud_3 + $estado_salud_4);
    
                return view('admin.reporte.index',compact('estado_salud_1','estado_salud_2','estado_salud_3','estado_salud_4','fecha1','fecha2','total_trabajadores'));
            
            }else{
                
                $estado_salud_2 = DB::table('estado_salud')->where('type_estado_salud',2)->where('estado_salud.logica_delete', 1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
                $estado_salud_3 = DB::table('estado_salud')->where('type_estado_salud',3)->where('estado_salud.logica_delete', 1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
                $estado_salud_4 = DB::table('estado_salud')->where('type_estado_salud',4)->where('estado_salud.logica_delete', 1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
    
                $estado_salud_1 = $total_trabajadores - ($estado_salud_2 + $estado_salud_3 + $estado_salud_4);
    
                return view('admin.reporte.index',compact('estado_salud_1','estado_salud_2','estado_salud_3','estado_salud_4','fecha1','fecha2','total_trabajadores'));
            
                
            }

       }elseif($type == 2){
           
           if(auth()->user()->id_type_user == 2){
                
                $total_trabajadores = 0;
                $seguimiento1 = 0;
                $seguimiento2 = 0;
                $empresas = DB::table('medico_empresa')->join('empresa','empresa.id','=','medico_empresa.id_empresa')->where('medico_empresa.logica_delete',1)
                ->where('id_medico',auth()->user()->id)->where('medico_empresa.logica_delete',1)->get();
                
                foreach($empresas as $empresa){
                    $total_trabajadores = $total_trabajadores + DB::table('users')
                                        ->join('datos_colaborador','datos_colaborador.id_user','users.id')
                                        ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                        ->count();
                                    
                    $seguimiento1 = DB::table('seguimiento')
                                    // ->join('estado_salud','estado_salud.id','=','seguimiento.id_estado_salud')
                                    ->join('datos_colaborador','datos_colaborador.id_user','=','seguimiento.id_estado_salud')
                                    ->where('seguimiento',1)
                                    ->whereDate('seguimiento.created_at','>=',$fecha1)
                                    ->whereDate('seguimiento.created_at','<=',$fecha2)
                                    ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                    ->count();
                                    
                    $seguimiento2 = DB::table('seguimiento')
                                    // ->join('estado_salud','estado_salud.id','=','seguimiento.id_estado_salud')
                                    ->join('datos_colaborador','datos_colaborador.id_user','=','seguimiento.id_estado_salud')
                                    ->where('seguimiento',2)
                                    ->whereDate('seguimiento.created_at','>=',$fecha1)
                                    ->whereDate('seguimiento.created_at','<=',$fecha2)
                                    ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                    ->count();
    
                }
    
                return view('admin.reporte.reprote2',compact('seguimiento1','seguimiento2','fecha1','fecha2','total_trabajadores'));
            
            }else{
        
                $seguimiento1 = DB::table('seguimiento')->where('seguimiento',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
                $seguimiento2 = DB::table('seguimiento')->where('seguimiento',2)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
    
                return view('admin.reporte.reprote2',compact('seguimiento1','seguimiento2','fecha1','fecha2','total_trabajadores'));
            
            }

       }elseif($type == 3){
           
           if(auth()->user()->id_type_user == 2){
                
                $total_trabajadores = 0;
                $cumplimiento1 = 0;
                $cumplimiento2 = 0;
                $empresas = DB::table('medico_empresa')->join('empresa','empresa.id','=','medico_empresa.id_empresa')
                ->where('medico_empresa.logica_delete',1)->where('id_medico',auth()->user()->id)->get();
                
                
                foreach($empresas as $empresa){
                    $total_trabajadores = $total_trabajadores + DB::table('users')
                                        ->join('datos_colaborador','datos_colaborador.id_user','users.id')
                                        ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                        ->count();
                                        
                    $cumplimiento1 = $cumplimiento1 + DB::table('cumplimiento')
                                        ->join('datos_colaborador','datos_colaborador.id_user','=','cumplimiento.id_user')
                                        ->where('cumplimiento',1)
                                        ->whereDate('cumplimiento.created_at','>=',$fecha1)
                                        ->whereDate('cumplimiento.created_at','<=',$fecha2)
                                        ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                        ->count();
                                        
                    $cumplimiento2 = $cumplimiento2 + DB::table('cumplimiento')
                                        ->join('datos_colaborador','datos_colaborador.id_user','=','cumplimiento.id_user')
                                        ->where('cumplimiento',2)
                                        ->whereDate('cumplimiento.created_at','>=',$fecha1)
                                        ->whereDate('cumplimiento.created_at','<=',$fecha2)
                                        ->where('datos_colaborador.id_empresa', $empresa->id_empresa)
                                        ->count();
                }
    
                return view('admin.reporte.reporte3',compact('cumplimiento1','cumplimiento2','fecha1','fecha2','total_trabajadores'));
                
            }else{
        
                $cumplimiento1 = DB::table('cumplimiento')->where('cumplimiento',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
                $cumplimiento2 = DB::table('cumplimiento')->where('cumplimiento',2)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
        
                return view('admin.reporte.reporte3',compact('cumplimiento1','cumplimiento2','fecha1','fecha2','total_trabajadores'));
                
            }

       }elseif($type == 4){

         if($id_empresa != 0){
            // $empresaid_busqueda = DB::table('empresa')->where('id',$id_empresa)->first();
            if(auth()->user()->id_type_user == 2){
               $empresas = DB::table('medico_empresa')->join('empresa','empresa.id','=','medico_empresa.id_empresa')
               ->where('id_medico',auth()->user()->id)->where('medico_empresa.logica_delete',1)->select('empresa.*')->get();
            }else{
                $empresas = DB::table('empresa')->where('logica_delete',1)->get();
            }

            $lista = DB::table('historial_riesgos')
            ->join('users','users.id','historial_riesgos.id_user')
            ->join('datos_colaborador','datos_colaborador.id_user','users.id')
            ->select('users.*','historial_riesgos.created_at as fecha_hr','historial_riesgos.name_table')->where('datos_colaborador.id_empresa',$id_empresa)->where('users.logica_delete',1)->get();
  
            return view('admin.reporte.seguimiento_masivo',compact('fecha1','fecha2','total_trabajadores','empresas','lista','id_empresa'));
         }else{
            $id_empresa = 0;
            if(auth()->user()->id_type_user == 2){
               $empresas = DB::table('medico_empresa')->join('empresa','empresa.id','=','medico_empresa.id_empresa')
               ->where('id_medico',auth()->user()->id)->where('medico_empresa.logica_delete',1)->get();
               $lista_empresa = [];
                foreach($empresas as $empresa){
                    $lista_empresa[] = $empresa->id;
                }
                $lista = DB::table('historial_riesgos')
                    ->join('users','users.id','historial_riesgos.id_user')
                    ->join('datos_colaborador','datos_colaborador.id_user','=','users.id')
                    ->select('users.*','historial_riesgos.created_at as fecha_hr','historial_riesgos.name_table')
                    ->whereIn('datos_colaborador.id_empresa', $lista_empresa)
                    ->where('users.logica_delete',1)->get();
            
                return view('admin.reporte.seguimiento_masivo',compact('fecha1','fecha2','total_trabajadores','empresas','lista','id_empresa'));
            }else{
                $empresas = DB::table('empresa')->where('logica_delete',1)->get();

                $lista = DB::table('historial_riesgos')
                ->join('users','users.id','historial_riesgos.id_user')
                ->select('users.*','historial_riesgos.created_at as fecha_hr','historial_riesgos.name_table')
                ->where('users.logica_delete',1)->get();
      
                return view('admin.reporte.seguimiento_masivo',compact('fecha1','fecha2','total_trabajadores','empresas','lista','id_empresa'));
            }
            // return redirect()->route('reportes', ['type' => 4,'fecha1' => $fecha1,'fecha2'=>$fecha2]);
         }

          
       }

    }

    public function export($type,$fecha1,$fecha2)
    {
     $total_trabajadores = DB::table('users')->where('id_type_user','!=','1')->where('id_type_user','!=',4)->count();
        if($type == 1){

          //   $estado_salud_1 = DB::table('estado_salud')->where('type_estado_salud',1)->where('logica_delete',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
            $estado_salud_2 = DB::table('estado_salud')->where('type_estado_salud',2)->where('logica_delete',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
            $estado_salud_3 = DB::table('estado_salud')->where('type_estado_salud',3)->where('logica_delete',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
            $estado_salud_4 = DB::table('estado_salud')->where('type_estado_salud',4)->where('logica_delete',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
            $estado_salud_1 = $total_trabajadores - ($estado_salud_2 + $estado_salud_3 + $estado_salud_4);

            return view('admin.export.index',compact('estado_salud_1','estado_salud_2','estado_salud_3','estado_salud_4','fecha1','fecha2','total_trabajadores'));

       }elseif($type == 2){
           
            $seguimiento1 = DB::table('seguimiento')->where('seguimiento',1)->where('logica_delete',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();
            $seguimiento2 = DB::table('seguimiento')->where('seguimiento',2)->where('logica_delete',1)->whereDate('created_at','>=',$fecha1)->whereDate('created_at','<=',$fecha2)->count();

            return view('admin.export.reprote2',compact('seguimiento1','segucimiento2','fecha1','fecha2','total_trabajadores'));

       }elseif($type == 3){

        $cumplimiento1 = DB::table('cumplimiento')->where('cumplimiento',1)->where('logica_delete',1)->count();
        $cumplimiento2 = DB::table('cumplimiento')->where('cumplimiento',2)->where('logica_delete',1)->count();

        return view('admin.export.reporte3',compact('cumplimiento1','cumplimiento2','fecha1','fecha2','total_trabajadores'));

       }
    }
}
