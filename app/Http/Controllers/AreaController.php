<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $empresas = Empresa::where('logica_delete',1)->get();
        $areas = DB::table('area')->where('area.logica_delete',1)->get();
        return view('admin.users.area.index',compact('empresas','areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if ($request->input('type_id') == 1){
            $rules = [
                'descripcion' => 'required',
                'titulo' => 'required'
            ];

            $messages = [
                'descripcion.required' => 'El campo descripción es campo obligatorio',
                'titulo.required' => 'El campo titulo es campo obligatorio',
            ];

            $this->validate($request, $rules, $messages);

            $area = new Area();
            $area->descripcion = $request->input('descripcion');
            $area->titulo = $request->input('titulo');

            if ($area->save()){
                return redirect()->route('area.index')->with('success','El area se guardo correctamente');
            }else{
                return redirect()->route('area.index')->with('error','Ups, error al guardar la información, intente mas tarde');
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $area_consult = Area::where('id',$id)->first();
        $empresas = Empresa::where('logica_delete',1)->get();
        $areas = DB::table('area')->where('area.logica_delete',1)->get();
        return view('admin.users.area.edit',compact('empresas','areas','area_consult','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->input('type_id') == 1){
            $rules = [
                'descripcion' => 'required',
                'titulo' => 'required'
            ];

            $messages = [
                'descripcion.required' => 'El campo descripci��n es campo obligatorio',
                'titulo.required' => 'El campo titulo es campo obligatorio',
            ];

            $this->validate($request, $rules, $messages);

            $update = DB::table('area')->where('id',$id)->update([
                'descripcion' => $request->input('descripcion'),
                'titulo' => $request->input('titulo')
            ]);

            if ($update){
                return redirect()->route('area.edit',$id)->with('success','Se ha actualizado correctamente');
            }else{
                return redirect()->route('area.edit',$id)->with('error','Ups, error al guardar la información, intente mas tarde');
            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = DB::table('area')->where('id',$id)->update([
            'logica_delete' => 0
        ]);

        if ($delete){
            return response()->json(['success'=>true,'message'=>'se ha eliminado correctamente']);
        }else{
            return response()->json(['success'=>false,'message'=>'Error al momento de eliminar el cargo, intente mas tarde']);
        }
    }
}
