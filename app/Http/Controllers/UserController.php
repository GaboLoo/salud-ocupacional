<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\DatosColaborador;
use App\Models\Empresa;
use App\Models\EstadoSalud;
use App\Models\Puesto;
use App\Models\Seguimiento;
use App\Models\TypeUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $puestos = Puesto::where('logica_delete',1)->get();
        $areas = Area::where('logica_delete',1)->get();
        $tipo_usuarios = TypeUser::where('logica_delete',1)->get();
        $empresas = Empresa::where('logica_delete',1)->get();
        $departamentos = DB::table('ubigeo_peru_departments')->get();
        $provincias = DB::table('ubigeo_peru_provinces')->get();
        $distritos = DB::table('ubigeo_peru_districts')->get();

        $empresas_medico = DB::table('medico_empresa')->join('empresa','empresa.id','medico_empresa.id_empresa')->where('id_medico',auth()->user()->id)->where('empresa.logica_delete',1)->get();

        foreach ($empresas_medico as $items){
            $array_empresas_medico[] = $items->id;
        }

        if (auth()->user()->id_type_user == 2){
            $usuarios = DB::table('users')
                ->join('type_user','type_user.id','=','users.id_type_user')
                ->join('datos_colaborador','datos_colaborador.id_user','=','users.id')
                ->where('users.logica_delete',1)
                ->whereIn('datos_colaborador.id_empresa',$array_empresas_medico)
                ->select('users.*','type_user.title')->get();
        }else{
            $usuarios = DB::table('users')->join('type_user','type_user.id','=','users.id_type_user')->where('users.logica_delete',1)
                ->select('users.*','type_user.title')->get();
        }


        return view('admin.users.index',compact('tipo_usuarios','empresas','usuarios','puestos','areas','departamentos','provincias','distritos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->input('tipo_form') == 1){
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'password' => 'required',
                'email' => 'required',
            ]);
            if($request->hasFile('file')){

                $path = $request->file('file1')->store('/avatares');

                $foto = url('/').'/'.$path;
                
            }else{
                $size = 100;

                $grav_url = "https://gravatar.com/avatar/" . md5( strtolower( trim( $request->input('email') ) ) ) ."?d=identicon&r=pg". "&s=" . $size;

                $foto = $grav_url;
            }

            $user = new User();
            $user->id_type_user = $request->input('id_type_user');
            $user->name = $request->input('nombre');
            $user->last_name = $request->input('apellido');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->documento = $request->input('documento');
            $user->type_documento = $request->input('tipo_documento');
            $user->celular = $request->input('celular');
            $user->id_departamento = $request->input('departamento');
            $user->id_provincia = $request->input('provincia');
            $user->id_distrito = $request->input('distrito');
            $user->foto = $foto;

            if($user->save()){
                return redirect()->route('user.index')->with('success', 'El usuario ha sido guardado con exito');
            }else{
                return redirect()->route('user.index')->with('error', 'Error, Intente mas tarde');
            }

        }else{
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'password' => 'required',
                'email' => 'required',
                'empresa' => 'required',
            ]);
            if($request->hasFile('file')){
                $path = $request->file('file')->store('/avatares');

                $foto = url('/').'/'.$path;
                
            }else{
                $size = 100;

                $grav_url = "https://gravatar.com/avatar/" . md5( strtolower( trim( $request->input('email') ) ) ) ."?d=identicon&r=pg". "&s=" . $size;

                $foto = $grav_url;
            }

            $user = new User();
            $user->id_type_user = $request->input('id_type_user_colaboradores');
            $user->name = $request->input('nombre');
            $user->last_name = $request->input('apellido');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->documento = $request->input('documento');
            $user->type_documento = $request->input('tipo_documento');
            $user->celular = $request->input('celular');
            $user->id_departamento = $request->input('departamento');
            $user->id_provincia = $request->input('provincia');
            $user->id_distrito = $request->input('distrito');
            $user->foto = $foto;
            $user->save();

            $datos_user = new DatosColaborador();
            $datos_user->id_user = $user->id;
            $datos_user->telefono_casa = $request->input('telefono_casa');
            $datos_user->puesto = $request->input('puesto');
            $datos_user->area = $request->input('area');
            $datos_user->fecha_nacimiento = $request->input('fecha_nacimiento');
            $datos_user->peso = $request->input('peso');
            $datos_user->altura = $request->input('altura');
            $datos_user->enfermedades = $request->input('enfermedades');
            $datos_user->id_empresa = $request->input('empresa');
            $datos_user->alergico = $request->input('alergico');
            $datos_user->tipo_sangre = $request->input('sangre');

            $estado_salud = new EstadoSalud();
            $estado_salud->id_user = $user->id;
            $estado_salud->type_estado_salud = 1;
            $estado_salud->save();

            if($datos_user->save()){
                return redirect()->route('user.index')->with('success', 'El usuario ha sido guardado con exito');
            }else{
                return redirect()->route('user.index')->with('error', 'Error, Intente mas tarde');
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $departamentos = DB::table('ubigeo_peru_departments')->get();
        $provincias = DB::table('ubigeo_peru_provinces')->get();
        $distritos = DB::table('ubigeo_peru_districts')->get();

        $usuario_data = DB::table('users')->join('type_user','type_user.id','=','users.id_type_user')->where('users.id',$id)
            ->select('users.*','type_user.title')->first();
//        dd($usuario_data);
        $tipo_usuarios = TypeUser::where('logica_delete',1)->get();
        $empresas = Empresa::where('logica_delete',1)->get();
        // $usuarios = DB::table('users')->join('type_user','type_user.id','=','users.id_type_user')->where('users.logica_delete',1)
            // ->select('users.*','type_user.title')->get();
            
        $empresas_medico = DB::table('medico_empresa')->join('empresa','empresa.id','medico_empresa.id_empresa')->where('id_medico',auth()->user()->id)->where('empresa.logica_delete',1)->get();

        foreach ($empresas_medico as $items){
            $array_empresas_medico[] = $items->id;
        }
            if (auth()->user()->id_type_user == 2){
                $usuarios = DB::table('users')
                    ->join('type_user','type_user.id','=','users.id_type_user')
                    ->join('datos_colaborador','datos_colaborador.id_user','=','users.id')
                    ->where('users.logica_delete',1)
                    ->whereIn('datos_colaborador.id_empresa',$array_empresas_medico)
                    ->select('users.*','type_user.title')->get();
            }else{
                $usuarios = DB::table('users')->join('type_user','type_user.id','=','users.id_type_user')->where('users.logica_delete',1)
                    ->select('users.*','type_user.title')->get();
            }
        $puestos = Puesto::where('logica_delete',1)->get();
        $areas = Area::where('logica_delete',1)->get();
        if ($usuario_data->id_type_user != 3 &&  $usuario_data->id_type_user != 4){
            return view('admin.users.edit',compact('tipo_usuarios','empresas','usuarios','usuario_data','id','puestos','areas','departamentos','provincias','distritos'));
        }else{
            $data_colaborador = DB::table('datos_colaborador')->where('id_user',$usuario_data->id)->first();
            return view('admin.users.edit',compact('tipo_usuarios','empresas','usuarios','usuario_data','id','data_colaborador','puestos','areas','departamentos','provincias','distritos'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        if($request->input('tipo_form') == 1){
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'email' => 'required',
            ]);

            if($request->hasFile('file')){

                $path = $request->file('file')->store('/avatares');

                if ($request->input('password')){
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                        'dias' => $request->input('cdias'),
                        'foto' => url('/').'/'.$path,
                    ]);
                }else{
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                        'dias' => $request->input('cdias'),
                        'foto' => url('/').'/'.$path,
                    ]);
                }

            }else{
                if ($request->input('password')){
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'dias' => $request->input('cdias'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                    ]);
                }else{
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'dias' => $request->input('cdias'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                    ]);
                }
            }

            if($update){
                return redirect()->route('user.index')->with('success', 'El usuario ha sido actualizado con exito');
            }else{
                return redirect()->route('user.index')->with('error', 'Error, Intente mas tarde');
            }

        }else{
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'email' => 'required',
                'empresa' => 'required',
            ]);

            if($request->hasFile('file')){

            }else{
                if ($request->input('password')){
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'documento' => $request->input('documento'),
                        'celular' => $request->input('celular'),
                        'dias' => $request->input('cdias'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                    ]);
                }else{
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'documento' => $request->input('documento'),
                        'celular' => $request->input('celular'),
                        'dias' => $request->input('cdias'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                    ]);
                }
            }

            $update_data = DB::table('datos_colaborador')->where('id_user',$id)->update([

                'telefono_casa' => $request->input('telefono_casa'),
                'puesto' => $request->input('puesto'),
                'area' => $request->input('area'),
                'fecha_nacimiento' => $request->input('fecha_nacimiento'),
                'peso' => $request->input('peso'),
                'altura' => $request->input('altura'),
                'enfermedades' =>  $request->input('enfermedades'),
                'id_empresa' => $request->input('empresa'),
                'alergico' => $request->input('alergico'),
                'tipo_sangre' => $request->input('sangre'),
            ]);

            if( $update || $update_data ){
                return redirect()->route('user.index')->with('success', 'El usuario ha sido actualizado con exito');
            }else{
                return redirect()->route('user.index')->with('error', 'Error, Intente mas tarde');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = DB::table('users')->where('id',$id)->where('logica_delete',1)->first();
        $delete = DB::table('users')->where('id',$id)->where('logica_delete',1)->update([
            'logica_delete' => 0,
            'documento' => $user->documento.'-delete'
        ]);

        if ($delete){
            return response()->json(['success'=>true,'message'=>'Se elimino correctamente']);
        }else{
            return response()->json(['success'=>false,'message'=>'Error, intente mas tarde']);
        }
    }

    public function user_colaboradores(){

        $dato_user = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();

        $colaboradores = DB::table('users')->join('datos_colaborador','datos_colaborador.id_user','=','users.id')->
        join('type_user','type_user.id','=','users.id_type_user')->
        where('users.logica_delete',1)->where('datos_colaborador.id_empresa',$dato_user->id_empresa)->
        select('users.*','type_user.title as title')->get();
        return view('admin.admin_empresa.index',compact('colaboradores'));
    }

    public function seguimiento($id_user){

        $seguimientos = DB::table('historial_riesgos')->where('logica_delete',1)->where('id_user',$id_user)->get();

        $data = null;

        foreach ($seguimientos as $seguimiento){
            if ($seguimiento->name_table == 'fase1'){
                $data[] = [
                    'data' => DB::table('fase1')->where('id',$seguimiento->id_riesgo)->first(),
                    'tipo' => 'fase1',
                ];
            }elseif ($seguimiento->name_table == 'fase2'){
                $fase2 = DB::table('fase2')->where('id',$seguimiento->id_riesgo)->first();
                $data[] = [
                    'data' => $fase2,
                    'tipo' => 'fase2',
                    'personas' => DB::table('personas_contacto')->where('id_fase2',$fase2->id)->get(),
                ];
            }elseif ($seguimiento->name_table == 'fase3'){
                $item3 = DB::table('fase3')->where('id',$seguimiento->id_riesgo)->first();
                $items = DB::table('fase3_items')->join('preguntas_fase_3','preguntas_fase_3.id','=','fase3_items.id_pregunta')
                    ->where('fase3_items.id_fase3',$item3->id)->select('fase3_items.*','preguntas_fase_3.pregunta')->get();
                $data[] = [
                    'registro' => $item3,
                    'data' => $items,
                    'tipo' => 'fase3'
                ];
            }elseif ($seguimiento->name_table == 'fase4'){
                $data[] = [
                    'data' => DB::table('fase4')->where('id',$seguimiento->id_riesgo)->first(),
                    'tipo' => 'fase4',
                ];
            }
        }

        $diagnosticos = DB::table('diagnostico')->where('logica_delete',1)->where('id_usuario',$id_user)->get();
        $user = DB::table('users')->join('datos_colaborador','datos_colaborador.id_user','=','users.id')->where('users.id',$id_user)->first();
        $enfermedades = DB::table('user_riesgo')->join('grupo_riesgo','grupo_riesgo.id','=','user_riesgo.id_grupo_riesgo')
            ->where('id_user',$id_user)->get();

        return view('admin.users.seguimiento.seguimiento',compact('data','id_user','diagnosticos','user','enfermedades'));
    }

    public function saveReporte(Request $request){

        if ($request->input('seguimiento') != ''){
            DB::table('users')->where('id',$request->input('id_user'))->update([
                'seguimiento' => $request->input('seguimiento'),
            ]);

//            $usuario_estado_salud = DB::table('estado_salud')->where('id_user',$request->input('id_user'))->where('logica_delete',1)->first();
//
//            dd($usuario_estado_salud);

            $b = DB::table('seguimiento')->where('id_estado_salud',$request->input('id_user'))->count();

            if ($b != 0){
                DB::table('seguimiento')->where('id_estado_salud',$request->input('id_user'))->where('logica_delete',1)->update([
                    'logica_delete' => 0
                ]);
            }

            $seguimiento = new Seguimiento();
            $seguimiento->id_estado_salud = $request->input('id_user');
            $seguimiento->seguimiento = $request->input('seguimiento');
            $seguimiento->save();

            return redirect()->route('seguimiento',$request->input('id_user'))->with('success','Se guardo correctamente');
        }else{
            return redirect()->route('seguimiento',$request->input('id_user'))->with('error','No se ha seleccionado ninguna opción');
        }

    }

    public function calificarActivo($id_usuario){

        DB::table('users')->where('id',$id_usuario)->update([
           'estado_salud' => 3,
        ]);

        DB::table('estado_salud')->where('id_user',$id_usuario)->where('logica_delete',1)->update([
            'logica_delete' => 0
        ]);

        $estado_salud = new EstadoSalud();
        $estado_salud->id_user = $id_usuario;
        $estado_salud->type_estado_salud = 3;
        $estado_salud->save();

        return redirect()->route('seguimiento',$id_usuario);

    }
    
    public function calificarSano($id_usuario){
        
        DB::table('users')->where('id',$id_usuario)->update([
           'estado_salud' => 1,
        ]);

        DB::table('estado_salud')->where('id_user',$id_usuario)->where('logica_delete',1)->update([
            'logica_delete' => 0
        ]);

        $estado_salud = new EstadoSalud();
        $estado_salud->id_user = $id_usuario;
        $estado_salud->type_estado_salud = 1;
        $estado_salud->save();

        return redirect()->route('seguimiento',$id_usuario);

    }
    
    public function calificarSospechoso(){
        DB::table('users')->where('id',$id_usuario)->update([
           'estado_salud' => 2,
        ]);

        DB::table('estado_salud')->where('id_user',$id_usuario)->where('logica_delete',1)->update([
            'logica_delete' => 0
        ]);

        $estado_salud = new EstadoSalud();
        $estado_salud->id_user = $id_usuario;
        $estado_salud->type_estado_salud = 2;
        $estado_salud->save();

        return redirect()->route('seguimiento',$id_usuario);
    }

}
