<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Cumplimiento;
use App\Models\EstadoSalud;
use App\Models\Fase1;
use App\Models\Fase2;
use App\Models\Fase3;
use App\Models\Fase3Item;
use App\Models\Fase4;
use App\Models\GrupoRiesgo;
use App\Models\HistorialRiesgo;
use App\Models\PersonasContacto;
use App\Models\Puesto;
use App\Models\UserRiesgo;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use function GuzzleHttp\Promise\queue;

class ColaboraadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $c_riesgos = DB::table('user_riesgo')->where('id_user',auth()->user()->id)->count();

        if ($c_riesgos > 0){

            $c_historial = DB::table('historial_riesgos')->where('id_user',auth()->user()->id)->count();
            $fecha = date('Y-m-d');
            $historial_riesgo = DB::table('historial_riesgos')->where('id_user',auth()->user()->id)->orderBy('id','desc')->first();

            if ($c_historial > 0 ){
                
                $dato_historiaal = DB::table('historial_riesgos')->where('id_user',auth()->user()->id)->
                orderBy('id','desc')->limit(1)->first();
                
                if(auth()->user()->estado_salud == 1 && $dato_historiaal->name_table == 'fase2'){
                    $form = 'fase1';
                    $bloqueo = false;
                    return view('admin.colaborador.index',compact('form','bloqueo'));
                }else if(auth()->user()->estado_salud == 1 && $dato_historiaal->name_table == 'fase3'){
                    $form = 'fase1';
                    $bloqueo = false;
                    return view('admin.colaborador.index',compact('form','bloqueo'));
                }else {
                    if ($dato_historiaal->name_table == 'fase1'){

                        $dato_fase1 = DB::table('fase1')->where('id',$dato_historiaal->id_riesgo)->
                        where('id_user',auth()->user()->id)->first();
    
                        if ($dato_fase1->opciones_seleccionadas == 'No presento síntomas'){
                            $form = 'fase1';
                            $date = strtotime(date('Y-m-d'));
    
                            $fecha = new DateTime($dato_historiaal->created_at);
                            $fecha_nueva = $fecha->format('Y-m-d');
    
                            if (strtotime($fecha_nueva) == $date){
                                $bloqueo = true;
                                return view('admin.colaborador.index',compact('form','bloqueo'));
                            }else{
                                $bloqueo = false;
                                return view('admin.colaborador.index',compact('form','bloqueo'));
                            }
    
                        }else{
                            $form = 'fase2';
                            $bloqueo = false;
                            return view('admin.colaborador.index',compact('form','bloqueo'));
                        }
    
                    }else if ($dato_historiaal->name_table == 'fase2'){
                        $form = 'fase3';
                        $bloqueo = false;
                        $preguntas = DB::table('preguntas_fase_3')->where('logica_delete',1)->get();
                        return view('admin.colaborador.index',compact('form','bloqueo','preguntas'));
    
                    }else if ($dato_historiaal->name_table == 'fase3'){
    
                        $date = new DateTime($historial_riesgo->created_at);
                        $fecha_validate = $date->format('Y-m-d');
    
                        if ($fecha_validate == $fecha){
                            $fecha_rais = DB::table('historial_riesgos')->where('id_user',auth()->user()->id)->where('name_table','fase1')->orderBy('id','desc')->first();
    
                            $fecha_fin = date("Y-m-d",strtotime($fecha_rais->created_at."+ ".auth()->user()->dias." days"));
    
    //                        $fecha_fin = date("Y-m-d",strtotime($fecha_fin_calculate."- 1 days"));
    
                            $v_fase3 = DB::table('historial_riesgos')->where('id_user',auth()->user()->id)->where('name_table','fase3')
                                ->whereDate('created_at', $fecha_fin)->count();
    
                            if ($v_fase3 != 0){
                                $form = 'fase4';
                                $bloqueo = false;
                            }else{
                                $form = 'fase3';
                                $bloqueo = true;
                            }
    //                        $form = 'fase3';
    //                        $bloqueo = true;
                        }else{
    
                            $fecha_rais = DB::table('historial_riesgos')->where('id_user',auth()->user()->id)->where('name_table','fase1')->orderBy('id','desc')->first();
    
                            $fecha_fin = date("Y-m-d",strtotime($fecha_rais->created_at."+ ".auth()->user()->dias." days"));
    
    //                        $fecha_fin = date("Y-m-d",strtotime($fecha_fin_calculate."- 1 days"));
    
    
                            $v_fase3 = DB::table('historial_riesgos')->where('id_user',auth()->user()->id)->where('name_table','fase3')
                                ->whereDate('created_at', $fecha_fin)->count();
    
                            if ($v_fase3 != 0){
                                $form = 'fase4';
                                $bloqueo = false;
                            }else{
                                $form = 'fase3';
                                $bloqueo = false;
                            }
    
                        }
                        $preguntas = DB::table('preguntas_fase_3')->where('logica_delete',1)->get();
                        return view('admin.colaborador.index',compact('form','bloqueo','preguntas'));
    
                    }else if($dato_historiaal->name_table == 'fase4'){
    
                        $date = new DateTime($historial_riesgo->created_at);
                        $fecha_validate = $date->format('Y-m-d');
    
                        if ($fecha_validate == $fecha){
                            $form = 'fase1';
                            $bloqueo = true;
                        }else{
                            $form = 'fase1';
                            $bloqueo = false;
                        }
    
                        return view('admin.colaborador.index',compact('form','bloqueo'));
                    }    
                }
                
            }else{
                $form = 'fase1';
                $bloqueo = false;
                return view('admin.colaborador.index',compact('form','bloqueo'));
            }

        }else{
            $form = 'riesgos';
            $bloqueo = false;
            $datos_colaborador = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();
            $riesgos = GrupoRiesgo::where('logica_delete',1)->orderBy('id','desc')->get();
            return view('admin.colaborador.encuesta',compact('form','riesgos','bloqueo','datos_colaborador'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $type = $request->input('type_form');

        if ($type == 0){

            $rules = [
                'riesgos' => 'required',
            ];

            $messages = [
                'riesgos.required' => 'Debe de marcar al menos uno',
            ];

            $this->validate($request, $rules, $messages);

            $riesgos = $request->input('riesgos');

            foreach ($riesgos as $riesgo){
                $user_riesgo = new UserRiesgo();
                $user_riesgo->id_user = auth()->user()->id;
                $user_riesgo->id_grupo_riesgo = $riesgo;
                $user_riesgo->save();
            }

            DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->update([
               'enfermedades' => $request->input('otras_enfermedades'),
            ]);

            return redirect()->route('form-colaborador.index')->
            with('success','Se guardaron correcamente los datos respondidos en el formulario de grupos de riesgo.');
        }elseif ($type == 1){

            $rules = [
                'fase1' => 'required',
            ];

            $messages = [
                'fase1.required' => 'Debe de marcar al menos una de las opciones',
            ];

            $this->validate($request, $rules, $messages);

            $fase1 = implode(',',$request->input('fase1'));

            $fecha_validate = date("Y-m-d",strtotime( date("Y-m-d")."- 1 days"));

            $count = DB::table('estado_salud')->where('id_user',auth()->user()->id)->count();


            if ($count != 0){
                $busqueda = DB::table('estado_salud')->where('id_user',auth()->user()->id)->where('logica_delete',1)->whereDate('created_at', $fecha_validate)->count();

                $bcumplimiento = DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->count();

                if ($bcumplimiento != 0){
                    DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->update([
                        'logica_delete' => 0
                    ]);
                }

                if($busqueda != 0){
                    $cumplimiento = new Cumplimiento();
                    $cumplimiento->id_user = auth()->user()->id;
                    $cumplimiento->cumplimiento = 1;
                    $cumplimiento->save();
                }else{
                    $cumplimiento = new Cumplimiento();
                    $cumplimiento->id_user = auth()->user()->id;
                    $cumplimiento->cumplimiento = 2;
                    $cumplimiento->save();
                }

            }else{
                $cumplimiento = new Cumplimiento();
                $cumplimiento->id_user = auth()->user()->id;
                $cumplimiento->cumplimiento = 1;
                $cumplimiento->save();
            }

            $fase1_data = new Fase1();
            $fase1_data->id_user = auth()->user()->id;
            $fase1_data->opciones_seleccionadas = $fase1;
            $fase1_data->save();

            $HR = new HistorialRiesgo();
            $HR->id_user = auth()->user()->id;
            $HR->id_riesgo = $fase1_data->id;
            $HR->name_table = 'fase1';
            $HR->save();

            DB::table('estado_salud')->where('id_user',auth()->user()->id)->where('logica_delete',1)->update([
               'logica_delete' => 0
            ]);

            if ($fase1 != 'No presento síntomas'){
                $empresa = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();

                $medicos = DB::table('medico_empresa')->join('users','users.id','=','medico_empresa.id_medico')
                    ->where('id_empresa',$empresa->id_empresa)->select('users.email')->get();


                foreach ($medicos as $medico){
                    $correo[] = $medico->email;
                }

                $to = implode(',',$correo);
                //$to = 'buitron19.18@gmail.com';

                $subject = "Encuesta - ".auth()->user()->name." ".auth()->user()->lastname;

                $message = "
                        <html><head></head>
                        <body>
                    <h1>El usuario </span> <span>".auth()->user()->name." ".auth()->user()->lastname." presenta las siguientes sintomas</h1>
                    <h3>Sintomas: </p> <p>".$fase1."</h3>
                    <p> <a href=".route('seguimiento',auth()->user()->id)." class='btn btn-dark' style='    color: #fff;
                        background-color: #007bff;
                        border-color: #007bff;
                        box-shadow: none;border: 1px solid transparent;
                        padding: .375rem .75rem;
                        font-size: 1rem;
                        line-height: 1.5;
                        border-radius: .25rem;text-decoration:none'>Ver Seguimiento</a> </p><br/>
                    </body></html>
                    ";

                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";


                $headers .= 'From: <info@medilaboris.net.pe>' . "\r\n";

                $mail = mail($to,$subject,$message,$headers);

                DB::table('users')->where('id',auth()->user()->id)->update([
                   'estado_salud' => 2
                ]);

                $estado_salud = new EstadoSalud();
                $estado_salud->id_user = auth()->user()->id;
                $estado_salud->type_estado_salud = 2;
                $estado_salud->save();
            }else{
                $estado_salud = new EstadoSalud();
                $estado_salud->id_user = auth()->user()->id;
                $estado_salud->type_estado_salud = 1;
                $estado_salud->save();
            }

            return redirect()->route('form-colaborador.index')->
            with('success','Se guardaron correctamente las opciones seleccionadas');
        }elseif ($type == 2){

//            dd($request->input('persona')[0]['nombre']);

            $fecha_validate = date("Y-m-d");

            $count = DB::table('estado_salud')->where('id_user',auth()->user()->id)->count();

            if ($count != 0){
                $busqueda = DB::table('estado_salud')->where('id_user',auth()->user()->id)->where('logica_delete',1)->whereDate('created_at', $fecha_validate)->count();

                if($busqueda != 0){

                }else{

                    $bcumplimiento = DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->count();

                    if ($bcumplimiento != 0){
                        DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->update([
                            'logica_delete' => 0
                        ]);
                    }

                    $cumplimiento = new Cumplimiento();
                    $cumplimiento->id_user = auth()->user()->id;
                    $cumplimiento->cumplimiento = 2;
                    $cumplimiento->save();
                }

            }

            $fase2 = new Fase2();
            $fase2->pregunta1 = $request->input('contacto');
            $fase2->pregunta2 = $request->input('fecha');
            $fase2->pregunta3 = $request->input('aislamiento');
            $fase2->save();

            $HR = new HistorialRiesgo();
            $HR->id_user = auth()->user()->id;
            $HR->id_riesgo = $fase2->id;
            $HR->name_table = 'fase2';
            $HR->save();

            if (count($request->input('persona')) > 0){

                $personas = $request->input('persona');

                foreach ($personas as $persona){

                    if ($persona['nombre'] == null && $persona['dni'] == null && $persona['edad'] == null && empty($persona['sexo'])
                        && $persona['telefono'] == null && $persona['direccion'] == null){

                    }else{
                        $personas = new PersonasContacto();
                        $personas->id_fase2 = $fase2->id;
                        $personas->nombre = $persona['nombre'];
                        $personas->dni = $persona['dni'];
                        $personas->edad = $persona['edad'];
                        $personas->sexo = $persona['sexo'];
                        $personas->telefono = $persona['telefono'];
                        $personas->direccion = $persona['direccion'];
                        $personas->save();
                    }
                }

            }

            return redirect()->route('form-colaborador.index')->
            with('success','Se guardaron correctamente las opciones seleccionadas');

        }elseif($type == 3){

            $rules = [
                'pregunta' => 'required',
            ];

            $messages = [
                'pregunta.required' => 'Debe de marcar al menos una de las opciones',
            ];

            $this->validate($request, $rules, $messages);

            $fecha_validate = date("Y-m-d");
            $fecha_validate_anterior = date("Y-m-d",strtotime( date("Y-m-d")."- 1 days"));

            $count = DB::table('estado_salud')->where('id_user',auth()->user()->id)->count();

            if ($count != 0){
                $busqueda = DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->whereDate('created_at', $fecha_validate)->count();
                $busqueda_anterior = DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->whereDate('created_at', $fecha_validate_anterior)->count();

                if($busqueda != 0){

                }else{

                    $bcumplimiento = DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->count();

                    if ($bcumplimiento != 0){
                        DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->update([
                            'logica_delete' => 0
                        ]);
                    }
                    if ($busqueda_anterior != 0){
                        $cumplimiento = new Cumplimiento();
                        $cumplimiento->id_user = auth()->user()->id;
                        $cumplimiento->cumplimiento = 1;
                        $cumplimiento->save();
                    }else{
                        $cumplimiento = new Cumplimiento();
                        $cumplimiento->id_user = auth()->user()->id;
                        $cumplimiento->cumplimiento = 2;
                        $cumplimiento->save();
                    }

                }

            }

            $fase3 = new Fase3();
            $fase3->id_user = auth()->user()->id;
            $fase3->fecha_registro = date('Y-m-d');
            $fase3->save();

            $historial = new HistorialRiesgo();
            $historial->id_user = auth()->user()->id;
            $historial->id_riesgo = $fase3->id;
            $historial->name_table = 'fase3';
            $historial->save();

            $preguntas = $request->input('pregunta');

            foreach ( $preguntas as $pregunta => $value ){

                $item = new Fase3Item();
                $item->id_pregunta = $pregunta;
                $item->id_usuario = auth()->user()->id;
                $item->id_fase3 = $fase3->id;
                $item->respuesta = $value;
                $item->save();

            }

            return redirect()->route('form-colaborador.index')->
            with('success','Se guardaron correctamente las opciones seleccionadas');

        }elseif ($type == 4){

            $rules = [
                'p1' => 'required',
                'hospital' => 'required',
                'hospital-complicacion' => 'required'
            ];

            $messages = [
                'p1.required' => 'Debe de marcar una opcion en la pregunta uno',
                'hospital.required' => 'Debe de marcar una opcion en la pregunta dos',
                'hospital-complicacion.required' => 'Debe de maarcaar una opcion en laa pregunta tres',
            ];

            $this->validate($request, $rules, $messages);

            if ($request->input('p1') == 'si'){

                $rules = [
                    'tipo' => 'required',
                    'fecha_de_toma' => 'required',
                    'como_se_infecto' => 'required',
                ];

                $messages = [
                    'tipo.required' => 'Debe de marcar una opcion en la pregunta 1.1',
                    'fecha_de_toma' => 'Debe de colocar la fecha en la que fue tomada la muestra',
                    'como_se_infecto.required' => 'Debe de marcar una opcion en la pregunta 1.2',
                ];

                $this->validate($request, $rules, $messages);



            }

            if ($request->input('hospital') == 'si'){
                $rules = [
                    'fecha_hospitalizacion' => 'required',
                    'fecha_alta' => 'required',
                ];

                $messages = [
                    'fecha_hospitalizacion.required' => 'Debe de colocar la fecha en la que fue hospitalizada',
                    'fecha_alta.required' => 'Debe de colocar la fecha en la que fue dada de alta',
                ];

                $this->validate($request, $rules, $messages);

            }

            if ($request->input('hospital-complicacion') == 'si'){
                $rules = [
                    'complicaciones' => 'required',
                ];

                $messages = [
                    'complicaciones.required' => 'Debe de colocar la que complicaciones sufrio',
                ];

                $this->validate($request, $rules, $messages);

            }

            $fecha_validate = date("Y-m-d");

            $count = DB::table('estado_salud')->where('id_user',auth()->user()->id)->count();

            if ($count != 0){
                $busqueda = DB::table('estado_salud')->where('id_user',auth()->user()->id)->where('logica_delete',1)->whereDate('created_at', $fecha_validate)->count();

                if($busqueda != 0){

                }else{

                    $bcumplimiento = DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->count();

                    if ($bcumplimiento != 0){
                        DB::table('cumplimiento')->where('id_user',auth()->user()->id)->where('logica_delete',1)->update([
                            'logica_delete' => 0
                        ]);
                    }

                    $cumplimiento = new Cumplimiento();
                    $cumplimiento->id_user = auth()->user()->id;
                    $cumplimiento->cumplimiento = 2;
                    $cumplimiento->save();
                }

            }

            $fase4 = new Fase4();
            $fase4->diagnostico = $request->input('p1');
            $fase4->tipo_diagnostico = (!empty($request->input('tipo'))) ? $request->input('tipo') : "" ;
            $fase4->fecha_toma = (!empty($request->input('fecha_de_toma'))) ? $request->input('fecha_de_toma') : "";
            $fase4->fecha_alta = (!empty($request->input('fecha_alta'))) ? $request->input('fecha_alta') : "";
            $fase4->como_cree_que_infecto = (!empty($request->input('como_se_infecto'))) ? $request->input('como_se_infecto') : "";
            $fase4->hospitalizacion = $request->input('hospital');
            $fase4->fecha_hospitalizacion = (!empty($request->input('fecha_hospitalizacion'))) ? $request->input('fecha_hospitalizacion') : "";
            $fase4->fecha_alta = (!empty($request->input('fecha_alta'))) ? $request->input('fecha_alta') : "";
            $fase4->complicacion_hospitalaria = $request->input('hospital-complicacion');
            $fase4->complicacion_hospitalaria_descripcion = (!empty($request->input('complicaciones'))) ? $request->input('complicaciones') : "";
            $fase4->save();

            $historial = new HistorialRiesgo();
            $historial->id_user = auth()->user()->id;
            $historial->id_riesgo = $fase4->id;
            $historial->name_table = 'fase4';
            $historial->save();

            DB::table('users')->where('id',auth()->user()->id)->update([
                'estado_salud' => 4,
            ]);

            DB::table('estado_salud')->where('id_user',auth()->user()->id)->where('logica_delete',1)->update([
                'logica_delete' => 0
            ]);

            $estado_salud = new EstadoSalud();
            $estado_salud->id_user = auth()->user()->id;
            $estado_salud->type_estado_salud = 4;
            $estado_salud->save();


            return redirect()->route('form-colaborador.index')->with('success','Se guardaron correctamente las opciones seleccionadas');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function perfil(){

        $departamentos = DB::table('ubigeo_peru_departments')->get();
        $provincias = DB::table('ubigeo_peru_provinces')->get();
        $distritos = DB::table('ubigeo_peru_districts')->get();
        if (auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4){
            $dato_user = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();

            $usuario_data = DB::table('users')->join('type_user','type_user.id','=','users.id_type_user')->
            where('users.id',auth()->user()->id)->select('users.*','type_user.title')->first();

            $data_colaborador = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();
            $empresas = DB::table('empresa')->where('logica_delete',1)->get();

            $puestos = Puesto::where('logica_delete',1)->where('id_empresa',$dato_user->id_empresa)->get();

            $areas = Area::where('logica_delete',1)->get();

            return view('admin.perfil.index',compact('empresas','usuario_data','data_colaborador','puestos','areas','departamentos','provincias','distritos'));
        }else{

            $usuario_data = DB::table('users')->join('type_user','type_user.id','=','users.id_type_user')->
            where('users.id',auth()->user()->id)->select('users.*','type_user.title')->first();

            $data_colaborador = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();

            return view('admin.perfil.index',compact('usuario_data','data_colaborador','departamentos','provincias','distritos'));
        }


    }

    public function perfilUpdate(Request $request, $id){
        // dd($request->all());
        if (auth()->user()->id_type_user == 3 || auth()->user()->id_type_user == 4){
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'documento' => 'required',
                'fecha_nacimiento' => 'required',
                'peso' => 'required',
                'altura' => 'required',
                'departamento' => 'required',
                'provincia' => 'required',
                'distrito' => 'required',
            ]);

            if($request->hasFile('file')){
                
                $path = $request->file('file')->store('/avatares');

                if ($request->input('password')){
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'direccion' => $request->input('direccion'),
                        'foto' => url('/').'/'.$path,
                    ]);
                }else{
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'direccion' => $request->input('direccion'),
                        'foto' => url('/').'/'.$path,
                    ]);
                }

            }else{
                if ($request->input('password')){
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'direccion' => $request->input('direccion')
                    ]);
                }else{
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'direccion' => $request->input('direccion')
                    ]);
                }
            }

            $update_data = DB::table('datos_colaborador')->where('id_user',$id)->update([

                'telefono_casa' => $request->input('telefono_casa'),
                'puesto' => $request->input('puesto'),
                'area' => $request->input('area'),
                'fecha_nacimiento' => $request->input('fecha_nacimiento'),
                'peso' => $request->input('peso'),
                'altura' => $request->input('altura'),
                'enfermedades' =>  $request->input('enfermedades'),
            ]);

            if( $update || $update_data ){
                return redirect()->route('form-colaborador.perfil')->with('success', 'Sus datos se actualizaron correctamente');
            }else{
                return redirect()->route('form-colaborador.perfil')->with('error', 'Los datos no han sido modificaados');
            }
        }else{
            $request->validate([
                'nombre' => 'required',
                'apellido' => 'required',
                'email' => 'required',
                'documento' => 'required',
                'celular' => 'required',
                'departamento' => 'required',
                'provincia' => 'required',
                'distrito' => 'required',
            ]);

            if($request->hasFile('file')){

                
                $path = $request->file('file')->store('/avatares');

                if ($request->input('password')){
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                        'foto' => url('/').'/'.$path,
                    ]);
                }else{
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito'),
                        'foto' => url('/').'/'.$path,
                    ]);
                }
            }else{
                if ($request->input('password')){
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'password' => Hash::make($request->input('password')),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito')
                    ]);
                }else{
                    $update = DB::table('users')->where('id',$id)->update([
                        'name' => $request->input('nombre'),
                        'last_name' => $request->input('apellido'),
                        'email' => $request->input('email'),
                        'documento' => $request->input('documento'),
                        'type_documento' => $request->input('tipo_documento'),
                        'celular' => $request->input('celular'),
                        'id_departamento' => $request->input('departamento'),
                        'id_provincia' => $request->input('provincia'),
                        'id_distrito' => $request->input('distrito')
                    ]);
                }
            }

            if( $update ){
                return redirect()->route('form-colaborador.perfil')->with('success', 'Sus datos se actualizaron correctamente');
            }else{
                return redirect()->route('form-colaborador.perfil')->with('error', 'Los datos no han sido modificaados');
            }
        }
    }

    public function sendEmailForm($nombre ,$apellido,$id_user,$to,$sintomas){
//        $to = $to_e;
        $subject = "Encuesta - ".$nombre." ".$apellido;

        $message = "
            <html><head></head>
            <body>
        <span>El usuario </span> <span>".$nombre." ".$apellido." presenta las siguientes sintomas</span><br/>
        <span>Sintomas: </span> <span>".$sintomas."</span><br/>
        <span> <a href=".route('seguimiento',$id_user)." class='btn btn-dark'>Ver Seguimiento</a> </span><br/>
        </body></html>
        ";

// Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
        $headers .= 'From: <ventas@asombra.com>' . "\r\n";

        $mail = mail($to,$subject,$message,$headers);

        return $mail;
    }
}
