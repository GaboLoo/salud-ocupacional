<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;

use App\Models\HistorialDescansoMedico;
use App\Models\HistorialReincorporacion;
use App\Models\CriterioDeReincorporacion;
use App\Models\RecomendacionesReincorporacion;

class MedicoController extends Controller
{
    //
    function descansoMedico(){
        $pacientes_id = DB::table('users')->join('datos_colaborador','datos_colaborador.id_user','users.id')
        ->join('medico_empresa','medico_empresa.id_empresa','datos_colaborador.id_empresa')
        // ->join('fase1','fase1.id_user','users.id')
        ->where('medico_empresa.id_medico',auth()->user()->id)->where('users.logica_delete',1)->get();
        
        $pacientes_array = [];
        foreach($pacientes_id as $paciente_id){
            $pacientes_array[] = DB::table('users')->where('id',$paciente_id->id_user)->first();
        }
        $pacientes = collect($pacientes_array);
        //dd($pacoentes);
        $historiales = DB::table('historial_descanso_medico')->where('id_doctor',auth()->user()->id)->orderBy('id','desc')->get();
        // dd($pacientes);
        return view('admin/medico/descanso_medico',compact('pacientes','historiales'));
    }

    function descansoMedicoEdad(Request $request){
        $paciente = $request->input('paciente');
        $edad = DB::table('datos_colaborador')->where('id_user',$paciente)->first();
        if($edad->fecha_nacimiento != null){
            $edad_item = $this->busca_edad($edad->fecha_nacimiento);
            return response()->json(['edad'=>$edad_item]);
        }
    }

    function busca_edad($fecha_nacimiento){
        $dia=date("d");
        $mes=date("m");
        $ano=date("Y");
        
        
        $dianaz=date("d",strtotime($fecha_nacimiento));
        $mesnaz=date("m",strtotime($fecha_nacimiento));
        $anonaz=date("Y",strtotime($fecha_nacimiento));
        
        //si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual
        
        if (($mesnaz == $mes) && ($dianaz > $dia)) {
        $ano=($ano-1); }
        
        //si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual
        
        if ($mesnaz > $mes) {
        $ano=($ano-1);}
        
         //ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad
        
        $edad=($ano-$anonaz);
        
        
        return $edad;
        
        
    }

    function descansoMedicoSave(Request $request){

        $rules = [
            'paciente' => 'required',
            'edad' => 'required',
            'hcn' => 'required',
            'diagnostico' => 'required',
            'dias' => 'required',
            'horas' => 'required',
            'del' => 'required',
            'al' => 'required',
            'cmp' => 'required',
        ];
        $customMessages = [
            'paciente.required' => 'Campo paciente es obligatorio',
            'edad.required' => 'Campo edad es obligatorio',
            'hcn.required' => 'Campo hcn es obligatorio',
            'diagnostico.required' => 'Campo diagnostico es obligatorio',
            'dias.required' => 'Campo dias es obligatorio',
            'horas.required' => 'Campo horas es obligatorio',
            'del.required' => 'Campo del es obligatorio',
            'al.required' => 'Campo al es obligatorio',
            'cmp.required' => 'Campo cmp es obligatorio',
        ];

        $this->validate($request, $rules, $customMessages);

        $busqueda = DB::table('historial_descanso_medico')->where('id_persona',$request->input('paciente'))->where('logica_delete',1)->count();

        if($busqueda == 0){
            $fase1 = DB::table('historial_riesgos')->where('id_user',$request->input('paciente'))->orderBy('id','desc')->limit(1)->first();

            $usuario = DB::table('users')->where('id',$request->input('paciente'))->first();
    
            $historial_descanso_medico = new HistorialDescansoMedico();
            $historial_descanso_medico->id_persona = $request->input('paciente');
            $historial_descanso_medico->id_historial_riesgo = $fase1->id;
            $historial_descanso_medico->nombre = $usuario->name.' '.$usuario->last_name;
            $historial_descanso_medico->edad = $request->input('edad');
            $historial_descanso_medico->hcn = $request->input('hcn');
            $historial_descanso_medico->diagnostico = $request->input('diagnostico');
            $historial_descanso_medico->dias = $request->input('dias');
            $historial_descanso_medico->horas = $request->input('horas');
            $historial_descanso_medico->del = $request->input('del');
            $historial_descanso_medico->al = $request->input('al');
            $historial_descanso_medico->fecha = date('Y-m-d');
            $historial_descanso_medico->hora = date('H:i:s');
            $historial_descanso_medico->id_doctor = auth()->user()->id;
            $historial_descanso_medico->doctor = auth()->user()->name.' '.auth()->user()->last_name;
            $historial_descanso_medico->cmp = $request->input('cmp');
            $save = $historial_descanso_medico->save();
    
            $data = array (
                'paciente' => $usuario->name.' '.$usuario->last_name,
                'edad' => $request->input('edad'),
                'hcn' => $request->input('hcn'),
                'diagnostico' => $request->input('diagnostico'),
                'dias' => $request->input('dias'),
                'horas' => $request->input('horas'),
                'del' => $request->input('del'),
                'al' => $request->input('al'),
                'fecha' => date('Y-m-d'),
                'hora' => date('H:i:s'),
                'doctor' => auth()->user()->name.' '.auth()->user()->last_name,
                'cmp' => $request->input('cmp'),
                'img' => url('/').'/images/medilaboris_logo.png',
            );
    
            // dd($data['paciente']);
    
            if($save){
                // $pdf = PDF::loadView('pdf.descanso_medico', compact('data'));
                $request->session()->flash('id_hdm', $historial_descanso_medico->id );
                return redirect()->route('descanso-medico')->with('success', 'El descanso medico se ha emitido de manera correcta, dar click en el boton para visualizarlo');
            }
        }else{
            return redirect()->route('descanso-medico')->with('error', 'El descanso medico del paciente ya ha sido emitido.');
        }
    }

    public function descansoMedicoPdf($id_hdm)
    {
        $hdm = DB::table('historial_descanso_medico')->where('id',$id_hdm)->first();
        return view('pdf.descanso_medico',compact('hdm'));
    }
    
    //Alta Epidemiológica = reincorporacion Laboral

    public function reincorporacionLaboral(){
        
        // $pacientes = DB::table('historial_descanso_medico')
        // ->join('users','users.id','historial_descanso_medico.id_persona')
        // ->where('historial_descanso_medico.logica_delete',1)
        // ->where('id_doctor',auth()->user()->id)
        // ->select('historial_descanso_medico.id','users.name','users.last_name')
        // ->get();
        
        $pacientes = DB::table('users')->join('datos_colaborador','datos_colaborador.id_user','users.id')
        ->join('medico_empresa','medico_empresa.id_empresa','datos_colaborador.id_empresa')
        ->where('medico_empresa.id_medico',auth()->user()->id)->where('users.logica_delete',1)->select('users.*')->get();
        
        $lista_reincorpracion = DB::table('historial_reincorporacion')->join('historial_descanso_medico','historial_descanso_medico.id','historial_reincorporacion.id_hdm')->select('historial_reincorporacion.*','historial_descanso_medico.nombre')->get();
        return view('admin.medico.reincorporacion_laboral',compact('pacientes','lista_reincorpracion'));
    }

    public function reincorporacionLaboralEdad(Request $request){
        
        $fase1 = DB::table('historial_descanso_medico')
        ->join('historial_riesgos','historial_riesgos.id','historial_descanso_medico.id_historial_riesgo')
        ->join('fase1','fase1.id','historial_riesgos.id_riesgo')->where('historial_descanso_medico.id',$request->input('paciente'))->select('fase1.*','historial_descanso_medico.id_persona')->first();

        $fr = DB::table('user_riesgo')->join('grupo_riesgo','grupo_riesgo.id','user_riesgo.id_grupo_riesgo')
        ->where('user_riesgo.id_user',$fase1->id_persona)->select('grupo_riesgo.*')->get();

        $historial_descanso_medico = DB::table('historial_descanso_medico')->where('id',$request->input('paciente'))->first();

        // $fase4 = DB::table('historial_descanso_medico')
        // ->join('historial_riesgos','historial_riesgos.id','historial_descanso_medico.id_historial_riesgo')
        // ->join('fase4','fase4.id','historial_riesgos.id_riesgo')->where('historial_descanso_medico.id',$request->input('paciente'))->whereDate('historial_riesgos.created_at','>',$fase1->created_at)->select('historial_riesgos.id','fase4.fecha_hospitalizacion','fase4.fecha_alta')->orderBy('historial_riesgos.id','desc')->first();

        $texto = '';
        $c = 1;
        foreach ($fr as $item){
            if($c == 1){
                $texto = $item->title;
            }else{
                $texto = $texto.', '.$item->title;
            }
            $c++;
        }

        return response()->json(['fase1'=>$fase1,'texto'=>$texto,'historial_descanso_medico'=>$historial_descanso_medico]);
    }

    public function reincorporacionLaboralSave(Request $request)
    {
    //    dd($request->input('hospitalizacion'));

        $rules = [
            'paciente' => 'required',
            'sigysinto' => 'required',
            'facriesgo' => 'required',
            'tipo_caso' => 'required',
            'inicio_sintomas' => 'required',
            'fin_sintomas' => 'required',
            'inicio_aislamiento' => 'required',
            'fin_aislamiento' => 'required',
            'cuadroclinico' => 'required',
            'modo_reincorporacion' => 'required',
            'fecha_reincorporacion' => 'required',
        ];
        $customMessages = [
            'paciente.required' => 'Campo paciente es obligatorio',
            'sigysinto.required' => 'Campo signos y sintomas principales es obligatorio',
            'facriesgo.required' => 'Campo factores de riesgo (edad) es obligatorio',
            'tipo_caso.required' => 'Campo tipo de caso es obligatorio',
            'inicio_sintomas.required' => 'Campo inicio de sintomas es obligatorio',
            'fin_sintomas.required' => 'Campo fin de sintomas es obligatorio',
            'inicio_aislamiento.required' => 'Campo inicio de aislamiento del es obligatorio',
            'fin_aislamiento.required' => 'Campo fin de aislamiento al es obligatorio',
            'cuadroclinico.required' => 'Campo cuadro clinico es obligatorio',
            'modo_reincorporacion.required' => 'Campo modo de reincorporación es obligatorio',
            'fecha_reincorporacion.required' => 'Campo fecha de reincorporación es obligatorio',
        ];

        $this->validate($request, $rules, $customMessages);

        $historial_reincorporacion = new HistorialReincorporacion();
        $historial_reincorporacion->id_hdm = $request->input('paciente');
        $historial_reincorporacion->signos_sintomas_principales = $request->input('sigysinto');
        $historial_reincorporacion->factores_riesgo = $request->input('facriesgo');
        $historial_reincorporacion->tipo_caso = $request->input('tipo_caso');
        $historial_reincorporacion->rango_sintoma_del = $request->input('inicio_sintomas');
        $historial_reincorporacion->rango_sintoma_al = $request->input('fin_sintomas');
        $historial_reincorporacion->rango_aislamiento_del = $request->input('inicio_aislamiento');
        $historial_reincorporacion->rango_aislamiento_al = $request->input('fin_aislamiento');
        $historial_reincorporacion->prm_1_fecha = $request->input('date1RA');
        $historial_reincorporacion->prm_1_igm = (!empty($request->input('IGM1'))) ? $request->input('IGM1') : 'off' ;
        $historial_reincorporacion->prm_1_igg = (!empty($request->input('IGG1'))) ? $request->input('IGG1') : 'off' ;
        $historial_reincorporacion->prm_1_pm = (!empty($request->input('PM1'))) ? $request->input('PM1') : 'off' ;
        $historial_reincorporacion->prm_1_lab = (!empty($request->input('LAB1'))) ? $request->input('LAB1') : 'off' ;
        $historial_reincorporacion->prm_2_fecha = (!empty($request->input('date2DA'))) ? $request->input('date2DA') : 'off' ;
        $historial_reincorporacion->prm_2_igm = (!empty($request->input('IGM2'))) ? $request->input('IGM2') : 'off' ;
        $historial_reincorporacion->prm_2_igg = (!empty($request->input('IGG2'))) ? $request->input('IGG2') : 'off' ;
        $historial_reincorporacion->prm_2_pm = (!empty($request->input('PM2'))) ? $request->input('PM2') : 'off' ;
        $historial_reincorporacion->prm_2_lab = (!empty($request->input('LAB2'))) ? $request->input('LAB2') : 'off' ;
        $historial_reincorporacion->cuadro_clinico = $request->input('cuadroclinico');
        $historial_reincorporacion->complicaciones_del = $request->input('hospitalizacion')['del'];
        $historial_reincorporacion->complicaciones_al = $request->input('hospitalizacion')['al'];
        $historial_reincorporacion->vent_mec = $request->input('hospitalizacion')['vent_mec'];
        $historial_reincorporacion->f_alta = $request->input('hospitalizacion')['f_alta'];
        $historial_reincorporacion->modo_reincorporacion = $request->input('modo_reincorporacion');
        $historial_reincorporacion->fecha_reincorporacion = $request->input('fecha_reincorporacion');
        $historial_reincorporacion->logica_delete = 1;
        $save = $historial_reincorporacion->save();

        

        if($save){
            $criterio_de_reincorporacion = new CriterioDeReincorporacion();
            $criterio_de_reincorporacion->id_hr = $historial_reincorporacion->id;
            $criterio_de_reincorporacion->criterio_name = 'ASINTOMATICO';
            $criterio_de_reincorporacion->criterio_select = $request->input('asintomatico_select');
            $criterio_de_reincorporacion->especificacion = (!empty($request->input('asintomatico_especificacion'))) ? $request->input('asintomatico_especificacion') : '';
            $criterio_de_reincorporacion->save();

            $criterio_de_reincorporacion = new CriterioDeReincorporacion();
            $criterio_de_reincorporacion->id_hr = $historial_reincorporacion->id;
            $criterio_de_reincorporacion->criterio_name = 'FACTORES DE RIESGO';
            $criterio_de_reincorporacion->criterio_select = $request->input('cr2_select');
            $criterio_de_reincorporacion->especificacion = (!empty($request->input('cr2_especificacion'))) ? $request->input('cr2_especificacion') : ' ';
            $criterio_de_reincorporacion->save();

            $criterio_de_reincorporacion = new CriterioDeReincorporacion();
            $criterio_de_reincorporacion->id_hr = $historial_reincorporacion->id;
            $criterio_de_reincorporacion->criterio_name = 'TUVO 2 SEMANAS AISLAMIENTO';
            $criterio_de_reincorporacion->criterio_select = $request->input('cr3_select');
            $criterio_de_reincorporacion->especificacion = (!empty($request->input('cr3_especificacion'))) ? $request->input('cr3_especificacion') : ' ';
            $criterio_de_reincorporacion->save();

            $criterio_de_reincorporacion = new CriterioDeReincorporacion();
            $criterio_de_reincorporacion->id_hr = $historial_reincorporacion->id;
            $criterio_de_reincorporacion->criterio_name = 'P.RAPIDA PREVIA RETORNO';
            $criterio_de_reincorporacion->criterio_select = $request->input('cr4_select');
            $criterio_de_reincorporacion->especificacion = (!empty($request->input('cr4_especificacion'))) ? $request->input('cr4_especificacion') : ' ';
            $criterio_de_reincorporacion->save();

            $criterio_de_reincorporacion = new CriterioDeReincorporacion();
            $criterio_de_reincorporacion->id_hr = $historial_reincorporacion->id;
            $criterio_de_reincorporacion->criterio_name = 'COMPLICACIONES';
            $criterio_de_reincorporacion->criterio_select = $request->input('cr5_select');
            $criterio_de_reincorporacion->especificacion = (!empty($request->input('cr5_especificacion'))) ? $request->input('cr5_especificacion') : ' ';
            $criterio_de_reincorporacion->save();

            $criterio_de_reincorporacion = new CriterioDeReincorporacion();
            $criterio_de_reincorporacion->id_hr = $historial_reincorporacion->id;
            $criterio_de_reincorporacion->criterio_name = 'PUESTO ESENCIAL';
            $criterio_de_reincorporacion->criterio_select = $request->input('cr6_select');
            $criterio_de_reincorporacion->especificacion = (!empty($request->input('cr6_especificacion'))) ? $request->input('cr6_especificacion') : ' ';
            $criterio_de_reincorporacion->save();

            $criterio_de_reincorporacion = new CriterioDeReincorporacion();
            $criterio_de_reincorporacion->id_hr = $historial_reincorporacion->id;
            $criterio_de_reincorporacion->criterio_name = 'RIESGO EXPOSICION';
            $criterio_de_reincorporacion->criterio_select = $request->input('cr7_select');
            $criterio_de_reincorporacion->especificacion = '';
            $criterio_de_reincorporacion->save();

            $recomendaciones = $request->input('recomendaciones');

            for($i = 0 ; $i < count($recomendaciones) ; $i++){
                if($recomendaciones[$i] != ''){
                    $recomendaciones_reincorporacion = new RecomendacionesReincorporacion();
                    $recomendaciones_reincorporacion->id_hr = $historial_reincorporacion->id;
                    $recomendaciones_reincorporacion->recomendacion = $recomendaciones[$i];
                    $recomendaciones_reincorporacion->save();
                }
                
            }

        }

        $request->session()->flash('id_hdm', $historial_reincorporacion->id );
        return redirect()->route('reincorporacion-laboral')->with('success', 'La autorización de reincorporacion se ha emitido de manera exitosa, click en el boton para visualizarlo');
        // dd($request->all());

    }

    public function reincorporacionLaboralPdf($id_hdm)
    {
        $datos = DB::table('historial_reincorporacion')
        ->join('historial_descanso_medico','historial_descanso_medico.id','historial_reincorporacion.id_hdm')
        ->where('historial_reincorporacion.id',$id_hdm)->select('historial_reincorporacion.*','historial_descanso_medico.nombre')->first();

        if($datos){
            $creterios_reincorporacion = DB::table('criterio_de_reincorporacion')->where('id_hr',$datos->id)->get();

            $recomendaciones = DB::table('recomendaciones_reincorporacion')->where('id_hr',$datos->id)->get(); 
        }else{
            
             $datos = DB::table('historial_reincorporacion')
            ->join('users','users.id','historial_reincorporacion.id_hdm')
            ->where('historial_reincorporacion.id',$id_hdm)->select('historial_reincorporacion.*','users.name as nombre')->first();
            
            $creterios_reincorporacion = DB::table('criterio_de_reincorporacion')->where('id_hr',$id_hdm)->get();

            $recomendaciones = DB::table('recomendaciones_reincorporacion')->where('id_hr',$id_hdm)->get();
        }
        
   

        return view('pdf.reincorporacion_laboral',compact('datos','creterios_reincorporacion','recomendaciones'));
    }
}

