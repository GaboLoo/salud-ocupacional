<?php

namespace App\Http\Controllers;

use App\Models\Area;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BitacoraController extends Controller
{
 
 public function bitacoraMedico(){
    
 
    if(auth()->user()->id_type_user == 2){
        $empresas = DB::table('medico_empresa')->join('empresa','empresa.id','=','medico_empresa.id_empresa')
                ->where('id_medico',auth()->user()->id)->where('empresa.logica_delete',1)->select('empresa.*')->get();
                
        $bitacoras = DB::table('bitacora')->join('empresa','empresa.id','=','bitacora.id_empresa')->where('id_medico',auth()->user()->id)->select('bitacora.*','empresa.title as empresa')->get();
    }else{
        $empresas = DB::table('empresa')->where('empresa.logica_delete',1)->get();
        $bitacoras = DB::table('bitacora')->join('empresa','empresa.id','=','bitacora.id_empresa')->select('bitacora.*','empresa.title as empresa')->get();
    }
    
    
    
    return view('admin.bitacora.medico',compact('empresas','bitacoras'));
 }
 
 public function saveBitacoraMedico(Request $request){
    
    $rules = [
        'fecha' => 'required',
        'empresa' => 'required',
        'resumen' => 'required'
    ];

    $messages = [
        'fecha.required' => 'El campo fecha es campo obligatorio',
        'empresa.required' => 'El campo empresa es campo obligatorio',
        'resumen.required' => 'El campo resumen es campo obligatorio',
    ];

    $this->validate($request, $rules, $messages);
    
    
    $save = DB::table('bitacora')->insert([
        
        'created_at' => $request->input('fecha'),
        'id_empresa' =>  $request->input('empresa'),
        'id_medico' => auth()->user()->id ,
        'mensaje' => $request->input('resumen')
        
    ]);
    
    if ($save){
        return back()->with('success','La bitacora se guardo correctamente');
    }else{
        return back()->with('error','Ups, error al guardar la información, intente mas tarde');
    }
     
 }
 
 public function bitacoraMedicoConsult(Request $request){
     
    $rules = [
        'fecha_inicio' => 'required',
        'fecha_fin' => 'required',
        'empresa' => 'required'
    ];

    $messages = [
        'fecha_inicio.required' => 'El campo fecha inicio es campo obligatorio',
        'fecha_fin.required' => 'El campo fecha fin es campo obligatorio',
        'empresa.required' => 'El campo empresa es campo obligatorio',
    ];

    $this->validate($request, $rules, $messages);
    
    if(auth()->user()->id_type_user == 2){
        $bitacoras = DB::table('bitacora')->join('empresa','empresa.id','=','bitacora.id_empresa')->where('id_medico',auth()->user()->id)
        ->where('bitacora.id_empresa',$request->input('empresa'))
        ->whereDate('bitacora.created_at','>=',$request->input('fecha_inicio'))
        ->whereDate('bitacora.created_at','<=',$request->input('fecha_fin'))
        ->select('bitacora.*','empresa.title as empresa')
        ->get();
    }else{
        $bitacoras = DB::table('bitacora')->join('empresa','empresa.id','=','bitacora.id_empresa')
        ->where('bitacora.id_empresa',$request->input('empresa'))
        ->whereDate('bitacora.created_at','>=',$request->input('fecha_inicio'))
        ->whereDate('bitacora.created_at','<=',$request->input('fecha_fin'))
        ->select('bitacora.*','empresa.title as empresa')
        ->get();
    }
    
    
    $tabla = '<table class="table mt-5" id="example2">
                                        <thead>
                                            <th>Nº</th>
                                            <th>Fecha</th>
                                            <th>Empresa</th>
                                            <th>Resumen</th>
                                        </thead>
                                        <tbody>';
    $c = 1;
    foreach($bitacoras as $bitacora){
        $tabla.= '<tr>
                    <td>'.$c.'</td>
                    <td>'.$bitacora->created_at.'</td>
                    <td>'.$bitacora->empresa.'</td>
                    <td>'.$bitacora->mensaje.'</td>
                </tr>';
    }
    
    $tabla.='</tbody></table>';
    
    return response()->json($tabla);
     
 }
    
}