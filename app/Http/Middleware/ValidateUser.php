<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class ValidateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->user()->id_type_user == 3){
            $error = null;

            if(auth()->user()->documento == ''){
                $error[] = 'error';
            }
            if(auth()->user()->celular == ''){
                $error[] = 'error';
            }
            if(auth()->user()->direccion == ''){
                $error[] = 'error';
            }

            if ($error == null){
                $data_validate = DB::table('datos_colaborador')->where('id_user',auth()->user()->id)->first();
                if ($this->validateDataColaborador($data_validate)){
                    session(['validaccion' => true]);
                    return redirect()->route('form-colaborador.perfil');
                }else{
                    return redirect()->route('form-colaborador.index');
                }
            }else{
//                return redirect()->route('form-colaborador.perfil');
            }

        }
        return $next($request);
    }

    function validateDataColaborador($datos){

        $error = null;


        if ($datos->telefono_casa == ''){
            $error[] = 'error';
        }

        if ($datos->puesto == ''){
            $error[] = 'error';
        }

        if ($datos->area == ''){
            $error[] = 'error';
        }

        if ($datos->fecha_nacimiento ==  ''){
            $error[] = 'error';
        }

        if ($datos->peso == ''){
            $error[] = 'error';
        }

        if ($datos->altura == ''){
            $error[] = 'error';
        }

        if ($datos->enfermedades == ''){
            $error[] = 'error';
        }

        if ($error == null){
            return false;
        }else{
            return true;
        }
    }
}
