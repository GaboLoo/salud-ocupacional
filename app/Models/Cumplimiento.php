<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cumplimiento
 * 
 * @property int $id
 * @property int $id_user
 * @property string $cumplimiento
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Cumplimiento extends Model
{
	protected $table = 'cumplimiento';

	protected $casts = [
		'id_user' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_user',
		'cumplimiento',
		'logica_delete'
	];
}
