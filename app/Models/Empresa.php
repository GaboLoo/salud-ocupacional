<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Empresa
 * 
 * @property int $id
 * @property string $title
 * @property string $ruc
 * @property string $direccion
 * @property string $telefono
 * @property string $correo
 * @property int $id_rugro
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Empresa extends Model
{
	protected $table = 'empresa';

	protected $casts = [
		'id_rugro' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'title',
		'ruc',
		'direccion',
		'telefono',
		'correo',
		'id_rugro',
		'logica_delete'
	];
}
