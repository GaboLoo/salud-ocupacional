<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Historial
 * 
 * @property int $id
 * @property int $id_user
 * @property int $id_fase
 * @property int $id_encuesta
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Historial extends Model
{
	protected $table = 'historial';

	protected $casts = [
		'id_user' => 'int',
		'id_fase' => 'int',
		'id_encuesta' => 'int'
	];

	protected $fillable = [
		'id_user',
		'id_fase',
		'id_encuesta'
	];
}
