<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fase1
 * 
 * @property int $id
 * @property int $id_user
 * @property string $opciones_seleccionadas
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Fase1 extends Model
{
	protected $table = 'fase1';

	protected $casts = [
		'id_user' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_user',
		'opciones_seleccionadas',
		'logica_delete'
	];
}
