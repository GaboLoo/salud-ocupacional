<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DatosColaborador
 * 
 * @property int $id
 * @property int $id_user
 * @property string $telefono_casa
 * @property string $puesto
 * @property string $area
 * @property string $fecha_nacimiento
 * @property string $peso
 * @property string $altura
 * @property string $enfermedades
 * @property string $tipo_sangre
 * @property string $alergico
 * @property int $id_empresa
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class DatosColaborador extends Model
{
	protected $table = 'datos_colaborador';

	protected $casts = [
		'id_user' => 'int',
		'id_empresa' => 'int'
	];

	protected $fillable = [
		'id_user',
		'telefono_casa',
		'puesto',
		'area',
		'fecha_nacimiento',
		'peso',
		'altura',
		'enfermedades',
		'tipo_sangre',
		'alergico',
		'id_empresa'
	];
}
