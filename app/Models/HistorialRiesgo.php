<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HistorialRiesgo
 * 
 * @property int $id
 * @property int $id_user
 * @property int $id_riesgo
 * @property string $name_table
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class HistorialRiesgo extends Model
{
	protected $table = 'historial_riesgos';

	protected $casts = [
		'id_user' => 'int',
		'id_riesgo' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_user',
		'id_riesgo',
		'name_table',
		'logica_delete'
	];
}
