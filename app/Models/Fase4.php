<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fase4
 * 
 * @property int $id
 * @property string $diagnostico
 * @property string $tipo_diagnostico
 * @property string $fecha_toma
 * @property string $como_cree_que_infecto
 * @property string $hospitalizacion
 * @property string $fecha_hospitalizacion
 * @property string $fecha_alta
 * @property string $complicacion_hospitalaria
 * @property string $complicacion_hospitalaria_descripcion
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Fase4 extends Model
{
	protected $table = 'fase4';

	protected $casts = [
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'diagnostico',
		'tipo_diagnostico',
		'fecha_toma',
		'como_cree_que_infecto',
		'hospitalizacion',
		'fecha_hospitalizacion',
		'fecha_alta',
		'complicacion_hospitalaria',
		'complicacion_hospitalaria_descripcion',
		'logica_delete'
	];
}
