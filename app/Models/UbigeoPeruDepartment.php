<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UbigeoPeruDepartment
 * 
 * @property string $id
 * @property string $name
 *
 * @package App\Models
 */
class UbigeoPeruDepartment extends Model
{
	protected $table = 'ubigeo_peru_departments';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'name'
	];
}
