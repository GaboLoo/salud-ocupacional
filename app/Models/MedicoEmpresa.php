<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MedicoEmpresa
 * 
 * @property int $id
 * @property int $id_medico
 * @property int $id_empresa
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class MedicoEmpresa extends Model
{
	protected $table = 'medico_empresa';

	protected $casts = [
		'id_medico' => 'int',
		'id_empresa' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_medico',
		'id_empresa',
		'logica_delete'
	];
}
