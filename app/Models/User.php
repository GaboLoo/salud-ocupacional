<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * 
 * @property int $id
 * @property int $id_type_user
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property Carbon $email_verified_at
 * @property string $password
 * @property string $type_documento
 * @property string $documento
 * @property string $celular
 * @property string $direccion
 * @property string $foto
 * @property string $dias
 * @property string $estado_salud
 * @property string $seguimiento
 * @property string $cumplimiento
 * @property int $id_departamento
 * @property int $id_provincia
 * @property int $id_distrito
 * @property int $logica_delete
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class User extends Model
{
	protected $table = 'users';

	protected $casts = [
		'id_type_user' => 'int',
		'id_departamento' => 'int',
		'id_provincia' => 'int',
		'id_distrito' => 'int',
		'logica_delete' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'id_type_user',
		'name',
		'last_name',
		'email',
		'email_verified_at',
		'password',
		'type_documento',
		'documento',
		'celular',
		'direccion',
		'foto',
		'dias',
		'estado_salud',
		'seguimiento',
		'cumplimiento',
		'id_departamento',
		'id_provincia',
		'id_distrito',
		'logica_delete',
		'remember_token'
	];
}
