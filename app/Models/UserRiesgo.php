<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRiesgo
 * 
 * @property int $id
 * @property int $id_user
 * @property int $id_grupo_riesgo
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class UserRiesgo extends Model
{
	protected $table = 'user_riesgo';

	protected $casts = [
		'id_user' => 'int',
		'id_grupo_riesgo' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_user',
		'id_grupo_riesgo',
		'logica_delete'
	];
}
