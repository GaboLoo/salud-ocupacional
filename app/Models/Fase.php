<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fase
 * 
 * @property int $id
 * @property string $titulo
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Fase extends Model
{
	protected $table = 'fase';

	protected $casts = [
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'titulo',
		'logica_delete'
	];
}
