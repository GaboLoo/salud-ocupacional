<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fase3Item
 * 
 * @property int $id
 * @property string $id_pregunta
 * @property int $id_usuario
 * @property string $id_fase3
 * @property string $respuesta
 * @property int $logicaa_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Fase3Item extends Model
{
	protected $table = 'fase3_items';

	protected $casts = [
		'id_usuario' => 'int',
		'logicaa_delete' => 'int'
	];

	protected $fillable = [
		'id_pregunta',
		'id_usuario',
		'id_fase3',
		'respuesta',
		'logicaa_delete'
	];
}
