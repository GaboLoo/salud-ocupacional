<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UbigeoPeruProvince
 * 
 * @property string $id
 * @property string $name
 * @property string $department_id
 *
 * @package App\Models
 */
class UbigeoPeruProvince extends Model
{
	protected $table = 'ubigeo_peru_provinces';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'name',
		'department_id'
	];
}
