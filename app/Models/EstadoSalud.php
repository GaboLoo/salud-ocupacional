<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class EstadoSalud
 * 
 * @property int $id
 * @property int $id_user
 * @property string $type_estado_salud
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class EstadoSalud extends Model
{
	protected $table = 'estado_salud';

	protected $casts = [
		'id_user' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_user',
		'type_estado_salud',
		'logica_delete'
	];
}
