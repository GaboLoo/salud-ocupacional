<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fase2
 * 
 * @property int $id
 * @property string $pregunta1
 * @property string $pregunta2
 * @property string $pregunta3
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Fase2 extends Model
{
	protected $table = 'fase2';

	protected $casts = [
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'pregunta1',
		'pregunta2',
		'pregunta3',
		'logica_delete'
	];
}
