<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PreguntasFase3
 * 
 * @property int $id
 * @property string $pregunta
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class PreguntasFase3 extends Model
{
	protected $table = 'preguntas_fase_3';

	protected $casts = [
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'pregunta',
		'logica_delete'
	];
}
