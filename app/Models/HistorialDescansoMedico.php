<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HistorialDescansoMedico
 * 
 * @property int $id
 * @property int $id_persona
 * @property int $id_historial_riesgo
 * @property string $nombre
 * @property string $edad
 * @property string $hcn
 * @property string $diagnostico
 * @property string $dias
 * @property string $horas
 * @property string $del
 * @property string $al
 * @property string $fecha
 * @property string $hora
 * @property int $id_doctor
 * @property string $doctor
 * @property string $cmp
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class HistorialDescansoMedico extends Model
{
	protected $table = 'historial_descanso_medico';

	protected $casts = [
		'id_persona' => 'int',
		'id_historial_riesgo' => 'int',
		'id_doctor' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_persona',
		'id_historial_riesgo',
		'nombre',
		'edad',
		'hcn',
		'diagnostico',
		'dias',
		'horas',
		'del',
		'al',
		'fecha',
		'hora',
		'id_doctor',
		'doctor',
		'cmp',
		'logica_delete'
	];
}
