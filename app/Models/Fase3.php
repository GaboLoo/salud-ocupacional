<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Fase3
 * 
 * @property int $id
 * @property int $id_user
 * @property string $fecha_registro
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Fase3 extends Model
{
	protected $table = 'fase3';

	protected $casts = [
		'id_user' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_user',
		'fecha_registro',
		'logica_delete'
	];
}
