<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CriterioDeReincorporacion
 * 
 * @property int $id
 * @property int $id_hr
 * @property string $criterio_name
 * @property string $criterio_select
 * @property string $especificacion
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class CriterioDeReincorporacion extends Model
{
	protected $table = 'criterio_de_reincorporacion';

	protected $casts = [
		'id_hr' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_hr',
		'criterio_name',
		'criterio_select',
		'especificacion',
		'logica_delete'
	];
}
