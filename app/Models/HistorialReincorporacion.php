<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HistorialReincorporacion
 * 
 * @property int $id
 * @property int $id_hdm
 * @property string $signos_sintomas_principales
 * @property string $factores_riesgo
 * @property string $tipo_caso
 * @property string $rango_sintoma_del
 * @property string $rango_sintoma_al
 * @property string $rango_aislamiento_del
 * @property string $rango_aislamiento_al
 * @property string $prm_1_fecha
 * @property string $prm_1_igm
 * @property string $prm_1_igg
 * @property string $prm_1_pm
 * @property string $prm_1_lab
 * @property string $prm_2_fecha
 * @property string $prm_2_igm
 * @property string $prm_2_igg
 * @property string $prm_2_pm
 * @property string $prm_2_lab
 * @property string $cuadro_clinico
 * @property string $complicaciones_del
 * @property string $complicaciones_al
 * @property string $vent_mec
 * @property string $f_alta
 * @property string $modo_reincorporacion
 * @property string $fecha_reincorporacion
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class HistorialReincorporacion extends Model
{
	protected $table = 'historial_reincorporacion';

	protected $casts = [
		'id_hdm' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_hdm',
		'signos_sintomas_principales',
		'factores_riesgo',
		'tipo_caso',
		'rango_sintoma_del',
		'rango_sintoma_al',
		'rango_aislamiento_del',
		'rango_aislamiento_al',
		'prm_1_fecha',
		'prm_1_igm',
		'prm_1_igg',
		'prm_1_pm',
		'prm_1_lab',
		'prm_2_fecha',
		'prm_2_igm',
		'prm_2_igg',
		'prm_2_pm',
		'prm_2_lab',
		'cuadro_clinico',
		'complicaciones_del',
		'complicaciones_al',
		'vent_mec',
		'f_alta',
		'modo_reincorporacion',
		'fecha_reincorporacion',
		'logica_delete'
	];
}
