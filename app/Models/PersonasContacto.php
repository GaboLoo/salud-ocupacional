<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PersonasContacto
 * 
 * @property int $id
 * @property int $id_fase2
 * @property string $nombre
 * @property string $dni
 * @property string $edad
 * @property string $sexo
 * @property string $telefono
 * @property string $direccion
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class PersonasContacto extends Model
{
	protected $table = 'personas_contacto';

	protected $casts = [
		'id_fase2' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_fase2',
		'nombre',
		'dni',
		'edad',
		'sexo',
		'telefono',
		'direccion',
		'logica_delete'
	];
}
