<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Seguimiento
 * 
 * @property int $id
 * @property int $id_estado_salud
 * @property string $seguimiento
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Seguimiento extends Model
{
	protected $table = 'seguimiento';

	protected $casts = [
		'id_estado_salud' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_estado_salud',
		'seguimiento',
		'logica_delete'
	];
}
