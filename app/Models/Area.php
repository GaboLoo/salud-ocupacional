<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Area
 * 
 * @property int $id
 * @property string $descripcion
 * @property string $titulo
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Area extends Model
{
	protected $table = 'area';

	protected $casts = [
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'descripcion',
		'titulo',
		'logica_delete'
	];
}
