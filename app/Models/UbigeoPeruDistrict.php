<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UbigeoPeruDistrict
 * 
 * @property string $id
 * @property string $name
 * @property string $province_id
 * @property string $department_id
 *
 * @package App\Models
 */
class UbigeoPeruDistrict extends Model
{
	protected $table = 'ubigeo_peru_districts';
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'name',
		'province_id',
		'department_id'
	];
}
