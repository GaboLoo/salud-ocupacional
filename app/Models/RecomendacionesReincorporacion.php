<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RecomendacionesReincorporacion
 * 
 * @property int $id
 * @property int $id_hr
 * @property string $recomendacion
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class RecomendacionesReincorporacion extends Model
{
	protected $table = 'recomendaciones_reincorporacion';

	protected $casts = [
		'id_hr' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_hr',
		'recomendacion',
		'logica_delete'
	];
}
