<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Diagnostico
 * 
 * @property int $id
 * @property int $id_fase
 * @property string $id_usuario
 * @property string $titulo
 * @property string $descripcion
 * @property string $fecha_hora
 * @property string $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Diagnostico extends Model
{
	protected $table = 'diagnostico';

	protected $casts = [
		'id_fase' => 'int'
	];

	protected $fillable = [
		'id_fase',
		'id_usuario',
		'titulo',
		'descripcion',
		'fecha_hora',
		'logica_delete'
	];
}
