<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TypeUser
 * 
 * @property int $id
 * @property string $title
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TypeUser extends Model
{
	protected $table = 'type_user';

	protected $casts = [
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'title',
		'logica_delete'
	];
}
