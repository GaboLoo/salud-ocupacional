<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Puesto
 * 
 * @property int $id
 * @property int $id_empresa
 * @property string $titulo
 * @property int $logica_delete
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Puesto extends Model
{
	protected $table = 'puesto';

	protected $casts = [
		'id_empresa' => 'int',
		'logica_delete' => 'int'
	];

	protected $fillable = [
		'id_empresa',
		'titulo',
		'logica_delete'
	];
}
